process.env.NODE_ENV = 'production'

require('colors')

var
  shell = require('shelljs'),
  path = require('path'),
  env = require('./env-utils'),
  webpack = require('webpack'),
  webpackConfig = require('./webpack.prod.conf'),
  targetPath = path.join(__dirname, '../dist'),
  S3Plugin = require('webpack-s3-plugin'),
  S3SyncPlugin = require('webpack-s3-sync-plugin')

require('./script.clean.js')

console.log((' Building App with "' + env.platform.theme + '" theme...\n').bold)

const isDeployToS3 = process.argv[3] === 'DEPLOY_TO_S3';
const bucketName = process.argv[4];
const distributionId = process.argv[5];
const s3Endpoint = process.argv[6];
const s3CDN = process.argv[7];
const accessKey = 'AKIAJVS77MP745LPFT2A';
const secretKey = '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';
const region = 'ap-southeast-1';

if (isDeployToS3 && bucketName && distributionId && s3Endpoint && s3CDN) {
  webpackConfig.plugins.push(new S3SyncPlugin({
    s3Options: {
      accessKeyId: accessKey,
      secretAccessKey: secretKey,
      region: region
    },
    s3UploadOptions: {
      Bucket: bucketName
    }
  }));
  webpackConfig.plugins.push(new S3Plugin({
    include: /.*/,
    s3Options: {
      accessKeyId: accessKey,
      secretAccessKey: secretKey,
      region: region
    },
    s3UploadOptions: {
      Bucket: bucketName
    }
    ,
    // for Demo
    cloudfrontInvalidateOptions: {
      DistributionId: distributionId,
      Items: ["/*"]
    }
  }));
}

shell.mkdir('-p', targetPath)
shell.mkdir('-p', path.join(targetPath, 'statics'))
shell.mkdir('-p', path.join(targetPath, 'themes'))
shell.cp('-R', 'src/statics/*', path.join(targetPath, 'statics'))
shell.cp('-R', 'src/themes/*', path.join(targetPath, 'themes'))

webpack(webpackConfig, function (err, stats) {
  if (err) throw err
  process.stdout.write(stats.toString({
    colors: true,
    modules: false,
    children: false,
    chunks: false,
    chunkModules: false
  }) + '\n')

  console.log(('Build complete with "' + env.platform.theme.bold + '" theme in ' + '"/dist"'.bold + ' folder.\n').cyan);
  if (isDeployToS3) {
    console.log(('Upload complete to ' + 'S3'.bold + '.\n').cyan);
    console.log('Now you can access Trainee Web App at: ' + s3Endpoint.bold.cyan + ' or ' + s3CDN.bold.cyan);
  }
})
