require('colors')
var
  config = require('../config'),
  platformName = process.argv[2] || config.defaultPlatformName,
  debugMode = process.argv[3] ? process.argv[3] : null
  gaId = process.argv[4] ? process.argv[4] : process.argv[3] ? process.argv[3] : 'default'
  theme = process.argv[5] || config.defaultTheme
  console.log(`Argument ${process.argv}`.green)
  console.log(`platform ${platformName}`.green)
  console.log(`DebugMode ${debugMode}`.green)
  console.log(`Process Mode: ${process.env.NODE_ENV}`.green)

module.exports = {
  dev: process.env.NODE_ENV === 'development',
  prod: process.env.NODE_ENV === 'production',

  platform: {
    name: platformName,
    theme: theme,
    cordovaAssets: './cordova/platforms/' + (theme === 'mat' ? 'android' : 'ios') + '/platform_www'
  },

  logMode: {
    debug: debugMode
  }
}
