function RouterConfig($urlRouterProvider, $locationProvider, STATE, $stateProvider) {
  'ngInject'
  $locationProvider.html5Mode(true)
  $locationProvider.hashPrefix('')
  $urlRouterProvider.otherwise(`${STATE.HOME}`)
  $stateProvider
    .state('dashboard', {
      abstract: true,
      url: '',
      views: {
        'dashboard': {
          template: '<head-foot-layout flex layout="row"></head-foot-layout>'
        }
      }
    })
}
export default RouterConfig
