/**
 * Created by hangntt10 on 24/10/2017.
 */

class NavtopController {
  constructor($scope, EVENTS) {
    'ngInject'
    this.$scope = $scope
    this.EVENTS = EVENTS
  }
  showHideModal () {
    this.$scope.$emit(this.EVENTS.EXPAND_MENU, true)
  }
}
export default NavtopController

