import controller from './navtop.controller'
import template from './navtop.html'
import './navtop.scss'

const moduleName = 'app.component.directive.navtop'

angular.module(moduleName, [])
  .component('navtop', {
    controller,
    template,
    controllerAs: 'vm'
  })
export default moduleName
