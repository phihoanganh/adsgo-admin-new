class CourseCategory {
  constructor($scope, $stateParams, CategoryService, CourseService, PAGE_SIZE, EVENTS) {
    'ngInject'
    this.$scope = $scope
    this.$stateParams = $stateParams
    this.categoryService = CategoryService
    this.courseService = CourseService
    this.PAGE_SIZE = PAGE_SIZE
    this.EVENTS = EVENTS
    this.page = this.PAGE_SIZE.LIST_COURSE.PAGE
    this.size = this.PAGE_SIZE.LIST_COURSE.SIZE
  }

  $onInit() {
    this.categoryService.getCategories().then((res) => {
      this.courseTypeList = res
      this.set()
    })
    this.$scope.$on(this.EVENTS.DESELECT_CATEGORY, (event, data) => {
      if (data) {
        this.setActive({})
      }
    })
  }

  set() {
    this.categoryID = this.$stateParams.categoryID
    if (this.categoryID) {
      for (var c of this.courseTypeList) {
        if (c.id === this.categoryID) {
          c.isActive = true
        }
      }
    }
  }

  /**
   * filter courses by categoryID and set active for class category
   * @param item
   * @returns {*|Promise.<TResult>}
   */
  setActive(item) {
    let categoryID = item.id
    for (var c of this.courseTypeList) {
      delete c.isActive
    }
    if (categoryID) {
      item.isActive = true
      return this.courseService.searchByCourseName(null, categoryID, this.page, this.size).then((res) => {
        this.onSearchCategory({searchData: res})
      })
    }
  }
}

export default CourseCategory
