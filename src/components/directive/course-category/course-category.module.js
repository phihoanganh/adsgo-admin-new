import controller from './course-category.controller'
import template from './course-category.html'
import './course-category.scss'

const moduleName = 'app.components.directive.course-category'

angular.module(moduleName, [])
  .component('courseCategory', {
    controller,
    template,
    controllerAs: 'vm',
    bindings: {
      onSearchCategory: '&'
    }
  })

export default moduleName
