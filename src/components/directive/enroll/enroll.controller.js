class EnrollController {
  constructor($scope, CourseService, $stateParams, TRAINEE_STATUS) {
    'ngInject'
    this.courseService = CourseService
    this.$stateParams = $stateParams
    this.TRAINEE_STATUS = TRAINEE_STATUS
  }
  /**
   * init data
   */
  $onInit() {
    this.getAction(this.traineeStatus)
  }
  getAction(traineeStatus) {
    let link = ''
    switch (traineeStatus) {
      case this.TRAINEE_STATUS.COMPLETED:
        link = 'Xem lại khóa học'
        break
      case this.TRAINEE_STATUS.IN_PROGRESS:
        link = 'Tiếp tục khóa học'
        break
      case this.TRAINEE_STATUS.OPEN:
        link = 'Bắt đầu khóa học'
        break
      default:
        break
    }
    this.linkReturn = link
  }
}
export default EnrollController
