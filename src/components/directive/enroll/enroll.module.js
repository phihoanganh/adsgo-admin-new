import controller from './enroll.controller'
import template from './enroll.html'
import './enroll.scss'

const moduleName = 'app.component.directive.enroll'

angular.module(moduleName, [])
  .component('enroll', {
    controller,
    template,
    controllerAs: 'vm',
    bindings: {
      traineeStatus: '<'
    }
  })
export default moduleName
