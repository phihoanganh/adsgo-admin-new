/**
 * Created by ChinhHT1 on 12/5/2017.
 */

/* global angular:true */

import controller from './carousel.directive'
import template from './carousel.directive.html'
import './carousel.directive.scss'

const moduleName = 'app.components.directive.carousel'

angular.module(moduleName, [])
  .component('carousel', {
    controller,
    template,
    controllerAs: 'vm'
  })

export default moduleName
