/**
 * Created by ChinhHT1 on 12/5/2017.
 */
class Carousel {
  constructor($scope, $state, CourseService, STATE_NAME, COURSE_TYPE, PAGE_SIZE) {
    'ngInject'
    this.$scope = $scope
    this.$state = $state
    this.courseService = CourseService
    this.detailStateName = STATE_NAME.COURSES.DETAIL_COURSE
    this.bannerType = COURSE_TYPE.BANNER
    this.page = PAGE_SIZE.BANNER.PAGE
    this.size = PAGE_SIZE.BANNER.SIZE
  }

  $onInit() {
    this.getBanners(this.page, this.size, this.bannerType)
    this.sliderCarousel = {
      enabled: true,
      dots: true,
      method: {},
      arrows: true,
      infinite: true,
      slidesToScroll: 1,
      slidesToShow: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      draggable: false,
      event: {
        beforeChange: (event, slick, currentSlide, nextSlide) => {
          this.currentSlide = this.findOnCarousel(this.banners, nextSlide)
        }
      }
    }
  }

  findOnCarousel(carouselsData, index) {
    if (carouselsData && carouselsData.length > 0) {
      return carouselsData[index]
    }
  }

  carouselGoTo() {
    this.$state.go(this.detailStateName, {id: this.currentSlide.id, searchName: null})
  }

  getBanners(page, size, type) {
    this.courseService.getListCourse(page, size, type).then(res => {
      if (res && res.content) {
        this.banners = res.content
        this.currentSlide = this.findOnCarousel(this.banners, 0)
      }
    })
  }
}
export default Carousel
