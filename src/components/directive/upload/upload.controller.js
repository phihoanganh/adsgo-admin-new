class UploadController {
  constructor($scope, Upload, $timeout, EVENTS) {
    'ngInject'
    this.$scope = $scope
    this.Upload = Upload
    this.EVENTS = EVENTS
  }
  $onInit() {
    console.log(this.source)
    this.$scope.$on(this.EVENTS.CLEAR_UPLOAD_IMAGE, (event, data) => {
      this.image = undefined
    })
  }
  onChange() {
    this.onSelected({image: this.image})
  }
  onDelete() {
    this.image = undefined
    this.source = ''
    console.log(this.source)
    this.onChange()
  }
}

export default UploadController
