import controller from './upload.controller'
import style from './upload.scss'
import template from './upload.html'

let moduleName = 'app.component.directive.upload'

angular.module(moduleName, [])
  .component('upload', {
    controller,
    template,
    style,
    controllerAs: 'vm',
    bindings: {
      title: '@',
      position: '@',
      image: '<',
      onSelected: '&',
      chiendich: '@',
      source: '@'
    }
  })

export default moduleName
