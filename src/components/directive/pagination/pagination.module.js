import controller from './pagination.controller'
import style from './pagination.scss'
import template from './pagination.html'

let moduleName = 'app.component.directive.pagination'

angular.module(moduleName, [])
  .component('pagination', {
    controller,
    template,
    style,
    controllerAs: 'vm',
    bindings: {
      totalElement: '=',
      itemPerPage: '<',
      currentPage: '<',
      onPageChange: '&',
      onChangeNumItem: '&'
    }
  })

export default moduleName
