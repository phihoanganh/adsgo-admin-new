class PaginationController {
  constructor($scope, FORMAT, EVENTS) {
    'ngInject'
    this.$scope = $scope
    this.totalElement = 0
    this.itemPerPage = 10
    this.currentPage = 1
    this.numPageShow = 5
    this.totalPages = 1
  }
  updateItemPerPage() {
    this.onChangeNumItem({itemPerPage: this.itemPerPage})
  }
  preListPage() {
    this.startPage -= this.numPageShow
  }
  nextListPage() {
    this.startPage += this.numPageShow
  }
  $onInit() {
    this.dataRows = [10, 20, 30, 50, 100]
    this.itemPerPage = 10
  }
  $onChanges() {
    this.check()
  }
  goToPage(page) {
    this.currentPage = page
    this.onPageChange({page: page})
  }
  check() {
    var oldTotal = this.totalPages
    if (this.totalElement % this.itemPerPage === 0) {
      this.totalPages = parseInt(this.totalElement / this.itemPerPage)
    } else {
      this.totalPages = parseInt(this.totalElement / this.itemPerPage) + 1
    }
    if (this.totalPages !== oldTotal) {
      this.currentPage = 1
    }
  }
}

export default PaginationController
