/**
 * Created by hangntt10 on 24/10/2017.
 */

class PopupController {
  constructor($scope, $mdDialog) {
    'ngInject'
    this.$scope = $scope
    this.$mdDialog = $mdDialog
    this.status = ''
  }
  $onInit() {
    this.status = 'ok'
  }
  showConfirm(ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = this.$mdDialog.confirm()
          .title('Would you like to delete your debt?')
          .textContent('All of the banks have agreed to forgive you your debts.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Please do it!')
          .cancel('Sounds like a scam')

    this.$mdDialog.show(confirm).then(function() {
      console.log('Okie')
    }, function() {
      console.log('Cancel')
    })
  }
}
export default PopupController

