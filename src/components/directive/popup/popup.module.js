import controller from './popup.controller'
import template from './popup.html'
import './popup.scss'

const moduleName = 'app.component.directive.popup'

angular.module(moduleName, [])
  .component('popup', {
    controller,
    template,
    controllerAs: 'vm'
  })
export default moduleName
