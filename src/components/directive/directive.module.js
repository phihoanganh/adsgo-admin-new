import pagination from './pagination/pagination.module'
import enroll from './enroll/enroll.module'
import carousel from './carousel/carousel.directive.module'
import courseCategory from './course-category/course-category.module'
import navigation from './navigation/navigation.module'
import navtop from './navtop/navtop.module'
import popup from './popup/popup.module'
import upload from './upload/upload.module'
import uploadcsv from './upload-csv/upload-csv.module'

const moduleName = 'app.components.directive'

angular.module(moduleName, [pagination, enroll, carousel, courseCategory, navigation, navtop, popup, upload, uploadcsv])

export default moduleName
