class UploadCSVController {
  constructor($scope, Upload, $timeout, EVENTS) {
    'ngInject'
    this.$scope = $scope
    this.title = ''
    this.Upload = Upload
    this.$timeout = $timeout
    this.url = 'http://210.245.30.157:8080/campaign-service/api/xe/import'
    this.username = 'LamTNB'
    this.position = ''
    this.EVENTS = EVENTS
    this.errorMsg = ''
    this.chiendich = ''
  }
  $onInit() {

  }
  onChange() {
    this.upload(this.filecsv)
  }
  upload(file) {
    if (file) {
      file.upload = this.Upload.upload({
        url: this.url,
        data: { username: this.username, file: file }
      })
      file.upload.then(function (response) {
        console.log('file ' + response['config']['data']['file']['name'] + 'is uploaded successfully. Response: ' + response['data'])
        // console.log(response)
        // this.$timeout(function () {
        //   file.result = response.data
        //   console.log(file.result)
        // })
      //   file.result = response['config']['data']['file']['name']
      //   this.result = file.result
        file.result = response.status
      }, function (response) {
        if (response.status > 0) {
          if (response.status === 500) {
            file.result = response.status
          }
        }
      }, function (evt) {
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
      })
    }
  }
  stopUpload() {
    console.log(`click`)
    this.filecsv.upload.abort()
  }
}

export default UploadCSVController
