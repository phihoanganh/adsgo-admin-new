import controller from './upload-csv.controller'
import style from './upload-csv.scss'
import template from './upload-csv.html'

let moduleName = 'app.component.directive.upload-csv'

angular.module(moduleName, [])
  .component('uploadcsv', {
    controller,
    template,
    style,
    controllerAs: 'vm',
    bindings: {
      filecsv: '<'
    }
  })

export default moduleName
