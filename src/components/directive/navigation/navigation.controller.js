/**
 * Created by hangntt10 on 24/10/2017.
 */

class NavigationController {
  constructor($scope, $state, $location, $cookies, AuthenticationService, CookieService, STATE_NAME, EVENTS) {
    'ngInject'
    this.menuItems = ['TongQuan', 'QuanLyChienDich', 'QuanLyXe', 'QuanLyTaiXe', 'QuanLyKhachHang', 'QuanLyCauHinh', 'TaiKhoanNoiBo']
    this.setActive = 'QuanLyChienDich'
    this.$scope = $scope
    this.$location = $location
    this.$cookies = $cookies
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.AuthenticationService = AuthenticationService
    this.CookieService = CookieService
    this.accessList = {}
    this.isLogin = false
    this.listRole = []
    this.activeMenu = ''
  }
  $onInit() {
    // this.activeMenu = 'QuanLyChienDich'
    this.isLogin = this.AuthenticationService.isAuthenticated()
    this.AuthenticationService.getUserRole().then((res) => {
      this.listRole = res
      console.log(res)
    })
    this.$scope.$on(this.EVENTS.SET_ACTIVE_MENU, (event, data) => {
      this.activeMenu = data
    })
  }
  setActive(item) {
    console.log(this.$location.path())
    this.activeMenu = item
    console.log(item)
  }
  redirecLogin() {
    this.$state.go(this.STATE_NAME.LOGIN.state)
  }
  logout() {
    this.CookieService.removeToken()
    this.redirecLogin()
  }
  checkRole(role) {
    if (this.listRole.indexOf(role) >= 0) {
      return true
    }
    return false
  }
}
export default NavigationController
