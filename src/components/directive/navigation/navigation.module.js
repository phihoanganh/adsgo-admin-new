import controller from './navigation.controller'
import template from './navigation.html'
import './navigation.scss'

const moduleName = 'app.component.directive.navigation'

/* global angular:true */

angular.module(moduleName, [])
  .component('navigation', {
    controller,
    template,
    controllerAs: 'vm'
  })
export default moduleName
