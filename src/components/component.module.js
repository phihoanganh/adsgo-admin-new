import pageModule from './pages/page.module'
import commonModule from './common/common.module'
import directiveModule from './directive/directive.module'
import layoutModule from './layouts/layouts.module'

const moduleName = 'app.component'

/* @ngInject */
angular.module(moduleName, [pageModule, commonModule, directiveModule, layoutModule])

export default moduleName
