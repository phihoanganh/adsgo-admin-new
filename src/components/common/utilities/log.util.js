import DateUtil from './datetime.util'
class Log {
  static printOut (...message) {
    if (__DEBUG_MODE === 'debug') {
      const args = Array.prototype.slice.call(message)
      console.log.apply(console, [DateUtil.format(new Date(), 'h:mm:ss')].concat(args))
    }
  }
}

export default Log
