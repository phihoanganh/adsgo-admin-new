class ModalController {

  constructor($scope, $uibModalInstance, $timeout, focusController) {
    'ngInject'
    this.$uibModalInstance = $uibModalInstance
    this.focusController = focusController
    this.$timeout = $timeout
    this.$scope = $scope
    this.rendered()
  }

  cancel() {
    this.$uibModalInstance.dismiss('cancel')
  }

  confirm() {
    this.$uibModalInstance.close({$value: 'ok'})
  }

  rendered() {
    this.$uibModalInstance.rendered.then(() => {
      this.focusController.setDepth(5)
      this.focusController.focus('cancel')
    })
  }
}

export default ModalController
