/**
 * Created by DungTQ9 on 12/19/2017.
 */

class MenuPanelController {
  constructor(mdPanelRef, EVENTS) {
    'ngInject'
    this.mdPanelRef = mdPanelRef
    this.menuPanelEvents = EVENTS.MENU_PANEL
  }

  selectItem(item, path, documentName) {
    if (item.indexOf(this.menuPanelEvents.PREVIEW.toLowerCase()) !== -1) {
      console.log('Preview >> ', item)
      window.open(path, 'Preview')
    } else {
      console.log('Download >> ', item)
      this.downloadFile(path, documentName)
    }
    this.closePanel()
  }

  closePanel() {
    const panelRef = this.mdPanelRef
    panelRef && panelRef.close().then(() => {
      panelRef.destroy()
    })
  }

  downloadFile(path, fileName) {
    const link = this.buildLinkDownload(path, fileName)
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }

  buildLinkDownload(path, fileName) {
    const link = document.createElement('a')
    link.download = fileName
    link.href = path
    return link
  }
}
export default MenuPanelController
