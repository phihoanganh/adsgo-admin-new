/**
 * Created by SamCM1 on 6/28/2017.
 */
import fastXmlParser from '../../../../node_modules/fast-xml-parser'
export default class SMILHelper {
  static getOptions() {
    return {
      attrPrefix: '',
      textNodeName: '#text',
      ignoreNonTextNodeAttr: false,
      ignoreTextNodeAttr: false,
      ignoreNameSpace: false,
      textNodeConversion: true
    }
  }
  static xmlToJson(xmlData) {
    const options = this.getOptions()
    if (fastXmlParser.validate(xmlData) === true) {
      var jsonObj = fastXmlParser.parse(xmlData, options)
      return jsonObj
    }
  }
}
