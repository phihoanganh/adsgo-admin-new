import compareAsc from 'date-fns/compare_asc'
import compareDesc from 'date-fns/compare_desc'
import distanceInWordsStrict from 'date-fns/distance_in_words_strict'
import format from 'date-fns/format'
import isDate from 'date-fns/is_date'
import isValid from 'date-fns/is_valid'
import getTime from 'date-fns/get_time'
import getHours from 'date-fns/get_hours'
import addMinutes from 'date-fns/add_minutes'
import setMinutes from 'date-fns/set_minutes'
import setHours from 'date-fns/set_hours'
import getMinutes from 'date-fns/get_minutes'
import getSeconds from 'date-fns/get_seconds'

export default class DateTimeUtil {
  // Compare the two dates and return 1 if the first date is after the second,
  // -1 if the first date is before the second or 0 if dates are equal.
  static compareAsc(dateLeft, dateRight) {
    return compareAsc(dateLeft, dateRight)
  }

  /* Compare the two dates and return -1 if the first date is after the second,
   1 if the first date is before the second or 0 if dates are equal. */
  static compareDesc(dateLeft, dateRight) {
    return compareDesc(dateLeft, dateRight)
  }

  static distanceInWordsStrict(dateToCompare, date, [options]) {
    return distanceInWordsStrict(dateToCompare, date, [options])
  }

  // Format date with pattern
  static format(date, pattern) {
    return format(date, pattern)
  }

  // Check if is date
  static isDate(argument) {
    return isDate(argument)
  }

  // Check valid date
  static isValid(date) {
    return isValid(date)
  }

  // Get timestamp of date
  static getTime(date) {
    return getTime(date)
  }

  static getCurrentHour() {
    return getHours(new Date())
  }

  static getCurrrentMinutes() {
    return getMinutes(new Date())
  }

  static getCurrrentSeconds() {
    return getSeconds(new Date())
  }

  static getHourString(amount) {
    const dateH = setMinutes(new Date(), 0)
    const result = addMinutes(dateH, amount)
    return this.format(result, 'h:mm A')
  }

  static getDateWithCurrentHour(hour, min) {
    const dateH = setHours(new Date(), hour)
    var date = setMinutes(dateH, min)
    return date
  }

  static convertDate(input) {
    console.log('inside convert...........')
    function pad(s) { return (s < 10) ? '0' + s : s }
    var d = new Date(input)
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/')
  }

  formatMilisecondsToMinutes(miliseconds) {
    return Math.round(miliseconds / 1000 / 60)
  }
}
