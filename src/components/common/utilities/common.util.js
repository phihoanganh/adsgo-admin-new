/**
 * Created by dungtq9 on 12/26/2017.
 */

export default class CommonUtil {
  constructor($rootScope, EVENTS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.EVENTS = EVENTS
  }

  scrollUp(element) {
    element.scrollIntoView()
  }

  menuEvent(scope, isExpend) {
    scope.$emit(this.EVENTS.EXPAND_MENU, isExpend)
  }

  downloadFile(path, fileName) {
    const link = this.buildLinkDownload(path, fileName)
    const clickEvent = new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: false
    })
    link.dispatchEvent(clickEvent)
    // document.body.appendChild(link)
    // link.click()
    // document.body.removeChild(link)
  }

  downloadZIP(blobData, fileName) {
    const link = this.buildBlobDownload(blobData, fileName)
    const clickEvent = new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: false
    })
    link.dispatchEvent(clickEvent)
    // document.body.appendChild(link)
    // link.click()
    // document.body.removeChild(link)
  }

  buildLinkDownload(path, fileName) {
    const link = document.createElement('a')
    link.download = fileName
    link.href = path
    return link
  }

  buildBlobDownload(blobData, fileName) {
    const urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL
    const blob = new Blob([blobData], {type: 'application/octet-stream;charset=UTF-8'})
    const link = document.createElement('a')
    link.download = fileName
    link.href = urlCreator.createObjectURL(blob)
    return link
  }
}
