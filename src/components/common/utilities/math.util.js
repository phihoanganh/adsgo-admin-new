class MathUtil {
  /**
   * Get a random floating point number between `min` and `max`.
   *
   * @param {number} min - min number
   * @param {number} max - max number
   * @return {float} a random floating point number
   */
  static getRandom(min, max) {
    return Math.random() * (max - min) + min
  }

  /**
   * Get a random integer between `min` and `max`.
   *
   * @param {number} min - min number
   * @param {number} max - max number
   * @return {int} a random integer
   */
  static getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  static formatTime(seconds) {
    if (seconds >= 0) {
      var hh = Math.floor(seconds / 3600),
        mm = Math.floor(seconds / 60) % 60,
        ss = Math.floor(seconds) % 60
      return (hh ? (hh < 10 ? '0' : '') + hh + ':' : '') + ((mm < 10) ? '0' : '') + mm + ':' + ((ss < 10) ? '0' : '') + ss
    }
  }

  static formatTimeFromTimestamp(timestamp) {
    if (timestamp > 0) {
      let date = new Date(timestamp)
      let hours = date.getHours()
      let minutes = date.getMinutes()
      if (hours > 0 && hours < 13) {
        return (hours < 10 ? '0' : '') + hours + ':' + (minutes < 10 ? '0' : '') + 'AM'
      } else {
        return hours + ':' + (minutes < 10 ? '0' : '') + 'PM'
      }
    }
  }
}

export default MathUtil
