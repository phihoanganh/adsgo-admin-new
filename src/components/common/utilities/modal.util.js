import ModalController from './modal.controller'

class ModalUtil {

  constructor(focusController, $uibModal, $timeout, $uibModalStack) {
    'ngInject'
    this.$uibModal = $uibModal
    this.focusController = focusController
    this.$timeout = $timeout
    this.$uibModalStack = $uibModalStack
    this.isOpenModal = false
  }

  openMessageConfirmModal(messageKey, confirmCallback) {
    if (!this.isOpenModal) {
      console.log(messageKey)
      let template = `<modal message="${messageKey}" confirm="vm.confirm()" type="message-confirm"/>`

      this.createModal(template, confirmCallback)
    }
  }

  /**
   * function to open bootstrap modal
   * @param $timeout
   * @param $uibModal
   * @param focusController
   * @param messageKey
   * @param focusDepth
   * @param isOpenModal
   */
  openModal(messageKey, isMessageDialog = false, isWelcomeDialog, cancelCallback, confirmCallback, cancelText, confirmText) {
    if (!this.isOpenModal) {
      console.log(messageKey)
      let template = `<modal message="${messageKey}" cancel="vm.cancel()" confirm="vm.confirm()" type="confirm"/>`

      if (isMessageDialog) {
        template = `<modal message="${messageKey}" cancel="vm.cancel()" type="message"/>`
      }

      if (cancelText && confirmText) {
        template = `<modal message="${messageKey}" cancel="vm.cancel()" confirm="vm.confirm()" type="confirm" cancel-text="${cancelText}" confirm-text="${confirmText}"/>`
      }

      if (isWelcomeDialog) {
        template = `<modal message="${messageKey}"/>`
      }

      this.createModal(template, confirmCallback, cancelCallback)
    }
  }

  createModal(template, confirmCallback, cancelCallback) {
    const modalInstance = this.$uibModal.open({
      template,
      size: 'sm',
      controllerAs: 'vm',
      controller: ModalController,
      backdrop: 'static',
      keyboard: false,
      animation: false
    })
    this.isOpenModal = true

    // Handle action after CONFIRM / CANCEL Modal
    modalInstance.result.then((value) => {
      if (confirmCallback) {
        confirmCallback(value)
      }
      this.isOpenModal = false
      this.focusController.setDepth(1)
    }, () => {
      if (cancelCallback) {
        cancelCallback()
      }
      this.isOpenModal = false
      this.focusController.setDepth(1)
    })
  }

  isModalOpened() {
    return this.isOpenModal
  }

  setOpenModal(mVal) {
    this.isOpenModal = mVal
  }

  closeAllModal() {
    this.$uibModalStack.dismissAll()
  }
}

export default ModalUtil
