/**
 * Created by DungTQ9 on 12/19/2017.
 */
import MenuPanelController from './menu-panel/menu-panel.controller'
import './menu-panel/menu-panel.scss'

class MenuPanelUtil {
  constructor($mdPanel) {
    'ngInject'
    this.$mdPanel = $mdPanel
  }

  openMenuPanel(event, menus, document, relativeClass) {
    var position = this.$mdPanel.newPanelPosition().relativeTo(`.${relativeClass}`).addPanelPosition(this.$mdPanel.xPosition.ALIGN_START, this.$mdPanel.yPosition.BELOW)
    const template = `<div class="menu-panel" tw-click-outside="vm.closePanel()" role="listbox"><div class="menu-panel-item" tabindex="-1" role="option" ng-repeat="item in vm.menus" ng-click="vm.selectItem(item, '${document.path}', '${document.name}')">{{item | translate}}</div></div>`
    const config = {
      attachTo: angular.element('body'),
      controller: MenuPanelController,
      controllerAs: 'vm',
      template: template,
      position: position,
      panelClass: 'menu-panel',
      openFrom: event,
      locals: {
        'menus': menus
      },
      clickOutsideToClose: true,
      escapeToClose: true,
      focusOnOpen: false,
      zIndex: 2
    }
    this.$mdPanel.open(config)
  }

}
export default MenuPanelUtil
