/**
 * Created by NguyenNL on 8/23/2017.
 */
import ModalUtil from './modal.util'
import MenuPanelUtil from './menu-panel.util'
import DateTimeUtil from './datetime.util'
import CommonUtil from './common.util'

/* global angular:true */

const moduleName = 'app.component.common.utilities'

angular.module(moduleName, [])
  .service('ModalUtil', ModalUtil)
  .service('DateTimeUtil', DateTimeUtil)
  .service('MenuPanelUtil', MenuPanelUtil)
  .service('CommonUtil', CommonUtil)

export default moduleName
