const headerConfig = {
  JWT_TOKEN_HEADER: {
    header: 'JWT-TOKEN'
  }
}
let $this

class AuthInterceptor {
  constructor($q, $injector, STATE_NAME, $timeout, AccountService, CookieService) {
    'ngInject'
    // inject dependencies in interceptor
    this.$q = $q
    this.STATE_NAME = STATE_NAME
    this.$injector = $injector
    this.accountService = AccountService
    this.CookieService = CookieService
    this.$timeout = $timeout
    $this = this
  }

  /**
   * Handle before sending request to API endpoint
   * @param config
   * @returns {*}
   */
  request(config) {
    config.headers = config.headers || {}
    config.timeout = config.timeout || 20000
    let token = $this.CookieService.getToken()
    let timeout = $this.accountService.getTimeOutValue()
    if (token) {
      config.headers[headerConfig.JWT_TOKEN_HEADER.header] = 'Bearer ' + token
      config.timeout = parseInt(timeout)
    }
    return config || $this.$q.when(config)
  }

  /**
   * Handle error request before sending to API endpoint
   * @param rejection
   */
  requestError(rejection) {
    return $this.$q.reject(rejection)
  }

  /**
   * Handle after success response from API endpoint
   * @param config
   * @returns {*}
   */
  response(response) {
    return response || $this.$q.when(response)
  }

  /**
   * Handle the error response from API endpoint
   * @param rejection
   */
  responseError(rejection) {
    const $state = $this.$injector.get('$state')
    const accountService = $this.$injector.get('AccountService')
    switch (rejection.status) {
      case 401:
        accountService.clearAccountInfo()
        $state.go($this.STATE_NAME.LOGIN_STATE.state)
        break
      case 404:
        window.history.back()
        break
    }
    return $this.$q.reject(rejection)
  }
}

export default AuthInterceptor
