import AuthInterceptor from './interceptor'

/* global angular:true */
const moduleName = 'app.component.common.interceptor'

angular.module(moduleName, [])
  .service('AuthInterceptor', AuthInterceptor)
  .config(['$httpProvider', function ($httpProvider) {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
    // set default header for each Request before sending
    $httpProvider.defaults.headers.common = headers
    $httpProvider.defaults.headers.post = {}
    $httpProvider.interceptors.push('AuthInterceptor')
  }])

export default moduleName
