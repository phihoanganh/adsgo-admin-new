/* global angular:true */
const moduleName = 'app.component.common.services'

import restModule from './rest/rest.module'
import businessModule from './business/business.module'
import utilModule from './utils/util.module'

angular.module(moduleName, [restModule, businessModule, utilModule])

export default moduleName
