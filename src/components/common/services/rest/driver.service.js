/**
 * Created by DungTQ9 on 12/18/2017.
 */
import BaseService from '../base-services'

class DriverService extends BaseService {
  constructor($http, $q, AuthenticationService, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.AuthenticationService = AuthenticationService
  }

  danhSachTen() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.KHACHHANG.danhSachTen}`
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  filterDriver(searchKey = '', thanhPho = '', page = 1, count = 10) {
    var params = {}
    if (searchKey !== '') {
      params['searchKey'] = searchKey
    }
    if (thanhPho !== '') {
      params['thanhPho'] = thanhPho
    }
    params['currentPage'] = page
    params['nbItem'] = count
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.DRIVER.filterDriver}`,
      params: params
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  getDriverById(cmnd) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.DRIVER.getDriverById}` + cmnd
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  editDriver(data) {
    if (!data.bienKiemSoat) {
      data.bienKiemSoat = ''
    }
    var driver = {
      'bienKiemSoat': data.bienKiemSoat,
      'cmnd': data.cmnd,
      'sdt': data.sdt,
      'tenTaiXe': data.tenTaiXe
    }
    console.log(driver)
    return this.$http({
      method: 'put',
      url: `${this.baseUrl}${this.restPath.DRIVER.editDriver}`,
      data: JSON.stringify(driver),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  deleteDriver(id) {
    return this.$http({
      method: 'DELETE',
      url: `${this.baseUrl}${this.restPath.DRIVER.deleteDriver}/` + id
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  addDriver(data) {
    return this.$http({
      method: 'post',
      url: `${this.baseUrl}${this.restPath.DRIVER.addDriver}`,
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
}
export default DriverService
