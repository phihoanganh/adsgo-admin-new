import BaseService from '../base-services'
class QuangCaoService extends BaseService {
  constructor($http, $q, AuthenticationService, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.AuthenticationService = AuthenticationService
  }

  listAdsKindDetail() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.QUANGCAO.path}`
    }).then((successResponse) => {
      return successResponse.data // return data after call API endpoint successfully
    }, (errorResponse) => {
      return Promise.reject(errorResponse) // return a reject Promise and handle this in interceptor
    })
  }
  getGeneralArgument() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.QUANGCAO.generalArgumentPath}`
    }).then((successResponse) => {
      return successResponse.data // return data after call API endpoint successfully
    }, (errorResponse) => {
      return Promise.reject(errorResponse) // return a reject Promise and handle this in interceptor
    })
  }
  UpdateViewsArgs(data) {
    let listArgs = []
    data.forEach(item => {
      listArgs.push({
        'idLoaiQuangCao': item.idLoaiQuangCao,
        'view4Cho': parseFloat(item.view4Cho),
        'view7Cho': parseFloat(item.view7Cho)
      })
    })
    return this.$http({
      method: 'put',
      url: `${this.baseUrl}${this.restPath.QUANGCAO.updateViewsArgsPath}`,
      data: JSON.stringify(listArgs),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  UpdateGeneralArgs(data) {
    let listArgs = []
    data.forEach(item => {
      listArgs.push({
        'id': item.idLoaiQuangCao,
        'giaTri': parseFloat(item.giaTri)
      })
    })
    return this.$http({
      method: 'put',
      url: `${this.baseUrl}${this.restPath.QUANGCAO.updateGeneralArgsPath}`,
      data: JSON.stringify(listArgs),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
}
export default QuangCaoService
