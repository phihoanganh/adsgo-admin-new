import BaseService from '../base-services'

class AuthenticationService extends BaseService {

  constructor($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH, CookieService) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.CookieService = CookieService
  }
  isAuthenticated() {
    if (this.CookieService.getToken()) {
      return true
    }
    return false
  }
  authenticate(username, password, remember = false) {
    return this.$http.post(this.baseUrl + this.restPath.AUTHENTICATION.path, {
      'username': username,
      'password': password
    }).then((res) => {
      if (res.token !== null) {
        console.log('login successfully: \n' + res.data.token)
        this.CookieService.setToken(res.data.token)
        if (remember) {
          this.saveAccountInfo(username, password)
        }
      } else {
        console.log('login fail: \n' + res)
      }
      return true
    }, () => {
      return false
    })
  }

  getUserRole() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.listRole}`
    }).then((successResponse) => {
      var listRole = []
      if (successResponse.data.authorities) {
        successResponse.data.authorities.forEach(element => {
          listRole.push(element.authority)
        })
      }
      return listRole
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
}

export default AuthenticationService
