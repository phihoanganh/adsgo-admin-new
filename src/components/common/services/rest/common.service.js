/**
 * Created by ThangHN2 on 2/1/2018.
 */
import BaseService from '../base-services'

class CommonService extends BaseService {
  constructor($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH, DEFAULT_VALUE) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.default = DEFAULT_VALUE
  }
  getListAreas() {
    return this.default.LIST_AREAS
  }
  getDongXe() {
    return this.default.DONG_XE
  }
  getLoaiXe() {
    return this.default.LOAI_XE
  }
  getHangModel() {
    return this.default.HANG_MODEL
  }
  getRole() {
    return this.default.ROLE
  }
}
export default CommonService
