/**
 * Created by DungTQ9 on 12/18/2017.
 */
import BaseService from '../base-services'

class HoTroService extends BaseService {
  constructor($http, $q, CookieService, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.CookieService = CookieService
  }
  listHoTro(chienDichId) {
    var params = {'chienDichId': chienDichId}
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.HOTRO.listHoTro}`,
      params: params
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  addSupportRequest(content) {
    let campaignId = this.CookieService.getCampainId()
    let data = {
      'id': campaignId,
      'content': content
    }
    return this.$http({
      method: 'post',
      url: `${this.baseUrl}${this.restPath.HOTRO.themHoTro}`,
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  updateSupportRequest(noti) {
    let data = {
      notificationId: noti.id,
      ghiChu: noti.ghiChu,
      trangThai: noti.trangThai
    }
    return this.$http({
      method: 'put',
      url: `${this.baseUrl}${this.restPath.HOTRO.updateHoTro}`,
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
}
export default HoTroService
