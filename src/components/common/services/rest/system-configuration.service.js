import BaseSerivce from '../base-services'

class SystemConfigurationService extends BaseSerivce {
  constructor($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
  }

  getPreSignedUrl(fileName) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.SYSTEM_CONFIGURATION.preSignedUrl}`,
      params: {
        fileName: fileName
      }
    }).then((successResponse) => {
      return successResponse.data // return data after call API endpoint successfully
    }, (errorResponse) => {
      return Promise.reject(errorResponse) // return a reject Promise and handle this in interceptor
    })
  }
}
export default SystemConfigurationService
