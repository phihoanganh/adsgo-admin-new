/**
 * Created by DungTQ9 on 12/18/2017.
 */
import BaseService from '../base-services'

class CategoryService extends BaseService {
  constructor($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
  }

  getCategories() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.CATEGORY.getCategoriesPath}`
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
}
export default CategoryService
