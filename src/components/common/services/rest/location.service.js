import BaseService from '../base-services'
class LocationService extends BaseService {
  constructor($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
  }

  getLocation() {
    return this.$http({
      method: 'GET',
      url: './data/location.json'
    }).then((successResponse) => {
      return successResponse // return data after call API endpoint successfully
    }, (errorResponse) => {
      return Promise.reject(errorResponse) // return a reject Promise and handle this in interceptor
    })
  }
  getProvices() {
    return this.$http({
      method: 'GET',
      url: './data/provinces.json'
    }).then((successResponse) => {
      return successResponse // return data after call API endpoint successfully
    }, (errorResponse) => {
      return Promise.reject(errorResponse) // return a reject Promise and handle this in interceptor
    })
  }
}

export default LocationService
