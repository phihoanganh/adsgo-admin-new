import BaseService from '../base-services'
class ImageReportService extends BaseService {
  constructor($http, $q, CONTEXT_PATH, DateUtils, CookieService, REST_BASE_URL, REST_PATH, DEFAULT_VALUE, DRIVER_REPORT_STT) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.CookieService = CookieService
    this.default = DEFAULT_VALUE
    this.DateUtils = DateUtils
    this.REST_PATH = REST_PATH
    this.DRIVER_REPORT_STT = DRIVER_REPORT_STT
  }

  getDanhSachBaoCaoHinh(tinhTrang = null, khachHang = null) {
    var params = {}
    // params['chienDichID'] = this.CookieService.getCampainId()
    // params['chienDichID'] = '15'

    if (tinhTrang !== null) {
      params['tinhTrang'] = tinhTrang
    }

    if (khachHang !== null) {
      params['khachHang'] = khachHang
    }
    // params['page'] = page
    // params['count'] = count
    // params['sortField'] = sortField
    // params['sortType'] = 'ASC'
    // console.log(params)
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.BAOCAOHINH.getListBaoCaoHinh}`,
      params: params
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  getReportDetail(imageReportId) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.BAOCAOHINH.getReportDetail}`,
      params: {
        imageReportId
      }
    }).then((successResponse) => {
      let res = successResponse.data
      let count = {chua_co_anh: 0, chua_duyet: 0, da_duyet: 0, tu_choi: 0}
      res.forEach(item => {
        item['urlReportShow'] = {}
        if (item.urlReport.length > 0 && item.urlReport[0].trim() !== '') {
          item.urlReport = item.urlReport[0]
          let listUrls = item.urlReport.split(',')
          listUrls.forEach(url => {
            if (url !== '') {
              let items = url.split('_')
              if (items.length > 1) {
                item['urlReportShow'][items[0]] = this.REST_PATH.AWS_IMAGE.imageBaseUrl + url
              }
            }
          })
        } else {
          item.urlReport = ''
        }
        item.createdDate = this.DateUtils.convertDate(item.createdDate)
        item.soCho = item.soCho + ' chỗ'
        if (item.trangThaiReport === this.DRIVER_REPORT_STT.CHUA_CO_ANH) {
          count.chua_co_anh += 1
        } else if (item.trangThaiReport === this.DRIVER_REPORT_STT.CHUA_DUYET) {
          count.chua_duyet += 1
        } else if (item.trangThaiReport === this.DRIVER_REPORT_STT.DA_DUYET) {
          count.da_duyet += 1
        } else {
          count.tu_choi += 1
        }
      })
      return {'data': res, 'count': count}
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  getReportNote(imageReportId) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.BAOCAOHINH.getReportNote}`,
      params: {
        imageReportId
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  requestReport(chienDichId, ghiChu, phanHoi = '', reportXe, soLuongXe) {
    return this.$http({
      method: 'POST',
      url: `${this.baseUrl}${this.restPath.BAOCAOHINH.requestReport}`,
      data: {
        chienDichId,
        ghiChu,
        phanHoi,
        reportXe,
        soLuongXe
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  addImageReportDetail(imageReportId, bienKiemSoats) {
    return this.$http({
      method: 'POST',
      url: `${this.baseUrl}${this.restPath.BAOCAOHINH.addImageReportDetail}`,
      data: {
        id: imageReportId,
        bienKiemSoats
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  updateImageReport(imageReport) {
    return this.$http({
      method: 'POST',
      url: `${this.baseUrl}${this.restPath.BAOCAOHINH.updateImageReport}`,
      data: {
        imageReportId: imageReport.id,
        phanHoi: imageReport.phanHoi,
        tinhTrang: imageReport.tinhTrang
      }
    }).then((successResponse) => {
      return successResponse.data[0]
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  updateImageReportDetail(report) {
    return this.$http({
      method: 'PUT',
      url: `${this.baseUrl}${this.restPath.BAOCAOHINH.updateImageReportDetail}`,
      data: {
        id: report.id,
        label: report.label,
        trangTraiReport: report.trangThaiReport,
        urlReport: report.urlReport
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
}

export default ImageReportService
