/**
 * Created by ChinhHT1 on 12/7/2017.
 */

import BaseService from '../base-services'

class BannerService extends BaseService {
  constructor($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
  }

  getBanners(type, limit) {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.COURSE.path,
      params: {
        type: type,
        limit: limit
      }
    }).then(res => {
      return res.data
    }, err => {
      return Promise.reject(err)
    })
  }
}

export default BannerService

