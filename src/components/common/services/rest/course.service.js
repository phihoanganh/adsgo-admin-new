import BaseService from '../base-services'

class CourseService extends BaseService {

  constructor($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
  }

  getCourseDetails(id) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.COURSE.path}/${id}`
    }).then((successResponse) => {
      console.log(successResponse)
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getRecommendCourseBannerIMG() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.COURSE.banner}`
    }).then((successResponse) => {
      console.log(successResponse)
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getRecommendCourse(id) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.COURSE.path}`,
      params: {
        size: 5,
        exclude: id,
        type: 'HOT'
      }
    }).then((successResponse) => {
      return successResponse.data // return data after call API endpoint successfully
    }, (errorResponse) => {
      return Promise.reject(errorResponse) // return a reject Promise and handle this in interceptor
    })
  }

  getListCourse(page, size, type, exclude) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.COURSE.path}`,
      params: {
        size: size,
        page: page,
        type: type,
        exclude: exclude
      }
    }).then((successResponse) => {
      return successResponse.data // return data after call API endpoint successfully
    }, (errorResponse) => {
      return Promise.reject(errorResponse) // return a reject Promise and handle this in interceptor
    })
  }

  searchCourse(page, size, search) {
    page = page - 1
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.COURSE.search,
      data: search,
      params: {
        page: page,
        size: size
      }
    }).then((successResponse) => {
      console.log(successResponse)
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  searchByCourseName(courseName, categoryID, page, size) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.COURSE.search}`,
      params: {
        keyword: courseName,
        categoryId: categoryID,
        page: page,
        size: size
      }
    }).then((successResponse) => {
      console.log(successResponse)
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getRoadMap() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.COURSE.roadMap}`
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
}

export default CourseService
