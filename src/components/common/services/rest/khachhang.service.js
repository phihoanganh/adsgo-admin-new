/**
 * Created by DungTQ9 on 12/18/2017.
 */
import BaseService from '../base-services'

class KhachHangService extends BaseService {
  constructor($http, $q, AuthenticationService, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.AuthenticationService = AuthenticationService
  }

  danhSachTen() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.KHACHHANG.danhSachTen}`
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  locKhachHang(searchKey = '', page = 1, count = 1) {
    var params = {}
    if (searchKey !== '') {
      params['searchKey'] = searchKey
    }
    params['currentPage'] = page
    params['nbItem'] = count
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.KHACHHANG.locKhachHang}`,
      params: params
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  deleteKH(id) {
    return this.$http({
      method: 'DELETE',
      url: `${this.baseUrl}${this.restPath.KHACHHANG.deleteKH}` + id
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  AddCustomer(data) {
    var customer = data
    customer.khachHangCapTren = '1'
    customer.taiKhoan = {
      'loaiTaiKhoan': 'khach hang',
      'trangThaiTK': 'enable'}
    return this.$http({
      method: 'post',
      url: `${this.baseUrl}${this.restPath.KHACHHANG.addEditCustomer}`,
      data: JSON.stringify(customer),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  EditCustomer(data) {
    var customer = data
    customer.khachHangCapTren = '1'
    customer.taiKhoan = {
      'loaiTaiKhoan': 'khach hang',
      'trangThaiTK': 'enable'}
    delete customer.countAllItem
    delete customer.statusRecord
    delete customer.createDate
    console.log(customer)
    return this.$http({
      method: 'put',
      url: `${this.baseUrl}${this.restPath.KHACHHANG.addEditCustomer}`,
      data: JSON.stringify(customer),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  getCustomerById(id) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.KHACHHANG.addEditCustomer}` + id
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
}
export default KhachHangService
