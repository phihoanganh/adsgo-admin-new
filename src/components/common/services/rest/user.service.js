import BaseService from '../base-services'

class UserService extends BaseService {

  constructor($resource, $http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.$resource = $resource
  }

  getUser() {
    return this.$resource(this.baseUrl + this.restPath.USERS.path + '/:id', {id: '@id'}, {
      update: {
        method: 'PUT'
      },
      resetPassword: {
        method: 'PUT',
        url: this.baseUrl + this.restPath.USERS.path + '/:id/password',
        params: {
          id: '@id'
        }
      }
    }).promise()
  }

  getUserById(id) {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.USERS.path + '/' + id
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  checkUserExist(user) {
    return this.$http({
      method: 'POST',
      url: this.baseUrl + this.restPath.USERS.path + '/check-user-existed',
      data: JSON.stringify(user),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getCurrentUser() {
    return this.$http({
      method: 'POST',
      url: this.baseUrl + this.restPath.USERS.path + '/getCurrentUser',
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  changePassword(user) {
    return this.$http({
      method: 'POST',
      url: this.baseUrl + this.restPath.USERS.path + '/change-password',
      data: JSON.stringify(user),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  checkEmailExist(user) {
    return this.$http({
      method: 'POST',
      url: this.baseUrl + this.restPath.USERS.path + '/check-email-existed',
      data: JSON.stringify(user),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  createUser(user) {
    return this.$http({
      method: 'POST',
      url: this.baseUrl + this.restPath.USERS.path,
      data: JSON.stringify(user),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  updateUserGroup(user, type) {
    return this.$http({
      method: 'POST',
      url: this.baseUrl + this.restPath.USERS.addUTG,
      data: JSON.stringify(user),
      params: {
        type: type
      },
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  // Get filter user
  getUsers(page, size, excludeGroups) {
    page = page - 1
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.USERS.path,
      params: {
        page: page,
        size: size,
        excludeGroups: excludeGroups
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getUserReservationFilter() {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.USERS.path + '/get-user-reservation-filter'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getUseAuditLogFilter() {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.USERS.path + '/get-user/audit-log'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getAllUsers(page, size, sort) {
    page = page - 1
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.USERS.path + '/all',
      params: {
        page: page,
        size: size,
        sort: sort
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  resetPassword(user) {
    return this.$http({
      method: 'POST',
      url: this.baseUrl + this.restPath.USERS.path + '/reset-password',
      data: JSON.stringify(user),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  // Assign Users To Group
  setGroups(usersGroupsObject) {
    return this.$http({
      method: 'PUT',
      url: this.baseUrl + this.restPath.USERS.path + '/set-groups',
      data: JSON.stringify(usersGroupsObject),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  checkAnonymousUser(username) {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.USERS.checkAnonymous,
      params: {
        userName: username
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  deleteUser(id) {
    return this.$http.delete(this.baseUrl + this.restPath.USERS.delete + '/' + id).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  active(username) {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.USERS.active,
      params: {
        username: username
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  updateReceivedMailStatus(userId, isReceived) {
    return this.$http.put(this.baseUrl + this.restPath.USERS.path + '?userId=' + userId + '&isReceived=' + isReceived).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
}

export default UserService
