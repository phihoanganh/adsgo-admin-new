import BaseService from '../base-services'

class UserRoleService extends BaseService {

  constructor($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
  }

  getRoles(page, size) {
    page = page - 1
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.ROLES.path,
      params: {
        page: page,
        size: size
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getRolesKeyValue() {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.ROLES.path + '/keyValue'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
}

export default UserRoleService
