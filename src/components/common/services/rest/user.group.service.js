import BaseService from '../base-services'

class UserGroupService extends BaseService {

  constructor($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
  }

  getGroups(page, size, excludeGroup) {
    page = page - 1
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.GROUPS.path,
      params: {
        page: page,
        size: size,
        excludeGroup: excludeGroup
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getGroupsKeyValue() {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.GROUPS.path + '/keyValue'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getGroupById(id) {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.GROUPS.path + '/' + id
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  checkGroupExist(group) {
    return this.$http({
      method: 'post',
      url: this.baseUrl + this.restPath.GROUPS.path + '/check-group-existed',
      data: JSON.stringify(group),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  saveOrUpdate(group) {
    return this.$http({
      method: 'post',
      url: this.baseUrl + this.restPath.GROUPS.path,
      data: JSON.stringify(group),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getUserGroups() {
    return this.$http({
      method: 'GET',
      url: this.baseUrl + this.restPath.GROUPS.path + this.restPath.GROUPS.get_groups
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
}

export default UserGroupService
