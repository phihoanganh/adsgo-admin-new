/**
 * Created by DungTQ9 on 12/18/2017.
 */
import BaseService from '../base-services'

class ChienDichService extends BaseService {
  constructor($http, $q, AuthenticationService, CONTEXT_PATH, REST_BASE_URL, REST_PATH, DEFAULT_VALUE) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.default = DEFAULT_VALUE
    this.AuthenticationService = AuthenticationService
  }

  locChienDich(ten = '', startDate = '', endDate = '', khuVuc = '', page = 1, count = 1, sort = this.default.CAMPAIGN_SORT) {
    var params = {}
    if (ten !== '') {
      params['ten'] = ten
    }
    if (startDate !== '') {
      params['startDate'] = startDate
    }
    if (endDate !== '') {
      params['endDate'] = endDate
    }
    if (khuVuc !== '') {
      params['khuVuc'] = khuVuc
    }
    params['page'] = page
    params['count'] = count
    params['sort'] = sort
    // console.log(params)
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.locChienDichPath}`,
      params: params
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  luuChienDich(data) {
    console.log(`${this.baseUrl}${this.restPath.CHIENDICH.luuChienDichPath}`)
    return this.$http({
      method: 'post',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.luuChienDichPath}`,
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  chinhSuaChienDich(data) {
    console.log(`${this.baseUrl}${this.restPath.CHIENDICH.chinhSuaChienDichPath}`)
    return this.$http({
      method: 'post',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.chinhSuaChienDichPath}`,
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getCampaignById(id) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.getChienDichPath}` + id
    }).then((successResponse) => {
      if (successResponse.data.length > 0) {
        return successResponse.data[0]
      }
      return null
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  getCampaignByKHId(id, page, num) {
    var params = {}
    params['id'] = id
    params['currentPage'] = page
    params['nbItem'] = num
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.getChienDichByKHIdPath}`,
      params: params
    }).then((successResponse) => {
      if (successResponse.data.length > 0) {
        return successResponse.data
      }
      return null
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  deleteCampaign(id) {
    return this.$http({
      method: 'DELETE',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.deleteChienDichPath}` + id
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  listAvailableCar(id, data = null) {
    let params = {'idChienDich': id}
    if (data) {
      params.loaiXe = data.loaiXe
      params.dongXe = data.dongXe
      params.soCho = data.soCho
    }
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.listAvailableCars}`,
      params: params
    }).then((successResponse) => {
      // console.log(successResponse)
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  listCampaignCars(id, data = null) {
    let params = {'chienDichId': id}
    if (data) {
      params.loaiXe = data.loaiXe
      params.dongXe = data.dongXe
      params.soCho = data.soCho
    }
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.listCarsInCampaign}`,
      params: params
    }).then((successResponse) => {
      // console.log(successResponse)
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  addCarsToCampaign(id, listCars) {
    let listCarString = listCars.join(';')
    let carData = {
      'idChienDich': id,
      'listBienKiemSoat': listCarString
    }
    return this.$http({
      method: 'post',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.addCarsToCampaign}`,
      data: JSON.stringify(carData),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  removeCarsFromCampaign(id, listCars) {
    let listCarString = listCars.join(';')
    let carData = {
      'bienKiemSoat': listCarString
    }
    return this.$http({
      method: 'delete',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.removeCarsFromCampaign}`,
      params: {'chienDichId': id},
      data: JSON.stringify(carData),
      dataType: 'json'
    }).then((successResponse) => {
      return (successResponse.status === 200)
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  getCampainViewAndKm(chienDichId) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.getTotalViewAndKm}`,
      params: {
        'chienDichId': chienDichId
      }
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  listCampainCarFilter(chienDichId) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.listCampaignCarFilter}/${chienDichId}`
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  saveCampainCarFilter(chienDichId, data) {
    let filter = {
      'chienDichId': chienDichId,
      'dongXe': data.dongXe,
      'loaiXe': data.loaiXe,
      'soCho': data.soCho,
      'tongXeCan': data.tongXeCan,
      'tongXeChon': 0
    }
    return this.$http({
      method: 'POST',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.saveCampaignCarFilter}`,
      data: JSON.stringify(filter),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  updateCampainCarFilter(id, tongXeCan, tongXeChon) {
    let params = {
      'id': id,
      'tongXeCan': tongXeCan,
      'tongXeChon': tongXeChon
    }
    return this.$http({
      method: 'PUT',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.updateCampaignCarFilter}`,
      data: JSON.stringify(params),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  deleteCampainCarFilter(id) {
    return this.$http({
      method: 'DELETE',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.deleteCampaignCarFilter}/${id}`
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  getOverviewInfo() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.CHIENDICH.getOverviewInfo}`
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
}
export default ChienDichService
