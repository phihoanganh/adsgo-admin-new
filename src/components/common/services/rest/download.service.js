import BaseService from '../base-services'

class DownloadService extends BaseService {

  constructor($resource, $http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.$resource = $resource
  }

  downloadDocument(documentId) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.DOCUMENT.downloadBinary}/${documentId}`,
      headers: {
        'Content-Type': 'application/pdf',
        'Accept': 'application/octet-stream'
      },
      responseType: 'arraybuffer'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  downloadZIP(courseId) {
    const url = `${this.baseUrl}${this.restPath.RESOURCE.DOWNLOAD_ZIP.replace('{id}', courseId)}`
    return this.$http({
      method: 'GET',
      url: url,
      // headers: {
      //   'Content-Type': 'application/octet-stream',
      //   'Accept': 'application/octet-stream'
      // },
      responseType: 'arraybuffer'
    }).then((successResponse) => {
      console.log(successResponse)
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
}

export default DownloadService
