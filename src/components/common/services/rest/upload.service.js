/**
 * Created by DungTQ9 on 12/18/2017.
 */
import BaseService from '../base-services'

class UploadService extends BaseService {
  constructor($http, $q, CookieService, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.CookieService = CookieService
  }
  uploadImage(image, category) {
    // let campaignId = this.CookieService.getCampainId()
    // let data = {
    //   'id': campaignId,
    //   'content': content
    // }
    return this.$http({
      method: 'post',
      url: `${this.baseUrl}${this.restPath.HOTRO.themHoTro}`,
    //   data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
}
export default UploadService
