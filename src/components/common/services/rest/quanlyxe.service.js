import BaseService from '../base-services'
class CarService extends BaseService {
  constructor($http, $q, AuthenticationService, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.AuthenticationService = AuthenticationService
    this.REST_PATH = REST_PATH
  }

  layDanhSachTinhTrang() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.QUANLYXE.tinhTrangXe}`
    }).then((successResponse) => {
      return successResponse.data // return data after call API endpoint successfully
    }, (errorResponse) => {
      return Promise.reject(errorResponse) // return a reject Promise and handle this in interceptor
    })
  }

  layDanhSachChienDich() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.QUANLYXE.danhSachTenChienDich}`
    }).then((successResponse) => {
      return successResponse.data // return data after call API endpoint successfully
    }, (errorResponse) => {
      return Promise.reject(errorResponse) // return a reject Promise and handle this in interceptor
    })
  }
  locXe(bienKiemSoat = '', tinhTrang = '', chienDich = '', page = 1, count = 1) {
    var params = {}
    if (bienKiemSoat !== '') {
      params['bienKiemSoat'] = bienKiemSoat
    }
    if (tinhTrang !== '') {
      params['tinhTrangXe'] = tinhTrang
    }
    if (chienDich !== '') {
      params['chienDichId'] = chienDich
    }
    params['currentPage'] = page
    params['items'] = count
    // console.log(params)
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.QUANLYXE.locXe}`,
      params: params
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  deleteXe(id) {
    return this.$http({
      method: 'DELETE',
      url: `${this.baseUrl}${this.restPath.QUANLYXE.deleteXe}` + '?bienKiemSoat=' + id
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }

  AddCar(data) {
    return this.$http({
      method: 'post',
      url: `${this.baseUrl}${this.restPath.QUANLYXE.addXe}`,
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  EditCar(data) {
    console.log('data service')
    console.log(data)
    var params = {}
    params['xe'] = data
    return this.$http({
      method: 'put',
      url: `${this.baseUrl}${this.restPath.QUANLYXE.editXe}/` + '?bienKiemSoat=' + data['bienKiemSoat'],
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  getCarById(id) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.QUANLYXE.getXe}/` + '?bienKiemSoat=' + id
    }).then((successResponse) => {
      // console.log(successResponse)
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }
  listCarReports(chienDichID, search = null, loaiXe = null, sortField = 'bien_kiem_soat') {
    var params = {}
    params['chienDichID'] = chienDichID
    if (search !== null) {
      params['search'] = search
    }

    if (loaiXe !== null) {
      params['loaiXe'] = loaiXe
    }
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.QUANLYXE.getDanhSachXe}`,
      params: params
    }).then((successResponse) => {
      let res = successResponse.data
      res.forEach(item => {
        if (item.urlReport.length > 0 && item.urlReport[0].trim() !== '') {
          let listUrls = item.urlReport[0].split(',')
          item.urlReport = []
          listUrls.forEach(url => {
            if (url !== '') {
              item.urlReport.push(this.REST_PATH.AWS_IMAGE.imageBaseUrl + url)
            }
          })
        } else {
          item.urlReport = []
        }
        item.tongKM = parseInt(item.tongKM)
      })
      return res
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
}

export default CarService
