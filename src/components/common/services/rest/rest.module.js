/* global angular:true */
import angularResource from 'angular-resource'
import ngCookies from 'angular-cookies'
import CommonService from './common.service'
import AuthenticationService from './authentication.service'
import UserService from './user.service'
import UserRoleService from './user.role.service'
import UserGroupService from './user.group.service'
import SystemConfigurationService from './system-configuration.service'
import CourseService from './course.service'
import DownloadService from './download.service'
import BannerService from './banner.service'
import CategoryService from './category.service'
import ChienDichService from './chiendich.service'
import KhachHangService from './khachhang.service'
import QuangCaoService from './quangcao.service'
import LocationService from './location.service'
import CarService from './quanlyxe.service'
import RoleService from './role.service'
import CookieService from './cookie.service'
import DriverService from './driver.service'
import HoTroService from './hotro.service'
import UploadService from './upload.service'
import ImageReportService from './imagereport.service'

const moduleName = 'app.component.common.services.rest'

angular.module(moduleName, [angularResource, ngCookies])
  .service('CommonService', CommonService)
  .service('AuthenticationService', AuthenticationService)
  .service('UserService', UserService)
  .service('UserRoleService', UserRoleService)
  .service('UserGroupService', UserGroupService)
  .service('SystemConfigurationService', SystemConfigurationService)
  .service('CourseService', CourseService)
  .service('DownloadService', DownloadService)
  .service('BannerService', BannerService)
  .service('CategoryService', CategoryService)
  .service('ChienDichService', ChienDichService)
  .service('KhachHangService', KhachHangService)
  .service('QuangCaoService', QuangCaoService)
  .service('LocationService', LocationService)
  .service('CarService', CarService)
  .service('RoleService', RoleService)
  .service('CookieService', CookieService)
  .service('DriverService', DriverService)
  .service('HoTroService', HoTroService)
  .service('UploadService', UploadService)
  .service('ImageReportService', ImageReportService)
export default moduleName
