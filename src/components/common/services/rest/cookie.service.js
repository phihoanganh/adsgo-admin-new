import BaseService from '../base-services'

class CookieService extends BaseService {

  constructor($q, $cookies, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.$cookies = $cookies
  }
  getToken() {
    return this.$cookies.get('token')
  }
  setToken(token) {
    return this.$cookies.put('token', token)
  }
  removeToken() {
    this.$cookies.remove('token')
  }
  saveAccountInfo(username, password) {
    this.$cookies.put('username', username)
    this.$cookies.put('password', password)
  }
  clearAccountInfo() {
    this.$cookies.remove('username')
    this.$cookies.remove('password')
  }
  isAuthenticated() {
    if (this.getToken()) {
      return true
    }
    return false
  }
  createRequestHeaders() {
    var headers = {}
    headers['Content-Type'] = 'application/json'
    headers['JWT-TOKEN'] = 'Bearer ' + this.getToken()
    return headers
  }
}

export default CookieService
