/**
 * Created by DungTQ9 on 12/18/2017.
 */
import BaseService from '../base-services'

class RoleService extends BaseService {
  constructor($http, $q, AuthenticationService, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    super($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH)
    this.AuthenticationService = AuthenticationService
  }

  getListRoles() {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.listRolePath}`
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getListAccessControls(id) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.accessControlsPath}`
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  addAccount(data) {
    return this.$http({
      method: 'post',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.addAccount}`,
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  editAccount(data, id) {
    return this.$http({
      method: 'put',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.editAccount}/` + id,
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  getAccountById(id) {
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.getAccountById}/` + id
    }).then((successResponse) => {
      // console.log(successResponse)
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  findAllAccount(page = '', count = '') {
    var params = {}
    params['currentPage'] = page
    params['nbItem'] = count
    return this.$http({
      method: 'GET',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.listAccount}`,
      params: params
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  deleteAccount(id) {
    console.log(id)
    return this.$http({
      method: 'DELETE',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.deleteAccount}/` + id
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      return Promise.reject(errorResponse)
    })
  }
  updateAccess(data) {
    var id = data.id
    return this.$http({
      method: 'PUT',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.listRolePath}/` + id,
      data: JSON.stringify(data),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

  createRole(name) {
    var role = {
      'name': name,
      'users': null,
      'accessControls': []
    }
    console.log(role)
    return this.$http({
      method: 'POST',
      url: `${this.baseUrl}${this.restPath.USER_ROLE.listRolePath}`,
      data: JSON.stringify(role),
      dataType: 'json'
    }).then((successResponse) => {
      return successResponse.data
    }, (errorResponse) => {
      console.log(errorResponse)
      return Promise.reject(errorResponse)
    })
  }

}
export default RoleService
