/** Base class to let the inheritance in services **/

class BaseService {
  constructor ($http, $q, CONTEXT_PATH, REST_BASE_URL, REST_PATH) {
    'ngInject'
    this.$http = $http
    this.$q = $q
    this.baseUrl = CONTEXT_PATH + REST_BASE_URL
    this.restPath = REST_PATH
  }
}

export default BaseService
