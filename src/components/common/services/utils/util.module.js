/* global angular:true */
import angularResource from 'angular-resource'

import DateUtils from './date.util'
import ValidationUtils from './validation.util'
import StateTransitionService from './state.service'

const moduleName = 'app.component.common.utils'

angular.module(moduleName, [angularResource])
  .service('DateUtils', DateUtils)
  .service('ValidationUtils', ValidationUtils)
  .service('StateTransitionService', StateTransitionService)
  .run((StateTransitionService) => {})

export default moduleName
