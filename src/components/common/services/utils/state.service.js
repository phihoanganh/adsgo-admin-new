/**
 * Created by DungTQ9 on 12/18/2017.
 */

class StateTransitionService {
  constructor($uiRouter) {
    'ngInject'
    $uiRouter.transitionService.onEnter({}, (trans) => {
      const from = trans.$from()
      const to = trans.$to()
      console.log(`State Changed from ${from.name} to ${to.name}`)
      document.body.scrollTop = document.documentElement.scrollTop = 0
    })
  }
}

export default StateTransitionService
