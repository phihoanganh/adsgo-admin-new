class ValidationUtils {
  constructor() {
    'ngInject'
    this.EMAIL_FORMAT = /^[a-zA-Z]+[a-zA-Z0-9._]+@[a-z]+\.[a-z.]{2,25}$/
    this.userNamePattern = /^[a-z0-9\\._]+$/
    this.passwordPattern = /^.*(?=.{6,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).*$/
    this.iPv4Pattern = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/
    this.macAddressPattern = /^([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2})$/
    this.pathLocationPatten = /^[a-zA-Z0-9./_-]*$/
    this.folderNamePattern = /^[a-zA-Z0-9_-][a-zA-Z0-9._-]*$/
  }
  getPathLocationPattern() {
    return this.pathLocationPatten
  }

  getFolderNamePattern() {
    return this.folderNamePattern
  }

  isNotEmailPattern(email) {
    let that = this
    return !that.EMAIL_FORMAT.test(email)
  }

  getUserNamePattern() {
    return this.userNamePattern
  }

  getPasswordPattern() {
    return this.passwordPattern
  }

  getIPv4Pattern() {
    return this.iPv4Pattern
  }

  getMacAddressPattern() {
    return this.macAddressPattern
  }

  setFormDirty(form) {
    // Set dirty
    if (form.$error.required !== undefined) {
      form.$error.required.forEach(function (field) {
        field.$setDirty()
      })
    }
  }
}

export default ValidationUtils
