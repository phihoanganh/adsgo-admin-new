/**
 * Created by ToanDQ5 on 5/12/2017.
 */
import moment from 'moment-timezone'

class DateUtils {
  constructor(FORMAT, SystemConfigurationService) {
    'ngInject'
    this.format = FORMAT
    this.systemConfigurationService = SystemConfigurationService
    // this.timeZone = this.getTimeZone()
    this.regex = /\(GMT[+-][0-9]{2}:[0-9]{2}\)/
  }

  getTimeZone() {
    this.systemConfigurationService.getSystemConfiguration('TIME_ZONE').then((res) => {
      this.timeZoneFull = res.data[0].configValue
      this.timeZone = res.data[0].configValue.replace(this.regex, '').trim()
      moment.tz.setDefault(this.timeZone)
    }, (err) => {
      console.log(err)
    })
  }

  displayDateTime(stringUTC) {
    if (stringUTC === this.format.UNLIMITED_TIME.value || stringUTC === this.format.UNLIMITED_TIME.simpleValue) {
      return this.format.UNLIMITED_TIME.key
    } else {
      return moment.tz(stringUTC, this.timeZone).format(this.format.DATETIME.format)
    }
  }

  displayDate(stringUTC) {
    return moment.tz(stringUTC, this.timeZone).format(this.format.DATE.format)
  }

  getCurrentTime() {
    return moment.tz(this.timeZone).format(this.format.DATETIME.format)
  }

  getCurrentFullTime() {
    return moment.tz(this.timeZone).format(this.format.FULL_DATETIME.format)
  }

  toCurrentTimeString() {
    let value = moment.tz(this.timeZone)
    let result = value.year().toString()
    result += value.month() >= 9 ? (value.month() + 1).toString() : '0' + (value.month() + 1).toString()
    result += value.date() >= 9 ? (value.date() + 1).toString() : '0' + (value.date() + 1).toString()
    result += value.hour() >= 9 ? value.hour().toString() : '0' + value.hour().toString()
    return result
  }

  getCurrentTimeStamp() {
    return moment.tz(this.timeZone)
  }

  getTimeWithFormat(time, format) {
    return moment(time, format)
  }

  toISOString(time, format) {
    return moment(time, format).toISOString()
  }

  /**
   * convert hour, miture from UTC to another timezone
   * @param hour hour
   * @param minute minute
   * @param fromUTC is convert from UTC to another time zone
   * @param timeZone timeZone to convert/ if undefined get current timezone
   * @returns {{hour: *, minute: *}}
   */
  convertTimeToZone(hour, minute, fromUTC, timeZone) {
    if (timeZone === undefined) {
      timeZone = this.timeZone
    } else {
      timeZone = timeZone.replace(this.regex, '').trim()
    }
    var convertHour
    var convertMinute
    if (fromUTC) {
      var date = new Date()
      date.setUTCHours(hour)
      date.setUTCMinutes(minute)
      var result = moment.tz(date, timeZone)
      convertHour = result.format('HH')
      convertMinute = result.format('mm')
    } else {
      var utcTime = moment(hour + ':' + minute + ' ' + timeZone, 'HH:mm Z').tz('Etc/UTC')
      convertHour = utcTime.format('HH')
      convertMinute = utcTime.format('mm')
    }
    return {
      hour: convertHour,
      minute: convertMinute
    }
  }

  getDiffTime(endTime) {
    let startTime = this.getCurrentFullTime()
    let end = moment(endTime).utc().format(this.format.FULL_DATETIME.format)
    let duration = new Date(end).getTime() - new Date(startTime).getTime()
    return duration
  }
  convertDate(input, splitText = '/') {
    function pad(s) { return (s < 10) ? '0' + s : s }
    var d = new Date(input)
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join(splitText)
  }
  convertDateSearch(input) {
    function pad(s) { return (s < 10) ? '0' + s : s }
    var d = new Date(input)
    return [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join('')
  }
  diffDay(d1, d2) {
    d1 = new Date(d1)
    d2 = new Date(d2)
    return (d2 - d1) / 86400000
  }
}

export default DateUtils
