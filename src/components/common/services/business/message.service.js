class MessageService {
  /* global $:true */
  constructor($translate, DateUtils, NotificationService, $rootScope, ErrorService, $injector) {
    'ngInject'
    this.$translate = $translate
    this.DateUtils = DateUtils
    this.NotificationService = NotificationService
    this.ErrorService = ErrorService
    this.$rootScope = $rootScope
    this.$injector = $injector
  }

  getMessage(key, context = {}) {
    this.$injector.get('$uibModalStack').dismissAll()
    if (this.$injector.get('$uibModalStack').getTop() === undefined) {
      // Close jquery modal
      $('.modal').modal('hide')
    } else {
      // Close uib modal
      this.$injector.get('$uibModalStack').dismissAll()
    }
    context._date = this.DateUtils.displayDateTime(context._date)
    return this.$translate(key, context)
  }

  displayErrorMessage(err) {
    if (err.data.code !== 503) {
      this.getMessage(err.data.msg).then((data) => {
        this.NotificationService.alertErrorMessage(data)
        this.$rootScope.$apply()
      }, (error) => {
        this.NotificationService.alertErrorMessage(error)
        this.$rootScope.$apply()
      })
    }
  }

  displaySuccessMessage(msgCode) {
    this.getMessage(msgCode).then((data) => {
      this.NotificationService.alertSuccessMessage(data)
      this.$rootScope.$apply()
    }, (error) => {
      this.NotificationService.alertSuccessMessage(error)
      this.$rootScope.$apply()
    })
  }
}

export default MessageService
