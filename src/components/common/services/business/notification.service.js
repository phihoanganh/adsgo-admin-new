class NotificationService {

  constructor (toaster) {
    'ngInject'
    this.toaster = toaster
  }

  alertSuccessMessage (msg) {
    this.toaster.pop('success', 'Successfully', msg)
  }

  alertSuccessMessageCustom (title, msg) {
    this.toaster.pop('success', title, msg)
  }

  alertErrorMessage (msg) {
    this.toaster.pop('error', 'Failure', msg)
  }

  alertErrorMessageCustom (title, msg) {
    this.toaster.pop('error', title, msg)
  }

  alertInfoMessageCustom (title, msg) {
    this.toaster.pop('info', title, msg)
  }
}

export default NotificationService
