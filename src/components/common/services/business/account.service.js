const config = {
  ACCOUNT_INFO_KEY: 'accountInfo',
  ACCESS_TOKEN_KEY: 'accessToken',
  ACCESS_TIMEOUT: 'apiTimeOut'
}

class AccountService {

  constructor() {
    this.accountInfo = undefined
  }

  saveAccountInfo(accountInfo) {
    if (accountInfo) {
      window.localStorage.setItem(config.ACCOUNT_INFO_KEY, JSON.stringify(accountInfo))
    } else {
      window.localStorage.removeItem(config.ACCOUNT_INFO_KEY)
    }

    if (accountInfo && accountInfo.accessToken) {
      window.localStorage.setItem(config.ACCESS_TOKEN_KEY, accountInfo.accessToken.token)
    } else {
      window.localStorage.removeItem(config.ACCESS_TOKEN_KEY)
    }

    if (accountInfo && accountInfo.apiTimeOut) {
      window.localStorage.setItem(config.ACCESS_TIMEOUT, parseInt(accountInfo.apiTimeOut) * 1000)
    } else if (accountInfo) {
      window.localStorage.setItem(config.ACCESS_TIMEOUT, 20000)
    } else {
      window.localStorage.removeItem(config.ACCESS_TIMEOUT)
    }
  }

  getAccountInfo() {
    let data = window.localStorage.getItem(config.ACCOUNT_INFO_KEY)
    if (data === undefined || data === null) {
      return data
    } else {
      return JSON.parse(data)
    }
  }

  clearAccountInfo() {
    this.saveAccountInfo(null)
  }

  getAccessToken() {
    return window.localStorage.getItem(config.ACCESS_TOKEN_KEY)
  }

  setAccessToken(token) {
    if (token !== undefined && token !== null) {
      window.localStorage.setItem(config.ACCESS_TOKEN_KEY, token)
    }
  }

  getTimeOutValue() {
    return window.localStorage.getItem(config.ACCESS_TIMEOUT)
  }

  hasRole(role) {
    let acountInfo = this.getAccountInfo()
    let roleUser = role.split(',')
    for (let temp of roleUser) {
      if (acountInfo != null && acountInfo.authorities !== undefined &&
        acountInfo.authorities !== null &&
        acountInfo.authorities.length > 0) {
        for (let auth of acountInfo.authorities) {
          if (temp.trim() === auth.authority) {
            return true
          }
        }
      }
    }
    return false
  }

  isAdminGroup() {
    let acountInfo = this.getAccountInfo()
    if (acountInfo != null && acountInfo.userInfo !== undefined &&
      acountInfo.userInfo !== null &&
      acountInfo.userInfo.adminAccount) {
      return true
    } else {
      return false
    }
  }
}

export default AccountService
