/* global angular:true */
import MessageService from './message.service'
import NotificationService from './notification.service'
import ErrorService from './error.service'
import AccountService from './account.service'

let moduleName = 'app.component.common.services.business'

angular.module(moduleName, [])
  .service('MessageService', MessageService)
  .service('NotificationService', NotificationService)
  .service('ErrorService', ErrorService)
  .service('AccountService', AccountService)

export default moduleName
