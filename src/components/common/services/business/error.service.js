class ErrorService {
  /* global $:true */
  constructor($state, $timeout, STATE_NAME, $injector) {
    'ngInject'
    this.$state = $state
    this.$timeout = $timeout
    this.STATE_NAME = STATE_NAME
    this.$injector = $injector
  }

  resourceNotFound() {
    this.$injector.get('$uibModalStack').dismissAll()
    if (this.$injector.get('$uibModalStack').getTop() === undefined) {
      // Close jquery modal
      $('.modal').modal('hide')
    } else {
      // Close uib modal
      this.$injector.get('$uibModalStack').dismissAll()
    }
    this.$timeout(() => {
      this.$injector.get('$state').go(this.STATE_NAME.RESOUCE_NOT_FOUND.state)
    }, 150)
  }
}

export default ErrorService
