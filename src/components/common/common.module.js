/**
 * Created by HungVT7 on 5/24/2017.
 */

/* global angular:true */

import services from './services/services.module'
import interceptor from './interceptor/interceptor.module'
import utilities from './utilities/utilities.module'

const moduleName = 'app.component.common'

angular.module(moduleName, [services, interceptor, utilities])

export default moduleName
