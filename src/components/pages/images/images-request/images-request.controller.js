/**
 * Created by Hangntt10
 */

class ImagesRequestController {
  constructor($rootScope, $scope, $stateParams, ImageReportService, $mdDialog, EVENTS, DEFAULT_VALUE) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$scope = $scope
    this.imageReportId = $stateParams.id
    this.ImageReportService = ImageReportService
    this.$mdDialog = $mdDialog
    this.EVENTS = EVENTS
    this.DEFAULT_VALUE = DEFAULT_VALUE
    this.litImageReportDetail = []
    this.selectedCars = []
    this.listSTT = DEFAULT_VALUE.DRIVER_REPORT_STT
    this.imageReport = null
    this.pagingConfig = {
      order: 'name',
      limit: 20,
      page: 1,
      filterTinhTrang: '',
      filterBienKiemSoat: ''
    }
    this.isSelectAllCars = false
  }
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyYeuCauHinh')
    this.loadImageReportDetails()
  }
  loadImageReportDetails() {
    this.ImageReportService.getReportDetail(this.imageReportId).then((res) => {
      this.litImageReportDetail = res.data
      res.forEach(item => {
        if (this.listSTT.indexOf(item.tinhTrang) < 0) {
          this.listSTT.push(item.tinhTrang)
        }
      })
      console.log('image report detail: ', this.litImageReportDetail)
    })
    this.ImageReportService.getReportNote(this.imageReportId).then((res) => {
      this.imageReport = res
    })
  }
  sendRequest() {
    this.ImageReportService.addImageReportDetail(this.imageReportId, this.selectedCars.join(',')).then((res) => {
      if (parseInt(res) === this.selectedCars.length) {
        this.showDialog('Gởi yêu cầu thành công!')
        this.loadImageReportDetails()
        this.selectedCars = []
        this.isSelectAllCars = false
      } else {
        this.showDialog('Không thể gởi yêu cầu!')
      }
    })
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  selectAllCars() {
    if (this.litImageReportDetail) {
      this.selectedCars = []
      if (this.isSelectAllCars) {
        let startIdx = (this.pagingConfig.page - 1) * this.pagingConfig.limit
        let tempAvailableCars = this.litImageReportDetail.slice(startIdx, startIdx + this.pagingConfig.limit)
        tempAvailableCars.forEach(item => {
          this.selectedCars.push(item.bienKiemSoat)
        })
        console.log(this.selectedCars)
      } else {
        this.selectedCars = []
      }
    }
  }
}
export default ImagesRequestController
