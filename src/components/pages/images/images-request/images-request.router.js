/**
 * Created by Hangntt10 on 11/21/2017.
 */
import ImagesRequestController from './images-request.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.IMAGES.REQUEST.state, {
      url: '/request/:id',
      views: {
        'content@dashboard': {
          template: require('./images-request.html'),
          controller: ImagesRequestController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
