/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './images-request.router'
import './images-request.scss'

const moduleName = 'app.component.pages.imagesRequest'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)

export default moduleName

