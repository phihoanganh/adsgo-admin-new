/**
 * Created by Hangntt10 on 11/20/2017.
 */
class ImagesAddController {
  constructor($rootScope, $scope, $state, ChienDichService, ImageReportService, CarService, KhachHangService, DateUtils,
    CommonService, $mdDialog, EVENTS, STATE_NAME, IMAGE_REQUEST_STT) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$scope = $scope
    this.EVENTS = EVENTS
    this.STATE_NAME = STATE_NAME
    this.IMAGE_REQUEST_STT = IMAGE_REQUEST_STT
    this.$state = $state
    this.ChienDichService = ChienDichService
    this.ImageReportService = ImageReportService
    this.CarService = CarService
    this.KhachHangService = KhachHangService
    this.CommonService = CommonService
    this.listReportPage = STATE_NAME.IMAGES.state
    this.$mdDialog = $mdDialog
    this.DateUtils = DateUtils
    this.ghiChu = ''
    this.pagingConfig = {
      order: 'name',
      limit: 20,
      page: 1,
      filterBienSo: '',
      filterDongXe: '',
      filterLoaiXe: '',
      filterSoCho: ''
    }
    this.itemImage = ''
    this.currentImage = ''
    this.selectedKH = null
    this.selectedCD = null
    this.listKH = []
    this.listCampaigns = []
    this.selectedCars = []
  }

  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyYeuCauHinh')
    this.loaixe = this.CommonService.getLoaiXe()
    this.KhachHangService.danhSachTen()
    .then((res) => {
      this.listKH = res
    },
    function (err) {
      console.log(err)
    })
  }
  getCampaignsByKHId(id, page = 1, num = -1) {
    this.ChienDichService.getCampaignByKHId(id, page, num).then(res => {
      this.listCampaigns = res
    })
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
        .clickOutsideToClose(false)
        .title('Thông báo')
        .textContent(result)
        .ariaLabel('Left to right demo')
        .ok('Ok')
        .openFrom('#left')
        .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  getListCampaignCars(cdId) {
    this.CarService.listCarReports(cdId).then((data) => {
      this.listXe = data
      this.listXe.forEach(item => {
        // this.listXe[i].ngayChup = this.DateUtils.convertDate(this.listXe[i].ngayChup)
        item.soCho += ' chỗ'
      })
      console.log(this.listXe)
    })
  }

  sendRequest() {
    if (this.selectedCars.length <= 0) {
      this.showDialog('Vui lòng chọn ít nhất 1 xe')
      return
    }
    if (this.ghiChu.trim().length <= 0) {
      this.showDialog('Vui lòng nhập ghi chú!')
      return
    }
    this.ImageReportService.requestReport(this.selectedCD, this.ghiChu, this.IMAGE_REQUEST_STT.DANG_TIEN_HANH, this.selectedCars.join(','), this.selectedCars.length).then((data) => {
      this.showDialog('Tạo báo cáo hình thành công!')
      this.$state.go(this.listReportPage)
    }, (errorResponse) => {
      this.showDialog('Có lỗi xảy ra, vui lòng thử lại sau')
    })
  }
  selectKH() {
    this.getCampaignsByKHId(this.selectedKH)
  }
  selectCD() {
    this.getListCampaignCars(this.selectedCD)
  }
  selectAllCars() {
    if (this.listXe) {
      this.selectedCars = []
      if (this.isSelectAllCars) {
        let startIdx = (this.pagingConfig.page - 1) * this.pagingConfig.limit
        let tempAvailableCars = this.listXe.slice(startIdx, startIdx + this.pagingConfig.limit)
        tempAvailableCars.forEach(item => {
          this.selectedCars.push(item.bienKiemSoat)
        })
        console.log(this.selectedCars)
      } else {
        this.selectedCars = []
      }
    }
  }
  goImages() {
    this.$state.go(this.STATE_NAME.IMAGES.state)
  }
}

export default ImagesAddController
