/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './images-add.router'
import './images-add.scss'

const moduleName = 'app.component.pages.images-add'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)

export default moduleName
