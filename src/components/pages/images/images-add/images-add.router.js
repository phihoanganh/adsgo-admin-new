/**
 * Created by Hangntt10 on 11/21/2017.
 */
import ImagesAddController from './images-add.controller'

function RouterConfig($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.IMAGES.ADD.state, {
      url: '/add',
      views: {
        'content@dashboard': {
          template: require('./images-add.html'),
          controller: ImagesAddController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
