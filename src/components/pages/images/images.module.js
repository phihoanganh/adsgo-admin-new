/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './images.router'
import './images.scss'
import imagesRequestPage from './images-request/images-request.module'
import imagesControlPage from './images-control/images-control.module'
import imagesAddPage from './images-add/images-add.module'

const moduleName = 'app.component.pages.images'

/* @ngInject */
angular.module(moduleName, [imagesRequestPage, imagesControlPage, imagesAddPage])
  .config(RouterConfig)

export default moduleName
