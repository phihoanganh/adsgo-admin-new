/**
 * Created by Hangntt10 on 11/21/2017.
 */
import ImagesControlController from './images-control.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.IMAGES.CONTROL.state, {
      url: '/control/:id',
      views: {
        'content@dashboard': {
          template: require('./images-control.html'),
          controller: ImagesControlController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
