/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './images-control.router'
import './images-control.scss'

const moduleName = 'app.component.pages.imagesControl'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)

export default moduleName

