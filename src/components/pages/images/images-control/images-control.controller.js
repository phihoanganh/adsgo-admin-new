/**
 * Created by Hangntt10
 */

class ImagesControlController {
  constructor($rootScope, $scope, $stateParams, ImageReportService, $mdDialog, Upload, EVENTS,
    DEFAULT_VALUE, REPORT_STATUS, DRIVER_REPORT_STT, CONTEXT_PATH, REST_BASE_URL, REST_PATH, IMAGE_REQUEST_STT) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$scope = $scope
    this.$mdDialog = $mdDialog
    this.DEFAULT_VALUE = DEFAULT_VALUE
    this.IMAGE_REQUEST_STT = IMAGE_REQUEST_STT
    this.REPORT_STATUS = REPORT_STATUS
    this.DRIVER_REPORT_STT = DRIVER_REPORT_STT
    this.url = CONTEXT_PATH + REST_BASE_URL + REST_PATH.UPLOAD.uploadPath
    this.Upload = Upload
    this.ImageReportService = ImageReportService
    this.imageReportId = $stateParams.id
    this.EVENTS = EVENTS
    this.imageReport = null
    this.pagingConfig = {
      order: 'name',
      limit: 20,
      page: 1,
      filterTinhTrang: '',
      filterBienKiemSoat: ''
    }
    this.username = 'report'
    this.itemImage = ''
    this.currentImage = ''
    this.showingReport = null
    this.requestReport = null
    this.numberFile = 0
    this.checkUploadFile = 0
    this.image1 = undefined
    this.image2 = undefined
    this.image3 = undefined
    this.uploadedImages = []
    this.isUploadFail = false
    this.count = {}
  }
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyYeuCauHinh')
    this.loadImageReportDetails()
  }
  loadImageReportDetails() {
    this.ImageReportService.getReportDetail(this.imageReportId).then((res) => {
      this.litImageReportDetail = res.data
      this.count = res.count
      console.log('image report detail: ', this.litImageReportDetail)
    })
    this.ImageReportService.getReportNote(this.imageReportId).then((res) => {
      this.imageReport = res
      console.log(res)
    })
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  showPrerenderedDialog(ev, report) {
    this.showingReport = report
    this.itemImage = report.urlReportShow
    if (report.urlReportShow['1']) {
      this.currentImage = report.urlReportShow['1']
    } else if (report.urlReportShow['2']) {
      this.currentImage = report.urlReportShow['2']
    } else if (report.urlReportShow['3']) {
      this.currentImage = report.urlReportShow['3']
    }
    this.$mdDialog.show({
      contentElement: '#viewCar',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    })
  }
  showUploadDialog(ev, report) {
    this.requestReport = JSON.parse(JSON.stringify(report))
    console.log(this.requestReport)
    console.log('number files: ', this.numberFile)
    this.$mdDialog.show({
      contentElement: '#uploadImages',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true,
      onRemoving: (event, removePromise) => {
        this.$scope.$broadcast(this.EVENTS.CLEAR_UPLOAD_IMAGE)
        this.numberFile = 0
        this.requestReport = {}
      }
    }).then(cancel => {
      this.$mdDialog.hide()
    })
  }
  cancel() {
    this.$mdDialog.hide()
  }
  uploadImages() {
    if (!this.canUpload()) {
      alert('Vui lòng chọn đủ 3 hình ảnh!')
    } else {
      this.uploadedImages = []
      this.isUploadFail = false
      this.checkUploadFile = 0
      this.upload(this.image1, 1)
      this.upload(this.image2, 2)
      this.upload(this.image3, 3)
    }
  }
  canUpload() {
    return (this.requestReport && ((this.countImagesUrl(this.requestReport.urlReport) + this.numberFile) >= 3))
  }
  countImagesUrl(url) {
    if (url) {
      let items = url.split(',')
      let count = 0
      items.forEach(item => {
        if (item.trim() !== '') {
          count++
        }
      })
      return count
    }
    return 0
  }
  updateUrl() {
    this.requestReport.label = this.REPORT_STATUS.DA_CHUP
    this.requestReport.trangThaiReport = this.DRIVER_REPORT_STT.TU_CHOI
    this.ImageReportService.updateImageReportDetail(this.requestReport).then((res) => {
      if (res === this.requestReport.id) {
        this.showDialog('Cập nhật hình ảnh thành công!')
        this.loadImageReportDetails()
        this.image1 = undefined
        this.image2 = undefined
        this.image3 = undefined
      } else {
        this.showDialog('Không thể cập nhật hình ảnh!')
      }
    })
  }
  changeImage(current) {
    this.currentImage = current
  }
  // approveReport() {
  //   this.showingReport.label =
  //   this.showingReport.trangThaiReport =
  //   this.ImageReportService.updateImageReportDetail()
  // }
  // rejectReport() {
  // }
  onChangeImage1(image) {
    if (image !== undefined) {
      this.numberFile += 1
    } else if (this.image1 !== undefined) {
      this.numberFile -= 1
    } else {
      this.requestReport.urlReport = this.removeImageUrl(1)
    }
    this.image1 = image
  }
  onChangeImage2(image) {
    if (image !== undefined) {
      this.numberFile += 1
    } else if (this.image2 !== undefined) {
      this.numberFile -= 1
    } else {
      this.requestReport.urlReport = this.removeImageUrl(2)
    }
    this.image2 = image
  }
  onChangeImage3(image) {
    if (image !== undefined) {
      this.numberFile += 1
    } else if (this.image3 !== undefined) {
      this.numberFile -= 1
    } else {
      this.requestReport.urlReport = this.removeImageUrl(3)
    }
    this.image3 = image
  }
  removeImageUrl(idx) {
    let listUrlImages = this.requestReport.urlReport.split(',')
    for (let i = listUrlImages.length - 1; i >= 0; i--) {
      let parts = listUrlImages[i].split('_')
      if (parts.length > 1 && parts[0] === String(idx)) {
        listUrlImages.splice(i, 1)
        break
      }
    }
    return listUrlImages.join(',')
  }
  upload(file, idx) {
    if (file) {
      file.upload = this.Upload.upload({
        url: this.url,
        data: { username: this.username, file: file }
      })
      var currentTimeStamp = this.getCurrentTimeStamp()
      var temp = this.splitFileExtension(file.name)
      var renameImage = idx + '_ad' + currentTimeStamp + '.' + temp
      this.Upload.rename(file, renameImage)
      file.upload.then((response) => {
        console.log('upload result: ', response)
        if (response.status === 200 && response.data.indexOf('http') !== -1) {
          this.checkUploadFile += 1
          this.uploadedImages.push(renameImage)
        } else {
          if (!this.isUploadFail) {
            this.showDialog('Không thể lưu file')
          }
          this.isUploadFail = true
        }
        if (this.checkUploadFile === this.numberFile) {
          if (this.requestReport.urlReport !== '') {
            this.requestReport.urlReport += ',' + this.uploadedImages.join(',')
          } else {
            this.requestReport.urlReport = this.uploadedImages.join(',')
          }
          this.updateUrl()
        }
      }, (response) => {
        if (response.status > 0) {
          if (response.status === 500) {
            console.log('upload fail')
            if (!this.isUploadFail) {
              this.showDialog('Lưu file không thành công')
            }
            this.isUploadFail = true
          }
        }
      }, function (evt) {
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
      })
    }
  }
  completeRequest() {
    for (let i = 0; i < this.litImageReportDetail.length; i++) {
      if (this.litImageReportDetail[i].urlReport === null || String(this.litImageReportDetail[i].urlReport).trim() === '') {
        this.showDialog('Không thể kết thúc vì còn xe chưa có hình ảnh!')
        return
      }
    }
    this.imageReport.tinhTrang = this.IMAGE_REQUEST_STT.HOAN_THANH
    this.ImageReportService.updateImageReport(this.imageReport).then((res) => {
      if (res.id === this.imageReport.id) {
        this.loadImageReportDetails()
        this.showDialog('Kết thúc yêu cầu báo cáo hình thành công.')
      } else {
        this.showDialog('Không thể kết thúc yêu cầu!')
      }
    })
  }
  splitFileExtension(filename) {
    return filename.split('.').pop()
  }
  getCurrentTimeStamp() {
    console.log(new Date().getTime())
    return new Date().getTime()
  }
}
export default ImagesControlController
