/**
 * Created by Hangntt10
 */

class ImagesController {
  constructor($rootScope, $scope, $state, ImageReportService, $mdDialog, EVENTS, STATE_NAME, DEFAULT_VALUE, IMAGE_REQUEST_STT) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$scope = $scope
    this.$state = $state
    this.$mdDialog = $mdDialog
    this.EVENTS = EVENTS
    this.STATE_NAME = STATE_NAME
    this.DEFAULT_VALUE = DEFAULT_VALUE
    this.IMAGE_REQUEST_STT = IMAGE_REQUEST_STT
    this.ImageReportService = ImageReportService
    this.listImageReports = []
    this.listKH = []
    this.listCD = []
    this.listSTT = DEFAULT_VALUE.IMAGE_REQUEST_STT
    this.showingReport = null
    this.pagingConfig = {
      order: 'name',
      limit: 20,
      page: 1,
      filterTinhTrang: '',
      filterKhachHang: '',
      filterChienDich: ''
    }
  }
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyYeuCauHinh')
    this.loadImageReports()
  }
  loadImageReports() {
    this.ImageReportService.getDanhSachBaoCaoHinh().then(res => {
      this.listImageReports = res
      this.listCD = []
      this.listKH = []
      res.forEach(item => {
        if (this.listKH.indexOf(item.tenCongTy) < 0) {
          this.listKH.push(item.tenCongTy)
        }
        if (this.listCD.indexOf(item.tenChienDich) < 0) {
          this.listCD.push(item.tenChienDich)
        }
      })
    })
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }

  showCustom(event, report) {
    this.showingReport = report
    console.log(this.showingReport)
    this.$mdDialog.show({
      clickOutsideToClose: true,
      scope: this.$scope,
      preserveScope: true,
      template: require('./dialog.tmpl.html'),
      parent: angular.element(document.body),
      controller: ['$scope', '$mdDialog',
        function($scope, $mdDialog) {
          $scope.closeDialog = function() {
            $mdDialog.hide()
          }
          $scope.cancel = function() {
            $mdDialog.cancel()
          }
          $scope.answer = function(answer) {
            $mdDialog.hide(answer)
          }
        }
      ]
    }).then((answer) => {
      if (answer) {
      }
    }, () => {
      console.log('You cancelled the dialog.')
    })
  }
  goToRequest(id) {
    this.$state.go(this.STATE_NAME.IMAGES.REQUEST.state, {id: id})
  }
  goToControl(id) {
    this.$state.go(this.STATE_NAME.IMAGES.CONTROL.state, {id: id})
  }
  goAddImageRequest() {
    this.$state.go(this.STATE_NAME.IMAGES.ADD.state)
  }
  rejectRequest() {
    if (this.showingReport.phanHoi === null || this.showingReport.phanHoi.trim() === '') {
      alert('Vui lòng nhập phản hồi!')
      return
    }
    this.showingReport.tinhTrang = this.IMAGE_REQUEST_STT.TU_CHOI
    this.ImageReportService.updateImageReport(this.showingReport).then((res) => {
      if (res.id === this.showingReport.id) {
        this.loadImageReports()
        this.showDialog('Đã từ chối yêu cầu.')
      } else {
        this.showDialog('Không thể từ chối yêu cầu!')
      }
    })
  }
  approveRequest() {
    if (this.showingReport.phanHoi === null || this.showingReport.phanHoi.trim() === '') {
      alert('Vui lòng nhập phản hồi!')
      return
    }
    this.showingReport.tinhTrang = this.IMAGE_REQUEST_STT.DANG_TIEN_HANH
    this.ImageReportService.updateImageReport(this.showingReport).then((res) => {
      if (res.id === this.showingReport.id) {
        this.loadImageReports()
        this.showDialog('Đã duyệt yêu cầu.')
      } else {
        this.showDialog('Không thể duyệt yêu cầu!')
      }
    })
  }
}

export default ImagesController
