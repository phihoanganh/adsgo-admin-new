/**
 * Created by Hangntt10 on 11/21/2017.
 */
import ImagesController from './images.controller'
// STATE_NAME.DRIVER

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.IMAGES.state, {
      url: '/images',
      views: {
        'content@dashboard': {
          template: require('./images.html'),
          controller: ImagesController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
