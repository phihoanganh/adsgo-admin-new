/**
 * Created by Hangntt10 on 11/20/2017.
 */
class ConfigurationController {
  constructor($rootScope, $scope, $state, DateUtils, CommonService, QuangCaoService, $mdDialog, STATE_NAME, EVENTS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$scope = $scope
    this.$mdDialog = $mdDialog
    this.EVENTS = EVENTS
    this.STATE_NAME = STATE_NAME
    this.$state = $state
    this.QuangCaoService = QuangCaoService
    this.listViewArguments = []
    this.listGeneralArguments = []
    this.generalArgs = {}
  }

  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'CauHinh')
    this.QuangCaoService.listAdsKindDetail().then((res) => {
      this.listViewArguments = res
    })
    this.QuangCaoService.getGeneralArgument().then((res) => {
      this.listGeneralArguments = res
      this.listGeneralArguments.forEach(arg => {
        this.generalArgs[arg.unnormalCarDays] = {'idLoaiQuangCao': arg.idLoaiQuangCao, 'giaTri': arg.giaTri}
      })
    })
  }
  showDialog(text) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(text)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  updateListArgsValue() {
    this.listGeneralArguments = []
    this.listGeneralArguments.push(this.generalArgs.views_19h_6h)
    this.listGeneralArguments.push(this.generalArgs.unnormal_car_days)
    this.listGeneralArguments.push(this.generalArgs.unnormal_car_km)
    this.listGeneralArguments.push(this.generalArgs.unnormal_campaign_days)
  }
  updateConfig() {
    let isSuccess = false
    let isFail = false
    let isValid = true
    this.updateListArgsValue()
    this.listViewArguments.forEach(item => {
      if (item.view4Cho === null || String(item.view4Cho).trim().length <= 0 || isNaN(parseFloat(item.view4Cho)) ||
          item.view7Cho === null || String(item.view7Cho).trim().length <= 0 || isNaN(parseFloat(item.view7Cho))) {
        isValid = false
      }
    })
    if (!isValid) {
      this.showDialog('Vui lòng nhập đầy đủ các giá trị và phải là số!')
      return false
    }
    this.listGeneralArguments.forEach(item => {
      if (item.giaTri === null || String(item.giaTri).trim().length <= 0 || isNaN(parseFloat(item.giaTri))) {
        isValid = false
      }
    })
    if (!isValid) {
      this.showDialog('Vui lòng nhập đầy đủ các giá trị và phải là số!')
      return false
    }
    this.QuangCaoService.UpdateViewsArgs(this.listViewArguments).then((res) => {
      if (res.length > 0 && res[0].id > 0) {
        if (!isSuccess) {
          this.showDialog('Cập nhật cấu hình thành công!')
          isSuccess = true
        }
      } else {
        if (isFail) {
          this.showDialog('Không thể cập nhật!')
        } else {
          isFail = true
        }
      }
    })
    this.QuangCaoService.UpdateGeneralArgs(this.listGeneralArguments).then((res) => {
      if (res.length > 0 && res[0].id > 0) {
        if (!isSuccess) {
          this.showDialog('Cập nhật cấu hình thành công!')
          isSuccess = true
        }
      } else {
        if (isFail) {
          this.showDialog('Không thể cập nhật!')
        } else {
          isFail = true
        }
      }
    })
  }
}

export default ConfigurationController
