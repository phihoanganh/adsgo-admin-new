/**
 * Created by Hangntt10 on 11/21/2017.
 */
import ConfigurationController from './configuration.controller'
// STATE_NAME.HOME

function RouterConfig($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CONFIGURATION.state, {
      url: '/configuration',
      views: {
        'content@dashboard': {
          template: require('./configuration.html'),
          controller: ConfigurationController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
