/**
 * Created by CuongNM13 on 4/23/2017.
 */

class LoginController {
  constructor($scope, $state, $mdDialog, AuthenticationService, STATE_NAME, ERROR_MSG) {
    'ngInject'
    this.user = {}
    this.$scope = $scope
    this.$state = $state
    this.$mdDialog = $mdDialog
    this.STATE_NAME = STATE_NAME
    this.ERROR_MSG = ERROR_MSG
    this.AuthenticationService = AuthenticationService
    this.username = ''
    this.password = ''
    this.token = ''
    this.isRemember = false
    this.isShowMessage = false
    this.msgLogin = ''
    this.listRole = []
    if (this.AuthenticationService.isAuthenticated()) {
      this.redirect()
    }
  }
  $onInit() {
    this.AuthenticationService.getUserRole().then((data) => {
      this.listRole = data
    })
  }
  redirect() {
    if (this.listRole.length <= 0) {
      this.AuthenticationService.getUserRole().then((data) => {
        this.listRole = data
        this.gotoPage(this.listRole)
      })
    } else {
      this.gotoPage(this.listRole)
    }
  }
  gotoPage(data) {
    if (data.length > 0) {
      if (data.indexOf('ROLE_QL_CHIEN_DICH') >= 0) {
        this.$state.go(this.STATE_NAME.HOME.state)
      } else if (data.indexOf('ROLE_QL_TAI_KHOAN_NOI_BO') >= 0) {
        this.$state.go(this.STATE_NAME.PERMISSION.state)
      } else if (data.indexOf('ROLE_QL_XE') >= 0) {
        this.$state.go(this.STATE_NAME.CAR.state)
      } else if (data.indexOf('ROLE_QL_KHACH_HANG') >= 0) {
        this.$state.go(this.STATE_NAME.CUSTOMER.state)
      }
    } else {
      this.isShowMessage = true
      this.msgLogin = this.ERROR_MSG.FORBIDDEN
    }
  }
  login() {
    this.AuthenticationService.authenticate(this.username, this.password, this.isRemember).then((res) => {
      if (res === true) {
        this.redirect()
        // new call loading
      } else {
        this.isShowMessage = true
        this.msgLogin = this.ERROR_MSG.LOGIN_ERR
      }
    })
  }
}
export default LoginController
