/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './login.router'
import './login.scss'

const moduleName = 'app.component.pages.login'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)

export default moduleName
