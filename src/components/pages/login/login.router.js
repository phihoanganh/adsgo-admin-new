import LoginController from './login.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.LOGIN.state, {
      url: '/login',
      views: {
        'dashboard': {
          template: require('./login.html'),
          controller: LoginController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
