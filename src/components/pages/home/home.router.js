/**
 * Created by Hangntt10 on 11/21/2017.
 */
import HomeController from './home.controller'
// STATE_NAME.HOME

function RouterConfig($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.HOME.state, {
      url: '/home',
      views: {
        'content@dashboard': {
          template: require('./home.html'),
          controller: HomeController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
