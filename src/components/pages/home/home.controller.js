/**
 * Created by Hangntt10 on 11/20/2017.
 */
class HomeController {
  constructor($rootScope, $state, ChienDichService, STATE_NAME, EVENTS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.$state = $state
    this.ChienDichService = ChienDichService
    this.counterStart = 0
    this.autoStartVar = 0
    this.counterDuration = 2
    this.name = ''
    this.overviewInfo = null
  }

  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'TongQuan')
    this.getOverviewInfo()
  }
  getOverviewInfo() {
    this.ChienDichService.getOverviewInfo().then((data) => {
      this.overviewInfo = data
      console.log(this.overviewInfo)
    })
  }
}

export default HomeController
