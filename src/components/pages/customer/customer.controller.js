/**
 * Created by Hangntt10 on 11/20/2017.
 */
class CustomerController {
  constructor($rootScope, $state, STATE_NAME, EVENTS, REST_PATH, KhachHangService) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.REST_PATH = REST_PATH
    this.khachHangService = KhachHangService
    this.pageItems = 10
    this.page = 1
    this.countAllItem = 0
    this.editKH = STATE_NAME.CUSTOMER.EDIT.state
    this.addKH = STATE_NAME.CUSTOMER.ADD.state
    this.searchKey = undefined
    this.delay = 0
    this.delayInterval = undefined
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyKhachHang')
    this.locKhachHang('', this.page, this.pageItems)
  }
  locKhachHang(searchKey = '', page = 1, count = 1) {
    this.khachHangService.locKhachHang(searchKey, page, count).then((data) => {
      this.listKH = data
      this.listKH.forEach(kh => {
        kh.logo = this.REST_PATH.AWS_IMAGE.imageBaseUrl + kh.logo
      })
      // console.log(data)
      if (data.length > 0) {
        this.countAllItem = parseInt(data[0]['countAllItem'])
      } else {
        this.countAllItem = 0
      }
    })
  }

  deleteKH(id) {
    var confirm = window.confirm('Xác nhận xóa khách hàng ?')
    if (confirm === true) {
      this.khachHangService.deleteKH(id).then(data => {
        if (data.id && data.id > 0) {
          this.applyFilter(this.page)
        } else {
          alert('Không thể xóa, có chiến dịch thuộc khách hàng này!')
        }
      })
    }
  }

  goEditKH(khId) {
    this.$state.go(this.editKH, {id: khId})
  }

  goAddKH() {
    this.$state.go(this.addKH)
  }

  changePage(page) {
    this.page = page
    this.applyFilter(page)
  }
  changeItemPerPage(itemPerPage) {
    this.page = 1
    this.pageItems = itemPerPage
    this.applyFilter()
  }

  applyFilter(page = 1) {
    var keyName = this.searchKey
    this.locKhachHang(keyName, page, this.pageItems)
  }

  searchChanged(event) {
    if (event.keyCode === 13) {
      this.applyFilter()
    } else {
      console.log('Changed: ' + this.searchKey)
      clearInterval(this.intervalDelay)
      var that = this
      this.intervalDelay = setInterval(function() {
        that.applyFilter()
        console.log('Searching...' + that.searchKey)
        clearInterval(that.intervalDelay)
      }, 350)
    }
  }
}

export default CustomerController
