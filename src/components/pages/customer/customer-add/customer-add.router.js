/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CustomerAddController from './customer-add.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CUSTOMER.ADD.state, {
      url: '/add',
      views: {
        'content@dashboard': {
          template: require('./customer-add.html'),
          controller: CustomerAddController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
