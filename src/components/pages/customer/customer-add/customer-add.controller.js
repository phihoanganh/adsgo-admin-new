/**
 * Created by Hangntt10 on 01/02/2018.
 */
class CustomerAddController {
  constructor($rootScope, $state, CommonService, KhachHangService, Upload, $mdDialog, CONTEXT_PATH, REST_BASE_URL, REST_PATH, STATE_NAME, EVENTS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.CommonService = CommonService
    this.KhachHangService = KhachHangService
    this.Upload = Upload
    this.customer = {}
    this.$mdDialog = $mdDialog
    this.url = CONTEXT_PATH + REST_BASE_URL + REST_PATH.UPLOAD.uploadPath
    this.username = 'khach_hang'
    this.image = undefined
  }

  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyKhachHang')
    this.customer = {}
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  goCustomerView() {
    this.$state.go(this.STATE_NAME.CUSTOMER.state)
  }

  addCustomer() {
    if (this.image) {
      this.saveCustomer(this.image)
    } else {
      this.customer.logo = ''
      this.KhachHangService.AddCustomer(this.customer).then((res) => {
        if (parseInt(res['id']) > 0) {
          this.showDialog('Lưu khách hàng thành công')
          this.goCustomerView()
        } else {
          this.showDialog('Không thể thêm khách hàng, email đã tồn tại')
        }
      })
    }
  }

  saveCustomer(file) {
    if (file) {
      file.upload = this.Upload.upload({
        url: this.url,
        data: { username: this.username, file: file }
      })
      var currentTimeStamp = this.getCurrentTimeStamp()
      var temp = this.splitFileExtension(file.name)
      var renameImage = 'KH_' + currentTimeStamp + '.' + temp
      this.Upload.rename(file, renameImage)
      this.customer.logo = renameImage
      this.KhachHangService.AddCustomer(this.customer).then((res) => {
        if (parseInt(res['id']) > 0) {
          file.upload.then((response) => {
            console.log('upload result: ', response)
            if (response.status === 200 && response.data.indexOf('http') !== -1) {
              this.showDialog('Lưu khách hàng thành công')
              this.goCustomerView()
            } else {
              this.showDialog('Không thể lưu file')
            }
          }, function (response) {
            if (response.status === 500) {
              console.log('upload fail')
              if (!this.isUploadFail) {
                this.showDialog('Lưu file không thành công')
              }
              this.isUploadFail = true
            }
          }, function (evt) {
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
          })
        } else {
          this.showDialog('Không thể thêm khách hàng, email đã tồn tại')
        }
      })
    }
  }
  onChangeImage(image) {
    console.log('change image')
    this.image = image
  }
  splitFileExtension(filename) {
    return filename.split('.').pop()
  }
  getCurrentTimeStamp() {
    console.log(new Date().getTime())
    return new Date().getTime()
  }
}

export default CustomerAddController
