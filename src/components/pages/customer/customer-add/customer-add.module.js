/**
 * Created by Hangntt10 on 11/20/2017.
 */
/* global angular:true */
import RouterConfig from './customer-add.router'
import './customer-add.scss'

const moduleName = 'app.component.pages.customer-add'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)
  .config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false)
  }])

export default moduleName
