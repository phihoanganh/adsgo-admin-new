/**
 * Created by Hangntt10 on 02/28/2018.
 */
import CustomerController from './customer.controller'
// STATE_NAME.CAR

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CUSTOMER.state, {
      url: '/khach-hang',
      views: {
        'content@dashboard': {
          template: require('./customer.html'),
          controller: CustomerController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
