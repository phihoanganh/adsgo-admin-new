/**
 * Created by Hangntt10 on 11/20/2017.
 */
/* global angular:true */
import RouterConfig from './customer-view.router'
import './customer-view.scss'

const moduleName = 'app.component.pages.customer-view'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)
  .config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false)
  }])

export default moduleName
