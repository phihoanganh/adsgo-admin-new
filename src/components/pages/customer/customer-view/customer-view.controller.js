/**
 * Created by Hangntt10 on 01/02/2018.
 */
class CustomerViewController {
  constructor($rootScope, $state, $stateParams, KhachHangService, DateUtils, ChienDichService, REST_PATH, STATE_NAME, EVENTS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.$stateParams = $stateParams
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.REST_PATH = REST_PATH
    this.customerId = this.$stateParams.id
    this.KhachHangService = KhachHangService
    this.ChienDichService = ChienDichService
    this.DateUtils = DateUtils
    this.customer = {}
    this.listCampaigns = []
    this.page = 1
    this.itemPerPage = 10
    this.countAllItem = 0
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyKhachHang')
    this.getCustomer()
    this.getCampaigns()
  }

  getCustomer() {
    console.log('Getting customer...')
    this.KhachHangService.getCustomerById(this.customerId).then((res) => {
      if (parseInt(res['id']) > 0) {
        this.customer = res
        if (res['sdt'] !== null) {
          this.customer['sdt'] = parseInt(res['sdt'])
        }
        if (res['sdtNguoiLienHe'] !== null) {
          this.customer['sdtNguoiLienHe'] = parseInt(res['sdtNguoiLienHe'])
        }
        this.customer.logo = this.REST_PATH.AWS_IMAGE.imageBaseUrl + res.logo
        console.log(this.customer)
      } else {
        this.showDialog('Không thể lấy thông tin khách hàng')
      }
    })
  }
  getCampaigns() {
    this.ChienDichService.getCampaignByKHId(this.customerId, this.page, this.itemPerPage).then((res) => {
      this.listCampaigns = res
      if (res.length > 0) {
        this.countAllItem = this.listCampaigns[0].countAllItem
        for (var i = 0; i < this.listCampaigns.length; i++) {
          var startDate = this.listCampaigns[i].thoiGianBatDau.split(' ')
          if (startDate.length > 0) {
            this.listCampaigns[i].thoiGianBatDau = this.DateUtils.convertDate(startDate[0])
          } else {
            this.listCampaigns[i].thoiGianBatDau = ''
          }
          var endDate = this.listCampaigns[i].thoiGianKetThuc.split(' ')
          if (endDate.length > 0) {
            this.listCampaigns[i].thoiGianKetThuc = this.DateUtils.convertDate(endDate[0])
          } else {
            this.listCampaigns[i].thoiGianKetThuc = ''
          }
          if (this.listCampaigns[i].quangCao.label != null) {
            this.listCampaigns[i].quangCao.label = this.listCampaigns[i].quangCao.label.split(';')
          }
        }
      }
      console.log(this.listCampaigns)
    })
  }
  changePage(page) {
    this.page = page
    this.getCampaigns()
  }

  changeItemPerPage(itemPerPage) {
    this.page = 1
    this.itemPerPage = itemPerPage
    this.getCampaigns()
  }
}

export default CustomerViewController
