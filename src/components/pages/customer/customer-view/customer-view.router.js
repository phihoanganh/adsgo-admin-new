/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CustomerViewController from './customer-view.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CUSTOMER.VIEW.state, {
      url: '/view/:id',
      views: {
        'content@dashboard': {
          template: require('./customer-view.html'),
          controller: CustomerViewController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
