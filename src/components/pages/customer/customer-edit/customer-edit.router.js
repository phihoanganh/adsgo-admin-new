/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CustomerEditController from './customer-edit.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CUSTOMER.EDIT.state, {
      url: '/edit/:id',
      views: {
        'content@dashboard': {
          template: require('./customer-edit.html'),
          controller: CustomerEditController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
