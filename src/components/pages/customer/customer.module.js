/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import customerEditPage from './customer-edit/customer-edit.module'
import customerAddPage from './customer-add/customer-add.module'
import customerViewPage from './customer-view/customer-view.module'
import RouterConfig from './customer.router'
import './customer.scss'

const moduleName = 'app.component.pages.customer'

/* @ngInject */
angular.module(moduleName, [customerEditPage, customerAddPage, customerViewPage])
  .config(RouterConfig)

export default moduleName
