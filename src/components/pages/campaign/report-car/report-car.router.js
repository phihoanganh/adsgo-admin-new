/**
 * Created by Hangntt10 on 11/21/2017.
 */
import ReportCarController from './report-car.controller'
// STATE_NAME.HOME

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CAMPAIGN.CAR_REPORT.state, {
      url: '/report-car/:id',
      views: {
        'content@dashboard': {
          template: require('./report-car.html'),
          controller: ReportCarController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
