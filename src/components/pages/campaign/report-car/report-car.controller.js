/**
 * Created by Hangntt10 on 11/20/2017.
 */
class ReportCarController {
  constructor($scope, $stateParams, CarService, DateUtils, CommonService, $mdDialog, STATE_NAME) {
    'ngInject'
    this.$scope = $scope
    this.campaignId = $stateParams['id']
    this.STATE_NAME = STATE_NAME
    this.CarService = CarService
    this.CommonService = CommonService
    this.$mdDialog = $mdDialog
    this.DateUtils = DateUtils
    this.selected = []
    this.listXe = []
    this.pagingConfig = {
      order: 'name',
      limit: 20,
      page: 1,
      filterBienSo: '',
      filterLoaiXe: ''
    }
    this.itemImage = ''
    this.currentImage = ''
    this.bienso = ''
  }

  /**
   * init data
   */
  $onInit() {
    this.loaixe = this.CommonService.getLoaiXe()
    this.getDanhSachXe()
  }

  applyFilter() {
    this.pagingConfig.filterBienSo = this.bienso
    this.pagingConfig.filterLoaiXe = this.loaixeselected
  }

  getDanhSachXe() {
    this.CarService.listCarReports(this.campaignId).then((data) => {
      this.listXe = data
      for (var i = 0; i < this.listXe.length; i++) {
        // this.listXe[i].ngayChup = this.DateUtils.convertDate(this.listXe[i].ngayChup)
        this.listXe[i].soCho += ' chỗ'
        // this.listXe[i].tongView = parseInt(this.listXe[i].tongView)
        // this.listXe[i].tongKM = parseInt(this.listXe[i].tongKM)
      }
      console.log(this.listXe)
    })
  }
  showPrerenderedDialog(ev, list, current) {
    console.log(list)
    console.log(current)
    // this.itemImage = list
    //    this.currentImage = current
    this.itemImage = list
    this.currentImage = current
    this.$mdDialog.show({
      contentElement: '#viewCar',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    })
  }
  changeImage(current) {
    this.currentImage = current
  }
  searchChanged(event) {
    if (event.keyCode === 13) {
      this.applyFilter()
    } else {
      setTimeout(() => {
        this.applyFilter()
      }, 350)
    }
  }
}

export default ReportCarController
