/**
 * Created by Hangntt10 on 11/20/2017.
 */
class CampaignController {
  constructor($rootScope, $scope, $state, ChienDichService, KhachHangService, CommonService, DateUtils, REST_PATH, STATE_NAME, DISPLAY_MODE, EVENTS, CAMPAIGN_STATUS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.$scope = $scope
    this.STATE_NAME = STATE_NAME
    this.REST_PATH = REST_PATH
    this.CAMPAIGN_STATUS = CAMPAIGN_STATUS
    this.EVENTS = EVENTS
    this.editCampaign = STATE_NAME.CAMPAIGN.EDIT.state
    this.searchDisplayMode = DISPLAY_MODE.ADVANCE
    this.ChienDichService = ChienDichService
    this.KhachHangService = KhachHangService
    this.CommonService = CommonService
    this.DateUtils = DateUtils
    this.countAllItem = 0
    this.listCampaigns = null
    this.listCustomers = null
    this.page = 1
    this.date = {from: '', to: ''}
    this.area = ''
    this.pageItems = 10
    this.name = ''
  }
  /**
   * init data
   */
  $onInit() {
    this.citys = this.CommonService.getListAreas()
    this.mode = 0
    this.searchCampaign('', '', '', '', 1, this.pageItems)
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyChienDich')
  }
  changePage(page) {
    this.page = page
    this.applyFilter(page)
  }
  applyFilter(page = 1) {
    var dateFrom = ''
    var dateTo = ''
    if (this.date.from !== null && this.date.from !== '') {
      dateFrom = this.DateUtils.convertDateSearch(this.date.from)
    }
    if (this.date.to !== null && this.date.to !== '') {
      dateTo = this.DateUtils.convertDateSearch(this.date.to)
    }
    this.searchCampaign(this.name, dateFrom, dateTo, this.area, page, this.pageItems)
  }
  searchCampaign(ten = '', startDate = '', endDate = '', khuVuc = '', page = 1, count = 1) {
    this.ChienDichService.locChienDich(ten, startDate, endDate, khuVuc, page, count).then((data) => {
      this.listCampaigns = data
      if (data.length > 0) {
        this.countAllItem = parseInt(data[0]['countAllItem'])
      } else {
        this.countAllItem = 0
      }
      for (var i = 0; i < this.listCampaigns.length; i++) {
        var startDate = this.listCampaigns[i].thoiGianBatDau.split(' ')
        if (startDate.length > 0) {
          this.listCampaigns[i].thoiGianBatDau = this.DateUtils.convertDate(startDate[0])
        } else {
          this.listCampaigns[i].thoiGianBatDau = ''
        }
        var endDate = this.listCampaigns[i].thoiGianKetThuc.split(' ')
        if (endDate.length > 0) {
          this.listCampaigns[i].thoiGianKetThuc = this.DateUtils.convertDate(endDate[0])
        } else {
          this.listCampaigns[i].thoiGianKetThuc = ''
        }
        if (this.listCampaigns[i].label != null) {
          this.listCampaigns[i].label = this.listCampaigns[i].label.split(';')
        }
        let listUrls = this.listCampaigns[i].urlQuangCao.split(',')
        this.listCampaigns[i]['listImgs'] = {}
        listUrls.forEach(url => {
          if (url !== '') {
            let items = url.split('_')
            if (items.length > 1) {
              this.listCampaigns[i]['listImgs'][items[0]] = this.REST_PATH.AWS_IMAGE.imageBaseUrl + url
            }
          }
        })
      }
      console.log('listimg', this.listCampaigns)
    })
  }
  changeItemPerPage(itemPerPage) {
    this.pageItems = itemPerPage
    this.applyFilter()
  }
  deleteCampaign(id) {
    var confirm = window.confirm('Xác nhận xóa chiến dịch?')
    if (confirm === true) {
      this.ChienDichService.deleteCampaign(id).then(data => {
        if (data) {
          // alert('Đã xóa!')
          this.applyFilter(this.page)
        } else {
          alert('Không thể xóa!')
        }
      })
    }
  }
  goAddScreen() {
    // console.log(`hello the world`)
    this.$state.go(this.STATE_NAME.CAMPAIGN.ADD.state)
  }
  goEditCampaign(campaignId) {
    this.$state.go(this.editCampaign, {id: campaignId})
  }
}

export default CampaignController
