/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CampaignAddController from './campaign-add.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CAMPAIGN.ADD.state, {
      url: '/add',
      views: {
        'content@dashboard': {
          template: require('./campaign-add.html'),
          controller: CampaignAddController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
