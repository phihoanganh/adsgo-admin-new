/**
 * Created by hangntt10 on 01/02/2018.
 */
class CampaignAddCarController {
  constructor($state, $scope, $mdDialog, $stateParams, ChienDichService, STATE_NAME) {
    'ngInject'
    this.$state = $state
    this.$scope = $scope
    this.$mdDialog = $mdDialog
    this.STATE_NAME = STATE_NAME
    this.$stateParams = $stateParams
    this.ChienDichService = ChienDichService
    this.chienDichId = this.$stateParams.id
    this.campaign = null
    this.availableCars = []
    this.campaignCars = []
    this.selectedCars = []
    this.selectedRemoveCars = []
    this.dates = []
    this.isSelectAllCars = false
    this.isSelectRemoveAllCars = false
    this.searchAvailableCars = ''
    this.searchCampaignCars = ''
    this.listCampaignCarFilters = []
    this.curFilter = {'tongXeChon': 0}
    this.applyingFilter = null
    this.pagingConfig = {
      limit: 100,
      page: 1,
      filterSoXe: ''
    }
    this.pagingConfig1 = {
      limit: 100,
      page: 1,
      filterSoXe: ''
    }
    this.soChos = [{'value': 4, 'name': '4 chỗ'},
                  {'value': 7, 'name': '7 chỗ'}]
    this.dongXes = ['Toyota', 'Sendan']
    this.loaiXes = ['Toyota Vios E', 'Toyota Vios Limo', 'Toyota Innova G', 'Toyota Vios J', 'Toyota Innova E']
    // this.priceSlider = {
    //   minValue: 200,
    //   maxValue: 300,
    //   options: {
    //     floor: 0,
    //     ceil: 500,
    //     translate: function (value) {
    //       return '$' + value
    //     }
    //   }
    // }
  }
  /**
   * init data
   */
  $onInit() {
    this.listCampainCarFilter()
    this.loadCampaign()
    for (var i = 1; i <= 31; i++) {
      this.dates.push(new Date(2018, 1, i))
    }
    this.priceSlider = {
      minValue: this.dates[5],
      maxValue: this.dates[8],
      options: {
        stepsArray: this.dates,
        translate: function(date) {
          if (date != null) { return date.toDateString() }
          return ''
        }
      }
    }
    console.log('check')
    // this.loadAvailableCar()
    // this.loadCampaignCars()
  }
  loadCampaign() {
    this.ChienDichService.getCampaignById(this.chienDichId).then((res) => {
      this.campaign = res
    })
  }
  applyFilter() {
    this.pagingConfig.filterSoXe = this.searchAvailableCars
  }
  applyFilter1() {
    this.pagingConfig1.filterSoXe = this.searchCampaignCars
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  loadAvailableCar(data = null) {
    this.ChienDichService.listAvailableCar(this.chienDichId, data).then((res) => {
      if (res.length > 0) {
        this.availableCars = res
        console.log('list available car: ', res)
      } else {
        this.availableCars = []
      }
    })
  }
  loadCampaignCars(data = null, isUpdate = false) {
    this.ChienDichService.listCampaignCars(this.chienDichId, data).then((res) => {
      if (res.length > 0) {
        this.campaignCars = res
        console.log('list campaign car: ', res)
      } else {
        this.campaignCars = []
      }
      if (isUpdate) {
        this.ChienDichService.updateCampainCarFilter(this.applyingFilter.id, this.applyingFilter.tongXeCan, this.campaignCars.length).then(res => {
          console.log(res)
          this.listCampainCarFilter()
          this.loadCampaign()
        })
      }
    })
  }
  addCarsToCampaign() {
    this.ChienDichService.addCarsToCampaign(this.chienDichId, this.selectedCars).then((res) => {
      if (res) {
        this.showDialog('Thêm xe vào chiến dịch thành công!')
        this.loadAvailableCar(this.applyingFilter)
        this.loadCampaignCars(this.applyingFilter, true)
        this.selectedCars = []
        this.selectedRemoveCars = []
        this.isSelectAllCars = false
        this.isSelectRemoveAllCars = false
      }
    })
  }
  removeCarsFromCampaign() {
    this.ChienDichService.removeCarsFromCampaign(this.chienDichId, this.selectedRemoveCars).then((res) => {
      if (res) {
        this.showDialog('Xóa xe khỏi chiến dịch thành công!')
        this.loadAvailableCar(this.applyingFilter)
        this.loadCampaignCars(this.applyingFilter, true)
        this.selectedCars = []
        this.selectedRemoveCars = []
        this.isSelectAllCars = false
        this.isSelectRemoveAllCars = false
      }
    })
  }
  toggleSelect(bienKiemSoat) {
    let idx = this.selectedCars.indexOf(bienKiemSoat)
    if (idx >= 0) {
      this.selectedCars.splice(idx, 1)
    } else {
      this.selectedCars.push(bienKiemSoat)
    }
    console.log('xe chọn:', this.selectedCars)
  }
  selectAllCars() {
    this.selectedCars = []
    if (this.isSelectAllCars) {
      let startIdx = (this.pagingConfig.page - 1) * this.pagingConfig.limit
      let tempAvailableCars = this.availableCars.slice(startIdx, startIdx + this.pagingConfig.limit)
      tempAvailableCars.forEach(item => {
        this.selectedCars.push(item.bienKiemSoat)
      })
      console.log(this.selectedCars)
    } else {
      this.selectedCars = []
    }
  }
  selectRemoveAllCars() {
    this.selectedRemoveCars = []
    if (this.isSelectRemoveAllCars) {
      let startIdx = (this.pagingConfig1.page - 1) * this.pagingConfig1.limit
      let tempListCampaignCars = this.campaignCars.slice(startIdx, startIdx + this.pagingConfig1.limit)
      tempListCampaignCars.forEach(item => {
        this.selectedRemoveCars.push(item.bienKiemsoat)
      })
      console.log(this.selectedRemoveCars)
    } else {
      this.selectedRemoveCars = []
    }
  }
  goCampaignView() {
    this.$state.go(this.STATE_NAME.CAMPAIGN.state)
  }
  listCampainCarFilter() {
    this.ChienDichService.listCampainCarFilter(this.chienDichId).then((res) => {
      this.listCampaignCarFilters = res
      console.log(res)
    })
  }
  saveCampainCarFilter() {
    let isValid = true
    this.listCampaignCarFilters.forEach(item => {
      if (String(item.soCho) === this.curFilter.soCho &&
        item.dongXe === this.curFilter.dongXe &&
        item.loaiXe === this.curFilter.loaiXe) {
        isValid = false
      }
    })
    if (!isValid) {
      this.showDialog('Bộ lọc này đã tồn tại!')
      return false
    }
    if (this.curFilter.tongXeCan <= 0 || isNaN(parseInt(this.curFilter.tongXeCan))) {
      this.showDialog('Số xe cần phải là số dương!')
      return false
    }
    this.ChienDichService.saveCampainCarFilter(this.chienDichId, this.curFilter).then((res) => {
      if (res > 0) {
        this.curFilter['id'] = res
        this.listCampaignCarFilters.push(this.curFilter)
        this.curFilter = {'tongXeChon': 0}
      }
    })
  }
  updateCampainCarFilter(id) {
    this.ChienDichService.updateCampainCarFilter(id, this.curFilter.tongXeCan).then((res) => {
      if (res > 0) {
        this.listCampaignCarFilters.push(this.curFilter)
        this.curFilter = {}
      }
    })
  }
  confirmDelete(ev, id) {
    var confirm = this.$mdDialog.confirm()
          .title('Xác nhận xóa?')
          .textContent('Bạn chắc chắn muốn xóa?')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Xóa')
          .cancel('Hủy')
    this.$mdDialog.show(confirm).then(() => {
      this.deleteCampainCarFilter(id)
    }, function() {
      console.log('Cancel')
    })
  }
  deleteCampainCarFilter(id) {
    this.ChienDichService.deleteCampainCarFilter(id).then((res) => {
      if (res && res.id > 0) {
        this.listCampainCarFilter()
      }
    })
  }
  filter(idx) {
    this.applyingFilter = this.listCampaignCarFilters[idx]
    if (String(this.applyingFilter.tongXeCan) !== this.curFilter.tongXeCan) {
      this.ChienDichService.updateCampainCarFilter(this.applyingFilter.id, this.applyingFilter.tongXeCan, this.applyingFilter.tongXeChon).then(res => {
        console.log(res)
      })
    }
    this.loadAvailableCar(this.applyingFilter)
    this.loadCampaignCars(this.applyingFilter)
  }
}

export default CampaignAddCarController
