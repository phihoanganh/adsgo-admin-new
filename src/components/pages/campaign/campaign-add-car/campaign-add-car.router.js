/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CampaignAddCarController from './campaign-add-car.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CAMPAIGN.ADD_CAR.state, {
      url: '/add-car/:id',
      views: {
        'content@dashboard': {
          template: require('./campaign-add-car.html'),
          controller: CampaignAddCarController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
