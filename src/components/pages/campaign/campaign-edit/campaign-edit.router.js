/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CampaignEditController from './campaign-edit.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CAMPAIGN.EDIT.state, {
      url: '/edit/:id',
      views: {
        'content@dashboard': {
          template: require('./campaign-edit.html'),
          controller: CampaignEditController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
