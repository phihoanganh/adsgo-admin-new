/**
 * Created by Hangntt10 on 11/20/2017.
 */
/**
 * Created by trantu on 01/02/2018.
 */
class CampaignEditController {
  constructor($rootScope, $state, $stateParams, $scope, moment, CommonService, KhachHangService, QuangCaoService,
    ChienDichService, $mdDateLocale, $mdDialog, STATE_NAME, Upload, $timeout, DateUtils,
    EVENTS, CONTEXT_PATH, REST_BASE_URL, REST_PATH, $q) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.$mdDialog = $mdDialog
    this.Upload = Upload
    this.STATE_NAME = STATE_NAME
    this.REST_PATH = REST_PATH
    this.addCarCampaign = STATE_NAME.CAMPAIGN.ADD_CAR.state
    this.DateUtils = DateUtils
    this.CommonService = CommonService
    this.khachHangService = KhachHangService
    this.quangCaoService = QuangCaoService
    this.chienDichService = ChienDichService
    this.chienDichId = $stateParams.id
    this.$scope = $scope
    this.$timeout = $timeout
    this.chienDich = {}
    this.EVENTS = EVENTS
    this.text = ''
    this.successId = -1
    this.$q = $q
    this.uploadedImages = []
    this.isUploadFail = false
    this.url = CONTEXT_PATH + REST_BASE_URL + REST_PATH.UPLOAD.uploadPath
    this.username = 'test'
    this.numberFile = 0
    this.checkUploadFile = 0
    $mdDateLocale.formatDate = function(date) {
      return moment(date).format('DD-MM-YYYY')
    }
    this.data = {
      soNgayHopDong: undefined,
      selectedKH: undefined,
      tenChienDich: undefined,
      tienChienDich: undefined,
      soNgaySetup: undefined,
      ngaySetup: undefined,
      thoiGianBatDau: undefined,
      thoiGianKetThucTamThoi: undefined,
      selectedKV: undefined,
      selectedQC: undefined,
      image1: undefined,
      image2: undefined,
      image3: undefined,
      image4: undefined,
      image5: undefined
    }
    this.cities = this.CommonService.getListAreas()
    if (this.cities !== null && this.cities.length > 0) {
      this.cities = this.cities.slice(1, this.cities.length)
    }
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyChienDich')
    this.chienDichService.getCampaignById(this.chienDichId)
    .then((result) => {
      console.log(result)
      var id = result['khachHangId']
      var idqc = result['loaiQuangCaoId']
      var kv = result['khuVuc']
      this.data.id = result['id']
      this.data.tenChienDich = result['tenChienDich']
      this.data.tienChienDich = Number(result['tienChienDich'])
      this.data.soNgaySetup = this.DateUtils.diffDay(result['ngayChuanBi'], result['thoiGianBatDau'])
      this.data.ngayChuanBi = result['ngayChuanBi']
      this.data.thoiGianBatDau = result['thoiGianBatDau']
      this.data.thoiGianKetThucTamThoi = result['ngayKetThucTamThoi']
      this.data.bonus = result['bonus']
      this.data.ghiChu = result['ghiChu']
      this.data.thoiGianKetThuc = result['thoiGianKetThuc']
      this.data.urlQuangCao = result['urlQuangCao']
      let listUrls = result['urlQuangCao'].split(',')
      this.listImgs = {}
      listUrls.forEach(url => {
        if (url !== '') {
          let items = url.split('_')
          if (items.length > 1) {
            this.listImgs[items[0]] = this.REST_PATH.AWS_IMAGE.imageBaseUrl + url
          }
        }
      })
      this.checkDate()
      console.log('url', this.urlQuangCao)
      console.log('list', this.listImgs)
      this.khachHangService.danhSachTen()
      .then((res) => {
        this.listKH = res
        for (var i in res) {
          if (res[i]['idKhachHang'] === String(id)) {
            this.data.selectedKH = res[i]
            break
          }
        }
      },
      function (err) {
        console.log(err)
      })

      this.quangCaoService.listAdsKindDetail()
      .then((res) => {
        this.listQC = res
        for (var i in res) {
          if (res[i]['idLoaiQuangCao'] === idqc) {
            this.data.selectedQC = res[i]
            break
          }
        }
      },
      function (err) {
        console.log(err)
      })
      var template = this.cities
      for (var i in template) {
        // console.log(template[i])
        if (template[i] === kv) {
          this.data.selectedKV = template[i]
          break
        }
      }
    },
    function (err) {
      console.log(err)
    })
  }
  checkDate() {
    if ((this.data.thoiGianBatDau && this.data.thoiGianKetThucTamThoi) !== undefined) {
      var d1 = this.data.thoiGianBatDau
      var d2 = this.data.thoiGianKetThucTamThoi
      d1 = new Date(d1)
      d2 = new Date(d2)
      this.data.soNgayHopDong = (d2 - d1) / 86400000
    }
    if ((this.data.thoiGianBatDau && this.data.soNgaySetup) !== undefined) {
      var d = new Date(this.data.thoiGianBatDau)
      var day = this.data.soNgaySetup
      d.setDate(d.getDate() - day)
      this.data.ngaySetup = d
    }
  }
  checkBonus() {
    if (this.data.bonus > 0 && this.data.thoiGianKetThucTamThoi) {
      var d = new Date(this.data.thoiGianKetThuc)
      var d1 = new Date(this.data.thoiGianKetThucTamThoi)
      d.setDate(d1.getDate() + this.data.bonus)
      this.data.thoiGianKetThuc = d
    }
  }
  getSelectedKH() {
    if (this.data.selectedKH !== undefined) {
      return this.data.selectedKH['tenVietTat']
    } else {
      return 'chọn khách hàng'
    }
  }
  getSelectedKV() {
    if (this.data.selectedKV !== undefined) {
      return this.data.selectedKV
    } else {
      return 'chọn khu vuc'
    }
  }

  getSelectedQC() {
    if (this.data.selectedQC !== undefined) {
      return this.data.selectedQC['tenLoaiQuangCao']
    } else {
      return 'chọn quảng cáo'
    }
  }
  showDialog(text) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(text)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  checktime() {
    var that = this
    var x = that.DateUtils.toCurrentTimeString()
    console.log(x)
  }
  submit() {
    this.checkUploadFile = 0
    this.isLoading = true
    var list = this.data
    var data = {}
    data.idChienDich = Number(list.id)
    data.idKhachHang = Number(list.selectedKH['idKhachHang'])
    data.tenChienDich = String(list.tenChienDich)
    data.tienChienDich = Number(list.tienChienDich)
    data.soNgaySetup = Number(list.soNgaySetup)
    data.idLoaiQuangCao = Number(list.selectedQC['idLoaiQuangCao'])
    data.khuVuc = list.selectedKV
    // console.log('thoi gian bat dau' + list.thoiGianBatDau)
    data.thoiGianBatDau = this.DateUtils.convertDateSearch(list.thoiGianBatDau)
    data.thoiGianKetThucTamThoi = this.DateUtils.convertDateSearch(list.thoiGianKetThucTamThoi)
    data.bonus = Number(list.bonus)
    data.ghiChu = String(list.ghiChu)
    data.urlQuangCao = list.urlQuangCao
    data.statusRecord = true
    data.trangThaiChienDich = 'Normal'
    if (!(this.image1 || this.image2 || this.image3 || this.image4 || this.image5)) {
      this.saveCampaign(data)
    } else {
      this.uploadedImages = []
      this.isUploadFail = false
      this.upload(this.image1, 1, data)
      this.upload(this.image2, 2, data)
      this.upload(this.image3, 3, data)
      this.upload(this.image4, 4, data)
      this.upload(this.image5, 5, data)
    }
  }
  saveCampaign(data) {
    this.chienDichService.chinhSuaChienDich(data)
      .then((res) => {
        console.log(res)
        if (res['id'] > 0) {
          this.successId = res['id']
          this.showDialog('Chỉnh sửa chiến dịch thành công')
          this.goToAddCar()
        } else {
          console.log(`that bai`)
          this.showDialog('Lưu không thành công. Vui lòng kiểm trang dữ liệu nhập vào và thử lại')
        }
      },
      (err) => {
        console.log(err)
        console.log(`that bai`)
        this.showDialog('Lưu không thành công, vui lòng kiểm tra kết nối.')
      })
  }
  goToAddCar() {
    this.$state.go(this.addCarCampaign, {id: this.successId})
  }
  goCampaignView() {
    this.$state.go(this.STATE_NAME.CAMPAIGN.state)
  }
  uploadFile() {
    // this.$broadcast(this.EVENTS.SUBMIT_UPLOAD, {'1': this.data.tenChienDich})
    console.log(this.text)
  }

  onChangeImage1(image) {
    if (image !== undefined) {
      this.numberFile += 1
    } else if (this.image1 !== undefined) {
      this.numberFile -= 1
    } else {
      console.log('urlQuangCao', this.data.urlQuangCao)
      this.data.urlQuangCao = this.removeImageUrl(1)
      console.log('urlQuangCao', this.data.urlQuangCao)
    }
    this.image1 = image
  }
  onChangeImage2(image) {
    if (image !== undefined) {
      this.numberFile += 1
    } else if (this.image2 !== undefined) {
      this.numberFile -= 1
    } else {
      console.log('urlQuangCao', this.data.urlQuangCao)
      this.data.urlQuangCao = this.removeImageUrl(2)
      console.log('urlQuangCao', this.data.urlQuangCao)
    }
    this.image2 = image
  }
  onChangeImage3(image) {
    if (image !== undefined) {
      this.numberFile += 1
    } else if (this.image3 !== undefined) {
      this.numberFile -= 1
    } else {
      console.log('urlQuangCao', this.data.urlQuangCao)
      this.data.urlQuangCao = this.removeImageUrl(3)
      console.log('urlQuangCao', this.data.urlQuangCao)
    }
    this.image3 = image
  }
  onChangeImage4(image) {
    if (image !== undefined) {
      this.numberFile += 1
    } else if (this.image4 !== undefined) {
      this.numberFile -= 1
    } else {
      console.log('urlQuangCao', this.data.urlQuangCao)
      this.data.urlQuangCao = this.removeImageUrl(4)
      console.log('urlQuangCao', this.data.urlQuangCao)
    }
    this.image4 = image
  }
  onChangeImage5(image) {
    if (image !== undefined) {
      this.numberFile += 1
    } else if (this.image5 !== undefined) {
      this.numberFile -= 1
    } else {
      console.log('urlQuangCao', this.data.urlQuangCao)
      this.data.urlQuangCao = this.removeImageUrl(5)
      console.log('urlQuangCao', this.data.urlQuangCao)
    }
    this.image5 = image
  }
  removeImageUrl(idx) {
    let listUrlImages = this.data.urlQuangCao.split(',')
    for (let i = listUrlImages.length - 1; i >= 0; i--) {
      let parts = listUrlImages[i].split('_')
      if (parts.length > 1 && parts[0] === String(idx)) {
        listUrlImages.splice(i, 1)
        break
      }
    }
    return listUrlImages.join(',')
  }
  upload(file, idx, dataParsed) {
    if (file) {
      file.upload = this.Upload.upload({
        url: this.url,
        data: { username: this.username, file: file }
      })
      var currentTimeStamp = this.getCurrentTimeStamp()
      var temp = this.splitFileExtension(file.name)
      var renameImage = idx + '_' + currentTimeStamp + '.' + temp
      this.Upload.rename(file, renameImage)
      file.upload.then((response) => {
        console.log('upload result: ', response)
        if (response.data.indexOf('http') !== -1) {
          this.checkUploadFile += 1
          this.uploadedImages.push(renameImage)
        } else {
          if (!this.isUploadFail) {
            this.showDialog('Không thể lưu file')
          }
          this.isUploadFail = true
        }
        if (this.checkUploadFile === this.numberFile) {
          dataParsed.urlQuangCao += ',' + this.uploadedImages.join(',')
          this.saveCampaign(dataParsed)
        }
      }, function (response) {
        if (response.status > 0) {
          if (response.status === 500) {
            console.log('upload fail')
            if (!this.isUploadFail) {
              this.showDialog('Lưu file không thành công')
            }
            this.isUploadFail = true
          }
        }
      }, function (evt) {
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
      })
    }
  }
  splitFileExtension(filename) {
    return filename.split('.').pop()
  }
  getCurrentTimeStamp() {
    console.log(new Date().getTime())
    return new Date().getTime()
  }
}

export default CampaignEditController
