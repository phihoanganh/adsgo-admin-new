/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './campaign-edit.router'
import './campaign-edit.scss'

const moduleName = 'app.component.pages.campaign-edit'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)
  .config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false)
  }])

export default moduleName
