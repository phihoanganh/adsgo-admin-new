/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import campaignEditPage from './campaign-edit/campaign-edit.module'
import campaignViewPage from './campaign-view/campaign-view.module'
import campaignAddPage from './campaign-add/campaign-add.module'
import campaignAddCarPage from './campaign-add-car/campaign-add-car.module'
import RouterConfig from './campaign.router'
import './campaign.scss'

const moduleName = 'app.component.pages.campaign'

/* @ngInject */
angular.module(moduleName, [campaignEditPage, campaignViewPage, campaignAddPage, campaignAddCarPage])
  .config(RouterConfig)

export default moduleName
