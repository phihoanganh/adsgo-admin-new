/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CampaignViewController from './campaign-view.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CAMPAIGN.VIEW.state, {
      url: '/view/:id',
      views: {
        'content@dashboard': {
          template: require('./campaign-view.html'),
          controller: CampaignViewController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
