/**
 * Created by Hangntt10 on 11/20/2017.
 */
class CampaignViewController {
  constructor($rootScope, $scope, $state, $stateParams, $mdDialog, ChienDichService, CommonService, HoTroService,
    DateUtils, COURSE_TYPE, STATE_NAME, DISPLAY_MODE, REST_PATH, EVENTS, NOTI_STATUS, CAMPAIGN_STATUS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$scope = $scope
    this.$state = $state
    this.$mdDialog = $mdDialog
    this.NOTI_STATUS = NOTI_STATUS
    this.CAMPAIGN_STATUS = CAMPAIGN_STATUS
    this.editCampaign = STATE_NAME.CAMPAIGN.EDIT.state
    this.addCarCampaign = STATE_NAME.CAMPAIGN.ADD_CAR.state
    this.campaignCarReport = STATE_NAME.CAMPAIGN.CAR_REPORT.state
    this.EVENTS = EVENTS
    this.REST_PATH = REST_PATH
    this.DateUtils = DateUtils
    this.CommonService = CommonService
    this.ChienDichService = ChienDichService
    this.HoTroService = HoTroService
    this.campaign = null
    this.id = $stateParams['id']
    this.listYeuCauHoTro = []
    this.listStatus = ['Chưa xử lý', 'Đang xử lý', 'Đã xử lý']
    this.filteredNotis = []
    this.selectedStatus = null
    this.totalViewandKm = null
    this.listImgs = {}
    this.showingNoti = null
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyChienDich')
    this.ChienDichService.getCampaignById(this.id).then(data => {
      this.campaign = data
      var startDate = data.thoiGianBatDau.split(' ')
      if (startDate.length > 0) {
        this.campaign.thoiGianBatDau = this.DateUtils.convertDate(startDate[0])
      } else {
        this.campaign.thoiGianBatDau = ''
      }
      var endDate = data.thoiGianKetThuc.split(' ')
      if (endDate.length > 0) {
        this.campaign.thoiGianKetThuc = this.DateUtils.convertDate(endDate[0])
      } else {
        this.campaign.thoiGianKetThuc = ''
      }
      let listUrls = this.campaign['urlQuangCao'].split(',')
      this.listImgs = {}
      listUrls.forEach(url => {
        if (url !== '') {
          let items = url.split('_')
          if (items.length > 1) {
            this.listImgs[items[0]] = this.REST_PATH.AWS_IMAGE.imageBaseUrl + url
          }
        }
      })
    })
    this.citys = this.CommonService.getListAreas()
    this.getListYeuCauHoTro()
    this.getTotalViewAndKm()
  }
  getTotalViewAndKm() {
    this.ChienDichService.getCampainViewAndKm(this.id).then((data) => {
      this.totalViewandKm = data
      console.log(this.totalViewandKm)
    })
  }
  goToEdit(id) {
    this.$state.go(this.editCampaign, {id: id})
  }
  goToAddCar(id) {
    this.$state.go(this.addCarCampaign, {id: id})
  }
  goToCarReport(id) {
    this.$state.go(this.campaignCarReport, {id: id})
  }
  getListYeuCauHoTro() {
    this.HoTroService.listHoTro(this.id).then((res) => {
      if (res && res.length > 0) {
        this.listYeuCauHoTro = res
        this.filteredNotis = res
      } else {
        this.listYeuCauHoTro = []
      }
    })
  }
  selectStatus() {
    this.filteredNotis = []
    if (this.selectedStatus && this.selectedStatus !== '') {
      this.listYeuCauHoTro.forEach(item => {
        if (item.trangThai === this.selectedStatus) {
          this.filteredNotis.push(item)
        }
      })
    } else {
      this.filteredNotis = this.listYeuCauHoTro
    }
  }
  showCustom(event, noti) {
    this.showingNoti = noti
    console.log(this.showingNoti)
    this.$mdDialog.show({
      clickOutsideToClose: true,
      scope: this.$scope,
      preserveScope: true,
      template: require('./dialog.tmpl.html'),
      parent: angular.element(document.body),
      controller: ['$scope', '$mdDialog',
        function($scope, $mdDialog) {
          $scope.closeDialog = function() {
            $mdDialog.hide()
          }
          $scope.cancel = function() {
            $mdDialog.cancel()
          }
          $scope.answer = function(answer) {
            $mdDialog.hide(answer)
          }
        }
      ]
    }).then((answer) => {
      if (answer) {
      }
    }, () => {
      console.log('You cancelled the dialog.')
    })
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
        .clickOutsideToClose(false)
        .title('Thông báo')
        .textContent(result)
        .ariaLabel('Left to right demo')
        .ok('Ok')
        .openFrom('#left')
        .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  updateNoti(status) {
    this.showingNoti.trangThai = status
    this.HoTroService.updateSupportRequest(this.showingNoti).then(res => {
      if (res.status === 200) {
        this.getListYeuCauHoTro()
        this.showDialog('Cập nhật trạng thái thành công!')
      } else {
        this.showDialog('Không thể cập nhật trạng thái!')
      }
    })
  }
}

export default CampaignViewController
