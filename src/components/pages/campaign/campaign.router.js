/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CampaignController from './campaign.controller'
// STATE_NAME.CAMPAIGN

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CAMPAIGN.state, {
      url: '/chien-dich',
      views: {
        'content@dashboard': {
          template: require('./campaign.html'),
          controller: CampaignController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
