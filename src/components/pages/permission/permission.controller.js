/**  goPermissionAdd() {
    this.$state.go(this.STATE_NAME.PERMISSION.ADD.state)
  }
 * Created by Hangntt10 on 11/20/2017.
 */
class PermissionController {
  constructor($rootScope, $state, STATE_NAME, EVENTS, RoleService) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.RoleService = RoleService
    this.pageItems = 10
    this.page = 1
    this.countAllItem = 0
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'TaiKhoanNoiBo')
    this.listAllAccount(this.page, this.pageItems)
  }
  goPermissionAdd() {
    this.$state.go(this.STATE_NAME.PERMISSION.ADD.state)
  }
  listAllAccount(page = '', count = '') {
    this.RoleService.findAllAccount(page, count).then((data) => {
      this.listAccounts = data
      console.log(data)
      if (data.length > 0) {
        this.countAllItem = parseInt(data[0].countAllItem)
      } else {
        this.countAllItem = 0
      }
    })
  }
  changePage(page) {
    this.page = page
    this.applyPagination(page)
  }
  changeItemPerPage(itemPerPage) {
    this.page = 1
    this.pageItems = itemPerPage
    this.applyPagination()
  }
  applyPagination(page = 1) {
    this.listAllAccount(page, this.pageItems)
  }
  deleteAccount(id) {
    var confirm = window.confirm('Xác nhận xóa khách hàng ?')
    if (confirm === true) {
      this.RoleService.deleteAccount(id).then(data => {
        if (data) {
          this.applyPagination(this.page)
        } else {
          alert('Không thể xóa!')
        }
      })
    }
  }
  goEditAccount(id) {
    this.$state.go(this.STATE_NAME.PERMISSION.EDIT.state, {id: id})
  }
  goGrant() {
    this.$state.go(this.STATE_NAME.PERMISSION.GRANT.state)
  }
}

export default PermissionController
