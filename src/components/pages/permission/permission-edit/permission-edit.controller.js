/**
 * Created by Hangntt10 on 11/20/2017.
 */
class PermissionEditController {
  constructor($rootScope, $state, STATE_NAME, EVENTS, $stateParams, $mdDialog, RoleService) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.data = {}
    this.$stateParams = $stateParams
    this.$mdDialog = $mdDialog
    this.accountId = this.$stateParams.id
    this.RoleService = RoleService
    this.data.role = undefined
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'TaiKhoanNoiBo')
    this.getAccount()
  }
  goPermissionView() {
    this.$state.go(this.STATE_NAME.PERMISSION.state)
  }
  getAccount() {
    this.RoleService.getAccountById(this.accountId).then((res) => {
      console.log(res)
      this.data.nameLogin = res['username']
      this.data.userName = res['name']
      this.data.email = res['email']
      this.data.numberPhone = res['sdt']
      this.data.confirmPassword = res['password']
      this.data.newPassword = res['password']
      var roleId = Number(res['vaiTro'])
      this.RoleService.getListRoles().then((res) => {
        this.listRole = res
        for (var i in res) {
          if (res[i]['id'] === roleId) {
            this.data.role = res[i]
            break
          }
        }
      },
      function (err) {
        console.log(err)
      })
    })
  }
  getSelectedRole() {
    if (this.data.role !== undefined) {
      return this.data.role['name']
    } else {
      return 'Chọn vai trò'
    }
  }
  submit() {
    this.result = {}
    this.result.username = this.data['nameLogin']
    this.result.name = this.data['userName']
    this.result.email = this.data['email']
    this.result.sdt = this.data['numberPhone']
    this.result.vaiTro = Number(this.data.role['id'])
    this.result.loaiTaiKhoan = 'NOI_BO'
    if (this.data['confirmPassword'] !== null) {
      this.result.password = this.data['confirmPassword']
    }
    // console.log(this.result)
    this.RoleService.editAccount(this.result, this.accountId).then((res) => {
      if (res['id'] > 0) {
        this.showDialog('Cập nhật tài khoản thành công')
        this.goPermissionView()
      } else {
        this.showDialog('Tài khoản đã tồn tại. Vui lòng nhập lại!')
      }
    },
    function (err) {
      console.log(err)
    })
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      // You can specify either sting with query selector
      .openFrom('#left')
      // or an element
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  preventNumberInput(e) {
    console.log(e)
    var keyCode = (e.keyCode ? e.keyCode : e.which)
    if (keyCode < 48 || keyCode > 57) {
      e.preventDefault()
    }
  }
}

export default PermissionEditController
