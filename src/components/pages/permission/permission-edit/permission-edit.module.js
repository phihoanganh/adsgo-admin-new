/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './permission-edit.router'
import './permission-edit.scss'

const moduleName = 'app.component.pages.permission-edit'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)

export default moduleName
