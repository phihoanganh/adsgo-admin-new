import PermissionEditController from './permission-edit.controller'
// STATE_NAME.CAR

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.PERMISSION.EDIT.state, {
      url: '/edit/:id',
      views: {
        'content@dashboard': {
          template: require('./permission-edit.html'),
          controller: PermissionEditController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
