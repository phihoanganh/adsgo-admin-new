import PermissionGrantController from './permission-grant.controller'
// STATE_NAME.CAR

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.PERMISSION.GRANT.state, {
      url: '/grant',
      views: {
        'content@dashboard': {
          template: require('./permission-grant.html'),
          controller: PermissionGrantController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
