/**
 * Created by Hangntt10 on 11/20/2017.
 */
class PermissionGrantController {
  constructor($rootScope, $state, RoleService, $mdDialog, STATE_NAME, EVENTS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.$mdDialog = $mdDialog
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.RoleService = RoleService
    this.oldListRoles = {}
    this.listRoles = {}
    this.listAccessControls = {}
    this.listIdxChanged = []
    this.enableUpdate = false
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'TaiKhoanNoiBo')
    this.loadRole()
    this.RoleService.getListAccessControls().then((res) => {
      this.listAccessControls = res
    })
  }
  loadRole() {
    this.RoleService.getListRoles().then((res) => {
      this.listRoles = res
      for (var i = 0; i < res.length; i++) {
        for (var j = 0; j < res[i].accessControls.length; j++) {
          this.listRoles[i].accessControls[j] = res[i].accessControls[j].id
        }
      }
      this.oldListRoles = JSON.parse(JSON.stringify(this.listRoles))
    })
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  showPrompt(ev) {
    var confirm = this.$mdDialog.prompt()
      .title('Thêm vai trò mới')
      .textContent('Nhập tên vai trò:')
      .placeholder('')
      .ariaLabel('')
      .initialValue('')
      .targetEvent(ev)
      .required(true)
      .ok('Thêm')
      .cancel('Hủy')
    let that = this
    this.$mdDialog.show(confirm).then(function(result) {
      if (!that.checkDuplicate(result)) {
        that.RoleService.createRole(result).then((res) => {
          if (res.id > 0) {
            that.loadRole()
            that.showDialog('Thêm vai trò thành công!')
          } else {
            that.showDialog('Không thể thêm vai trò!')
          }
        })
      } else {
        that.showDialog('Vai trò đã tồn tại!')
      }
    }, function() {
      console.log('Hủy')
    })
  }
  checkDuplicate(role) {
    var check = this.listRoles.some(item => item.name === role)
    return check
  }
  toggle(role, access) {
    var idx = this.listRoles.indexOf(role)
    if (idx >= 0) {
      var idx1 = this.listRoles[idx].accessControls.indexOf(access.id)
      if (idx1 >= 0) {
        this.listRoles[idx].accessControls.splice(idx1, 1)
      } else {
        this.listRoles[idx].accessControls.push(access.id)
      }
      if (this.compareList(this.listRoles[idx].accessControls, this.oldListRoles[idx].accessControls)) {
        var check = this.listIdxChanged.indexOf(idx)
        if (check >= 0) {
          this.listIdxChanged.splice(check, 1)
        }
      } else {
        check = this.listIdxChanged.indexOf(idx)
        if (check < 0) {
          this.listIdxChanged.push(idx)
        }
      }
      if (this.listIdxChanged.length > 0) {
        this.enableUpdate = true
      } else {
        this.enableUpdate = false
      }
    }
  }
  exists(role, access) {
    return role.accessControls.indexOf(access.id) >= 0
  }
  compareList(arr1, arr2) {
    return arr2.every(arr2Item => arr1.includes(arr2Item)) &&
            arr1.every(arr1Item => arr2.includes(arr1Item))
  }
  updateAccess() {
    var checkSuccess = 0
    var completeProcess = 0
    this.listIdxChanged.forEach(idx => {
      var role = JSON.parse(JSON.stringify(this.listRoles[idx]))
      var access = role.accessControls
      for (var i = 0; i < access.length; i++) {
        access[i] = {'id': access[i]}
      }
      this.RoleService.updateAccess(role).then((res) => {
        if (res.id === role.id) {
          checkSuccess += 1
        }
        completeProcess += 1
        if (completeProcess === this.listIdxChanged.length) {
          if (checkSuccess === this.listIdxChanged.length) {
            this.showDialog('Cập nhật quyền thành công!')
            this.listIdxChanged = []
            this.enableUpdate = false
          } else {
            this.showDialog('Không thể cập nhật!')
          }
        }
      })
    })
  }
}

export default PermissionGrantController
