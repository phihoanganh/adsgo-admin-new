/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './permission-grant.router'
import './permission-grant.scss'

const moduleName = 'app.component.pages.permission-grant'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)

export default moduleName
