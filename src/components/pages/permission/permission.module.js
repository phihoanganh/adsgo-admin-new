/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import permissionAddPage from './permission-add/permission-add.module'
import permissionEditPage from './permission-edit/permission-edit.module'
import permissionGrantPage from './permission-grant/permission-grant.module'
import RouterConfig from './permission.router'
import './permission.scss'

const moduleName = 'app.component.pages.permission'

/* @ngInject */
angular.module(moduleName, [permissionAddPage, permissionEditPage, permissionGrantPage])
  .config(RouterConfig)

export default moduleName
