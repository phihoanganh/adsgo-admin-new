/**
 * Created by Hangntt10 on 02/28/2018.
 */
import PermissionController from './permission.controller'
// STATE_NAME.CAR

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.PERMISSION.state, {
      url: '/phan-quyen',
      views: {
        'content@dashboard': {
          template: require('./permission.html'),
          controller: PermissionController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
