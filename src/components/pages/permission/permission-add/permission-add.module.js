/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './permission-add.router'
import './permission-add.scss'

const moduleName = 'app.component.pages.permission-add'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)

export default moduleName
