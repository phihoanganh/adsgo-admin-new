import PermissionAddController from './permission-add.controller'
// STATE_NAME.CAR

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.PERMISSION.ADD.state, {
      url: '/add',
      views: {
        'content@dashboard': {
          template: require('./permission-add.html'),
          controller: PermissionAddController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
