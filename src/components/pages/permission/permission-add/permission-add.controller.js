/**
 * Created by Hangntt10 on 11/20/2017.
 */
class PermissionAddController {
  constructor($rootScope, $state, STATE_NAME, EVENTS, CommonService, RoleService, $mdDialog) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.commonService = CommonService
    this.RoleService = RoleService
    this.data = {}
    this.$mdDialog = $mdDialog
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'TaiKhoanNoiBo')
    // this.listRole = this.commonService.getRole()
    this.getListRole()
  }
  goPermissionView() {
    this.$state.go(this.STATE_NAME.PERMISSION.state)
  }
  getListRole() {
    this.RoleService.getListRoles().then((res) => {
      this.listRole = res
    })
  }
  addNewInternalAccount() {
    this.result = {}
    this.result.username = this.data['nameLogin']
    this.result.name = this.data['userName']
    this.result.email = this.data['email']
    this.result.sdt = this.data['numberPhone']
    this.result.vaiTro = Number(this.data['role'])
    this.result.loaiTaiKhoan = 'NOI_BO'
    this.result.password = this.data['confirmPassword']
    console.log(this.result)
    this.RoleService.addAccount(this.result).then((res) => {
      // console.log(res)
      if (res['id'] > 0) {
        this.showDialog('Tạo tài khoản thành công')
        this.goPermissionView()
      } else {
        this.showDialog('Tài khoản đã tồn tại. Vui lòng nhập lại!')
      }
    })
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      // You can specify either sting with query selector
      .openFrom('#left')
      // or an element
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  preventNumberInput(e) {
    var keyCode = (e.keyCode ? e.keyCode : e.which)
    if (keyCode < 48 || keyCode > 57) {
      e.preventDefault()
    }
  }
}
export default PermissionAddController
