/**
 * Created by Hangntt10 on 02/28/2018.
 */
import CarController from './car.controller'
// STATE_NAME.CAR

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CAR.state, {
      url: '/xe',
      views: {
        'content@dashboard': {
          template: require('./car.html'),
          controller: CarController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
