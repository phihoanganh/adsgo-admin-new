/**
 * Created by Hangntt10 on 11/20/2017.
 */
class CarController {
  constructor($rootScope, $state, STATE_NAME, EVENTS, CarService, $scope) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.editCar = STATE_NAME.CAR.EDIT.state
    this.addCar = STATE_NAME.CAR.ADD.state
    this.CarService = CarService
    this.$scope = $scope
    this.selectChienDich = undefined
    this.selectTinhTrang = undefined
    this.pageItems = 10
    this.page = 1
    this.countAllItem = 0
    this.bienKiemSoat = ''
    this.delay = 0
    this.delayInterval = undefined
    this.getSelectedTinhTrang = function() {
      if (this.selectTinhTrang !== undefined) {
        return this.selectTinhTrang
      } else {
        return 'chọn tình trạng'
      }
    }
    this.getSelectedChienDich = function() {
      if (this.selectChienDich !== undefined) {
        return this.selectChienDich['name']
      } else {
        return 'chọn chiến dịch'
      }
    }
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyXe')
    this.CarService.layDanhSachTinhTrang()
    .then((res) => {
      this.$scope.danhSachTinhTrang = res
      // console.log(this.$scope.danhSachTinhTrang)
    },
    function (err) {
      console.log(err)
    })
    this.CarService.layDanhSachChienDich()
    .then((res) => {
      this.$scope.danhSachChienDich = res
      // console.log(this.$scope.danhSachChienDich)
    },
    function (err) {
      console.log(err)
    })
    this.searchXe('', '', '', 1, this.pageItems)
  }
  changePage(page) {
    this.page = page
    this.applyFilter(page)
  }
  changeItemPerPage(itemPerPage) {
    this.pageItems = itemPerPage
    this.applyFilter()
  }
  searchChanged(event) {
    if (event.keyCode === 13) {
      this.applyFilter()
    } else {
      // console.log('Changed: ' + this.bienKiemSoat)
      clearInterval(this.intervalDelay)
      var that = this
      this.intervalDelay = setInterval(function() {
        that.applyFilter()
        // console.log('Searching...' + that.bienKiemSoat)
        clearInterval(that.intervalDelay)
      }, 350)
    }
  }
  applyFilter(page = 1) {
    var chienDich = ''
    var tinhTrang = ''
    if (this.bienKiemSoat === undefined) {
      this.bienKiemSoat = ''
    }
    if (this.selectTinhTrang === undefined) {
      tinhTrang = ''
    } else {
      tinhTrang = this.selectTinhTrang
    }
    if (this.selectChienDich === undefined) {
      chienDich = ''
    } else {
      chienDich = this.selectChienDich['id']
    }
    // console.log(chienDich)
    this.searchXe(this.bienKiemSoat, tinhTrang, chienDich, page, this.pageItems)
  }
  searchXe(bienKiemSoat = '', tinhTrang = '', chienDich = '', page = 1, count = 1) {
    this.CarService.locXe(bienKiemSoat, tinhTrang, chienDich, page, count).then((data) => {
      this.listXe = data
      // console.log(this.listXe)
      if (data.length > 0) {
        this.countAllItem = parseInt(data[0]['countAllItem'])
      } else {
        this.countAllItem = 0
      }
    })
  }
  deleteXe(id) {
    var confirm = window.confirm('Xác nhận xóa xe?')
    if (confirm === true) {
      this.CarService.deleteXe(id).then(data => {
        if (data) {
          // alert('Đã xóa!')
          this.applyFilter(this.page)
        } else {
          alert('Không thể xóa!')
        }
      })
    }
  }
  goEditCar(carid) {
    this.$state.go(this.editCar, {id: carid})
  }
  goAddCar() {
    this.$state.go(this.addCar)
  }
}

export default CarController
