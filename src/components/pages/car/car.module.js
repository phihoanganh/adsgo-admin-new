/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import carEditPage from './car-edit/car-edit.module'
import carAddPage from './car-add/car-add.module'
import RouterConfig from './car.router'
import './car.scss'

const moduleName = 'app.component.pages.car'

/* @ngInject */
angular.module(moduleName, [carEditPage, carAddPage])
  .config(RouterConfig)

export default moduleName
