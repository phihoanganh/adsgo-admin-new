/**
 * Created by Hangntt10 on 01/02/2018.
 */
class CarAddController {
  constructor($rootScope, $state, CommonService, CarService, $mdDialog, STATE_NAME, EVENTS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.area = ''
    this.CommonService = CommonService
    this.CarService = CarService
    this.car = {}
    this.$mdDialog = $mdDialog
  }

  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyXe')
    this.citys = this.CommonService.getListAreas()
    this.listDongXe = this.CommonService.getDongXe()
    this.listLoaiXe = this.CommonService.getLoaiXe()
    this.listHangModel = this.CommonService.getHangModel()
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      // You can specify either sting with query selector
      .openFrom('#left')
      // or an element
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  goCarView() {
    this.$state.go(this.STATE_NAME.CAR.state)
  }
  addNewCar() {
    var numb = this.car.soCho.match(/\d+/g)
    var newCar = Object.assign({}, this.car)
    if (numb > 0) {
      newCar.soCho = numb[0]
    }
    // console.log(numb)
    newCar.createDate = ''
    // console.log(this.car)
    this.CarService.AddCar(newCar).then((res) => {
      // console.log(res)
      if (res['bienKiemSoat'] === newCar.bienKiemSoat) {
        // console.log(`thanh cong`)
        this.showDialog('Thêm xe thành công')
        this.goCarView()
      } else {
        // console.log(`that bai`)
        this.showDialog('Không thể thêm xe')
      }
    })
  }
}

export default CarAddController
