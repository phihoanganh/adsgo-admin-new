/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CarAddController from './car-add.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CAR.ADD.state, {
      url: '/add',
      views: {
        'content@dashboard': {
          template: require('./car-add.html'),
          controller: CarAddController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
