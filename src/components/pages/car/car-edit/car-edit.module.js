/**
 * Created by Hangntt10 on 11/20/2017.
 */
/* global angular:true */
import RouterConfig from './car-edit.router'
import './car-edit.scss'

const moduleName = 'app.component.pages.car-edit'

/* @ngInject */
angular.module(moduleName, [])
  .config(RouterConfig)
  .config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false)
  }])

export default moduleName
