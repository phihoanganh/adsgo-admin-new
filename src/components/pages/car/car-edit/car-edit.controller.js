/**
 * Created by Hangntt10 on 01/02/2018.
 */
class CarEditController {
  constructor($rootScope, $scope, $state, $stateParams, CommonService, CarService, $mdDialog, STATE_NAME, EVENTS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$scope = $scope
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.area = ''
    this.CommonService = CommonService
    this.CarService = CarService
    this.$stateParams = $stateParams
    this.carId = this.$stateParams.id
    this.car = null
    this.$mdDialog = $mdDialog
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyXe')
    this.citys = this.CommonService.getListAreas()
    this.listDongXe = this.CommonService.getDongXe()
    this.listLoaiXe = this.CommonService.getLoaiXe()
    this.listHangModel = this.CommonService.getHangModel()
    this.CarService.getCarById(this.carId).then((res) => {
      this.car = res
      this.car.soCho = this.car.soCho + ' chỗ'
    })
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  goCarView() {
    this.$state.go(this.STATE_NAME.CAR.state)
  }
  editCar() {
    var numb = this.car.soCho.match(/\d+/g)
    var newCar = Object.assign({}, this.car)
    if (numb > 0) {
      newCar.soCho = numb[0]
    }
    // console.log(numb)
    newCar.createDate = ''
    this.CarService.EditCar(newCar).then((res) => {
      // console.log(res)
      if (res['bienKiemSoat'] === newCar.bienKiemSoat) {
        // console.log(`thanh cong`)
        this.showDialog('Cập nhật xe thành công')
        this.goCarView()
      } else {
        // console.log(`that bai`)
        this.showDialog('Không thể cập nhật')
      }
    })
  }
}

export default CarEditController
