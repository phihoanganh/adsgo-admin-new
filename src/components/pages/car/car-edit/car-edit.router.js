/**
 * Created by Hangntt10 on 11/21/2017.
 */
import CarEditController from './car-edit.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.CAR.EDIT.state, {
      url: '/edit/:id',
      views: {
        'content@dashboard': {
          template: require('./car-edit.html'),
          controller: CarEditController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
