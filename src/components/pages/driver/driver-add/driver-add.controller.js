/**
 * Created by Hangntt10 on 01/02/2018.
 */
class DriverAddController {
  constructor($rootScope, $state, STATE_NAME, EVENTS, DriverService, $mdDialog) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.DriverService = DriverService
    this.driver = {}
    this.$mdDialog = $mdDialog
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyTaiXe')
    console.log('init edit driver')
  }

  addDriver() {
    this.driver.bienKiemSoat = ''
    this.DriverService.addDriver(this.driver).then((res) => {
      console.log(res)
      if (parseInt(res['cmnd']) !== -1) {
        this.showDialog('Thêm tài xế thành công!')
        this.goDriverView()
      } else {
        this.showDialog('Chứng minh nhân dân đã tồn tại!')
      }
    })
  }
  goDriverView() {
    this.$state.go(this.STATE_NAME.DRIVER.state)
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
        .clickOutsideToClose(false)
        .title('Thông báo')
        .textContent(result)
        .ariaLabel('Left to right demo')
        .ok('Ok')
        .openFrom('#left')
        .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  preventNumberInput(e) {
    var keyCode = (e.keyCode ? e.keyCode : e.which)
    if (keyCode < 48 || keyCode > 57) {
      e.preventDefault()
    }
  }
}

export default DriverAddController
