/**
 * Created by Hangntt10 on 11/21/2017.
 */
import DriverAddController from './driver-add.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.DRIVER.ADD.state, {
      url: '/add',
      views: {
        'content@dashboard': {
          template: require('./driver-add.html'),
          controller: DriverAddController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
