/**
 * Created by Hangntt10 on 11/20/2017.
 */
class DriverController {
  constructor($rootScope, $state, $mdDialog, STATE_NAME, EVENTS, DriverService, CommonService) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.$mdDialog = $mdDialog
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.DriverService = DriverService
    this.listDrivers
    this.pageItems = 10
    this.page = 1
    this.CommonService = CommonService
    this.selectThanhPho = undefined
    this.taiXe = undefined
    this.delayInterval = undefined
    this.countAllItem = 0
    this.addDriver = STATE_NAME.DRIVER.ADD.state
    this.editDriver = STATE_NAME.DRIVER.EDIT.state
    this.getSelectedThanhPho = function() {
      if (this.selectThanhPho !== undefined) {
        return this.selectThanhPho
      } else {
        return 'chọn thành phố '
      }
    }
  }
  /**
   * init data
   */
  $onInit() {
    this.cities = this.CommonService.getListAreas()
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyTaiXe')
    console.log('innit data')
    this.searchDriver('', '')
  }
  searchDriver(searchKey = '', thanhPho = '', page = 1, count = 10) {
    this.DriverService.filterDriver(searchKey, thanhPho, page, count).then((data) => {
      this.listDrivers = data
      if (data.length > 0) {
        this.countAllItem = parseInt(data[0]['countAllItem'])
      } else {
        this.countAllItem = 0
      }
    })
  }
  changePage(page) {
    this.page = page
    this.applyFilter(page)
  }

  changeItemPerPage(itemPerPage) {
    this.page = 1
    this.pageItems = itemPerPage
    this.applyFilter()
  }

  applyFilter(page = 1) {
    var thanhPho = this.selectThanhPho
    var taiXe = this.taiXe
    if (thanhPho === undefined) {
      thanhPho = ''
    }
    if (taiXe === undefined) {
      taiXe = ''
    }
    this.searchDriver(taiXe, thanhPho, page, this.pageItems)
  }

  searchChanged(event) {
    if (event.keyCode === 13) {
      this.applyFilter()
    } else {
      // console.log('Changed: ' + this.bienKiemSoat)
      clearInterval(this.intervalDelay)
      var that = this
      this.intervalDelay = setInterval(function() {
        that.applyFilter()
        // console.log('Searching...' + that.bienKiemSoat)
        clearInterval(that.intervalDelay)
      }, 350)
    }
  }
  goEditDriver(cmnd) {
    this.$state.go(this.editDriver, {cmnd: cmnd})
  }
  goAddDriver() {
    this.$state.go(this.addDriver)
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  showConfirm(ev, id) {
    var confirm = this.$mdDialog.confirm()
          .title('Xác nhận xóa tài xế')
          .textContent('Bạn chắc chắn muốn xóa tài xế?')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Xóa')
          .cancel('Hủy')
    let that = this
    this.$mdDialog.show(confirm).then(function() {
      that.DriverService.deleteDriver(id).then(data => {
        if (data) {
          that.applyFilter(that.page)
        } else {
          that.showDialog('Không thể xóa!')
        }
      })
    }, function() {
      console.log('Cancel')
    })
  }
  deleteDriver(id) {
    var confirm = window.confirm('Xác nhận xóa tài xế  ?')
    if (confirm === true) {
      this.DriverService.deleteDriver(id).then(data => {
        if (data) {
          this.applyFilter(this.page)
        } else {
          alert('Không thể xóa!')
        }
      })
    }
  }
}

export default DriverController
