/**
 * Created by Hangntt10 on 11/21/2017.
 */
import DriverViewController from './driver-view.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.DRIVER.VIEW.state, {
      url: '/view/:cmnd',
      views: {
        'content@dashboard': {
          template: require('./driver-view.html'),
          controller: DriverViewController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
