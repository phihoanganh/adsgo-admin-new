/**
 * Created by Hangntt10 on 01/02/2018.
 */
class DriverAddController {
  constructor($rootScope, $state, $stateParams, $mdDialog, DriverService, STATE_NAME, EVENTS) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.$stateParams = $stateParams
    this.STATE_NAME = STATE_NAME
    this.EVENTS = EVENTS
    this.driverDashboard = STATE_NAME.DRIVER.state
    this.$mdDialog = $mdDialog
    this.DriverService = DriverService
    this.cmnd = this.$stateParams.cmnd
    this.driver = null
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyTaiXe')
    console.log('init view driver')
    this.getTaiXe()
  }
  getTaiXe() {
    console.log('geting tai xe', this.cmnd)
    this.DriverService.getDriverById(this.cmnd).then((data) => {
      console.log('data get ', data)
      this.driver = data
    })
  }
}

export default DriverAddController
