/**
 * Created by Hangntt10 on 01/02/2018.
 */
class DriverEditController {
  constructor($rootScope, $state, $stateParams, $mdDialog, STATE_NAME, EVENTS, DriverService) {
    'ngInject'
    this.$rootScope = $rootScope
    this.$state = $state
    this.$stateParams = $stateParams
    this.STATE_NAME = STATE_NAME
    this.driverDashboard = STATE_NAME.DRIVER.state
    this.$mdDialog = $mdDialog
    this.EVENTS = EVENTS
    this.DriverService = DriverService
    this.cmnd = this.$stateParams.cmnd
    this.driver = null
    this.isDisplayPassword = false
  }
  /**
   * init data
   */
  $onInit() {
    this.$rootScope.$broadcast(this.EVENTS.SET_ACTIVE_MENU, 'QuanLyTaiXe')
    console.log('init edit driver')
    this.getTaiXe()
  }
  showDialog(result) {
    this.$mdDialog.show(
      this.$mdDialog.alert()
      .clickOutsideToClose(false)
      .title('Thông báo')
      .textContent(result)
      .ariaLabel('Left to right demo')
      .ok('Ok')
      .openFrom('#left')
      .closeTo(angular.element(document.querySelector('#right')))
    )
  }
  getTaiXe() {
    console.log('geting tai xe', this.cmnd)
    this.DriverService.getDriverById(this.cmnd).then((data) => {
      console.log('data get ', data)
      this.driver = data
    })
  }
  editDriver() {
    this.DriverService.editDriver(this.driver).then((res) => {
      console.log('result ', res)
      if (res.cmnd === this.cmnd) {
        this.showDialog('Sửa thông tin tài xế thành công')
        this.goDriverDashboard()
      } else {
        this.showDialog('Không thể chỉnh sửa!')
      }
    })
  }
  goDriverDashboard() {
    this.$state.go(this.driverDashboard)
  }
  preventNumberInput(e) {
    var keyCode = (e.keyCode ? e.keyCode : e.which)
    if (keyCode < 48 || keyCode > 57) {
      e.preventDefault()
    }
  }
}

export default DriverEditController
