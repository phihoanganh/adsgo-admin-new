/**
 * Created by Hangntt10 on 11/21/2017.
 */
import DriverEditController from './driver-edit.controller'

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.DRIVER.EDIT.state, {
      url: '/edit/:cmnd',
      views: {
        'content@dashboard': {
          template: require('./driver-edit.html'),
          controller: DriverEditController,
          controllerAs: 'vm'
        }
      }
    })
}
export default RouterConfig
