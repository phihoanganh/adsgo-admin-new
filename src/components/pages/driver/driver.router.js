/**
 * Created by Hangntt10 on 11/21/2017.
 */
import DriverController from './driver.controller'
// STATE_NAME.DRIVER

function RouterConfig ($stateProvider, STATE_NAME) {
  'ngInject'
  $stateProvider
    .state(STATE_NAME.DRIVER.state, {
      url: '/tai-xe',
      views: {
        'content@dashboard': {
          template: require('./driver.html'),
          controller: DriverController,
          controllerAs: 'vm'
        }
      }
    })
}

export default RouterConfig
