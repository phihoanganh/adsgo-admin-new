/**
 * Created by Hangntt10 on 11/20/2017.
 */

/* global angular:true */
import RouterConfig from './driver.router'
import driverEditPage from './driver-edit/driver-edit.module'
import driverAddPage from './driver-add/driver-add.module'
import driverViewPage from './driver-view/driver-view.module'
import './driver.scss'

const moduleName = 'app.component.pages.driver'

/* @ngInject */
angular.module(moduleName, [driverEditPage, driverAddPage, driverViewPage])
  .config(RouterConfig)

export default moduleName
