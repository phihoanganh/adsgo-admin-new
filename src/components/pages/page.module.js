/* global angular:true */
import loginPage from './login/login.module'
import homePage from './home/home.module'
import campaignPage from './campaign/campaign.module'
import carPage from './car/car.module'
import customerPage from './customer/customer.module'
import permissionPage from './permission/permission.module'
import driverPage from './driver/driver.module'
import configurationPage from './configuration/configuration.module'
import imagesPage from './images/images.module'
import reportCarPage from './campaign/report-car/report-car.module'

const moduleName = 'app.component.pages'

angular.module(moduleName, [loginPage, homePage, campaignPage, carPage, customerPage, permissionPage, driverPage, configurationPage, imagesPage, reportCarPage])

export default moduleName
