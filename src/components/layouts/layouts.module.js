/* global angular:true */
import header from './header/header.component'
import footer from './footer/footer.component'
import hfl from './head-foot-layout/hf.component'

let moduleName = 'app.component.layouts'

angular.module(moduleName, [header, footer, hfl])

export default moduleName
