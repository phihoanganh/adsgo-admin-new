/* global angular:true */
let moduleName = 'app.component.layouts.footer'
import controller from './footer.controller'
import style from './footer.scss'
import template from './footer.html'

angular.module(moduleName, [])
  .component('footer', {
    controller, template, style, controllerAs: 'vm'
  })

export default moduleName
