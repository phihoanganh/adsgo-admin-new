class HeaderFooterLayoutController {
  constructor($scope, EVENTS) {
    'ngInject'
    this.$scope = $scope
    this.EVENTS = EVENTS
  }

  $onInit() {
    this.isExpand = true
    this.$scope.$on(this.EVENTS.EXPAND_MENU, (event, data) => {
      console.log(event)
      console.log(data)
      this.isExpand = !this.isExpand
    })
  }
}

export default HeaderFooterLayoutController
