/**
 * Created by TranMXN on 2017/08/11
 */
/* global angular:true */
let moduleName = 'app.component.layouts.head-foot-layout'
import controller from './hf.controller'
import style from './hf.scss'
import template from './hf.html'

angular.module(moduleName, [])
  .component('headFootLayout', {
    controller, template, style, controllerAs: 'vm'
  })

export default moduleName
