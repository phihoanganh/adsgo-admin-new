/* global angular:true */
let moduleName = 'app.component.layouts.header'
import controller from './header.controller'
import './header.scss'
import template from './header.html'

angular.module(moduleName, [])
  .component('header', {
    controller, template, controllerAs: 'vm'
  })

export default moduleName
