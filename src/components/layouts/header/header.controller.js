class HeaderController {
  constructor($scope, $state, AccountService, CommonUtil, STATE_NAME) {
    'ngInject'
    this.$scope = $scope
    this.$state = $state
    this.accountService = AccountService
    this.commonUtil = CommonUtil
    this.homeStateName = STATE_NAME.HOME
    this.campaignStateName = STATE_NAME.CAMPAIGN
    this.loginStateName = STATE_NAME.LOGIN_STATE.state
  }

  $onInit() {
    this.isExpand = false
  }

  expandMenu () {
    this.isExpand = true
    this.commonUtil.menuEvent(this.$scope, this.isExpand)
  }

  collapseMenu () {
    this.isExpand = false
    this.commonUtil.menuEvent(this.$scope, this.isExpand)
  }

  goToHomePage() {
    this.$state.go(this.homeStateName)
  }
  goToCampaignPage() {
    this.$state.go(this.campaignStateName)
  }

  logout() {
    this.accountService.clearAccountInfo()
    this.collapseMenu()
    this.$state.go(this.loginStateName)
  }
}

export default HeaderController
