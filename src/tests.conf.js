import 'angular'
import 'angular-mocks/angular-mocks'

// include all `.js` files except itself
window.__PLATFORM = 'tizen'
window.__DEBUG_MODE = 'debug'
window.__THEME = 'mat'
const context = require.context('../src', true, /^((?!tests\.conf\.spec).)*\.js/)
console.log(context)
context.keys().forEach(context)
