/* global angular:true */
const moduleName = 'app.config'

// For demo
// const contextPath = PROD ? 'http://demo-sg.fsoft-hcm.net:8129' : 'http://10.88.96.171:9090'
// For QA
// const contextPath = PROD ? 'http://demo-sg.fsoft-hcm.net:8134' : 'http://10.88.96.173:8129'
const contextPath = PROD ? 'http://13.229.247.214:8080' : 'http://13.229.247.214:8080'

angular.module(moduleName, [])
  .constant('REST_BASE_URL', '')
  .constant('CONTEXT_PATH', contextPath)
  .constant('STATE_NAME', {
    LOGIN: {
      state: 'login',
      path: '/login'
    },
    HOME: {
      state: 'dashboard.home',
      path: '/home'
    },
    USER_STATE: {
      state: 'users',
      path: '/users'
    },
    // CAMPAIGN: 'dashboard.campaign',
    CAMPAIGN: {
      state: 'dashboard.campaign',
      path: '/campaign',
      EDIT: {
        state: 'dashboard.campaign.edit'
      },
      VIEW: {
        state: 'dashboard.campaign.view'
      },
      ADD: {
        state: 'dashboard.campaign.add'
      },
      ADD_CAR: {
        state: 'dashboard.campaign.addCar'
      },
      CAR_REPORT: {
        state: 'dashboard.campaign.report-car'
      }
    },
    DRIVER: {
      state: 'dashboard.driver',
      path: '/driver',
      EDIT: {
        state: 'dashboard.driver.edit'
      },
      ADD: {
        state: 'dashboard.driver.add'
      },
      VIEW: {
        state: 'dashboard.driver.view'
      }
    },
    CAR: {
      state: 'dashboard.car',
      path: '/car',
      EDIT: {
        state: 'dashboard.car.edit'
      },
      ADD: {
        state: 'dashboard.car.add'
      }
    },
    CUSTOMER: {
      state: 'dashboard.customer',
      path: '/customer',
      EDIT: {
        state: 'dashboard.customer.edit'
      },
      ADD: {
        state: 'dashboard.customer.add'
      },
      VIEW: {
        state: 'dashboard.customer.view'
      }
    },
    PERMISSION: {
      state: 'dashboard.permission',
      path: '/permission',
      ADD: {
        state: 'dashboard.permission.add'
      },
      EDIT: {
        state: 'dashboard.permission.edit'
      },
      GRANT: {
        state: 'dashboard.permission.grants'
      }
    },
    CONFIGURATION: {
      state: 'dashboard.configuration',
      path: '/configuration'
    },
    IMAGES: {
      state: 'dashboard.images',
      path: '/images',
      REQUEST: {
        state: 'dashboard.images.request'
      },
      CONTROL: {
        state: 'dashboard.images.control'
      },
      ADD: {
        state: 'dashboard.images.add'
      }
    },
    RESOUCE_NOT_FOUND: {
      state: '404'
    },
    WELCOME_STATE: {
      state: 'welcome'
    },
    COURSES: {
      LIST_COURSE: 'dashboard.listcourse',
      DETAIL_COURSE: 'dashboard.listcourse.course-detail'
    }
  })
  .constant('DEFAULT_VALUE', {
    CAMPAIGN_SORT: 'create_date',
    DONG_XE: [
      'Sanda'
    ],
    LOAI_XE: [
      '4 chỗ',
      '7 chỗ'
    ],
    IMAGE_REQUEST_STT: [
      'Chờ duyệt',
      'Đang tiến hành',
      'Hoàn thành',
      'Từ chối'
    ],
    REPORT_STATUS: [
      'Đang chờ',
      'Đã chụp',
      'Bị hủy'
    ],
    DRIVER_REPORT_STT: [
      'Chưa có ảnh',
      'Chưa duyệt',
      'Đã duyệt',
      'Từ chối'
    ],
    HANG_MODEL: [
      'Toyota Innova 2011'
    ],
    ROLE: [
      'Administrator'
    ],
    LIST_AREAS: ['',
      'HCM',
      'An Giang',
      'Bà Rịa - Vũng Tàu',
      'Bắc Giang',
      'Bắc Kạn',
      'Bạc Liêu',
      'Bắc Ninh',
      'Bến Tre',
      'Bình Định',
      'Bình Dương',
      'Bình Phước',
      'Bình Thuận',
      'Cà Mau',
      'Cao Bằng',
      'Đắk Lắk',
      'Đắk Nông',
      'Điện Biên',
      'Đồng Nai',
      'Đồng Tháp',
      'Gia Lai',
      'Hà Giang',
      'Hà Nam',
      'Hà Tĩnh',
      'Hải Dương',
      'Hậu Giang',
      'Hòa Bình',
      'Hưng Yên',
      'Khánh Hòa',
      'Kiên Giang',
      'Kon Tum',
      'Lai Châu',
      'Lâm Đồng',
      'Lạng Sơn',
      'Lào Cai',
      'Long An',
      'Nam Định',
      'Nghệ An',
      'Ninh Bình',
      'Ninh Thuận',
      'Phú Thọ',
      'Quảng Bình',
      'Quảng Nam',
      'Quảng Ngãi',
      'Quảng Ninh',
      'Quảng Trị',
      'Sóc Trăng',
      'Sơn La',
      'Tây Ninh',
      'Thái Bình',
      'Thái Nguyên',
      'Thanh Hóa',
      'Thừa Thiên Huế',
      'Tiền Giang',
      'Trà Vinh',
      'Tuyên Quang',
      'Vĩnh Long',
      'Vĩnh Phúc',
      'Yên Bái',
      'Phú Yên',
      'Cần Thơ',
      'Đà Nẵng',
      'Hải Phòng',
      'Hà Nội']
  })
  .constant('REST_PATH', {
    AUTHENTICATION: {
      path: '/user-service/api/auth'
    },
    USERS: {
      path: '/users',
      addUTG: '/users/addUsrToGr',
      checkAnonymous: '/users/isAnonymousUser',
      delete: '/users/delete',
      active: '/users/active'
    },
    GROUPS: {
      path: '/groups',
      get_groups: '/get-groups'
    },
    ROLES: {
      path: '/roles'
    },
    SYSTEM_CONFIGURATION: {
      preSignedUrl: '/resource/getPreSignedUrl'
    },
    COURSE: {
      path: '/courses',
      recommend: '/courses/recommend',
      search: '/courses/search',
      banner: '/courses/banner',
      roadMap: '/courses/road-map'
    },
    DOCUMENT: {
      path: '/documents',
      download: '/documents/download',
      downloadBinary: '/documents/download/binary'
    },
    CATEGORY: {
      getCategoriesPath: '/categories'
    },
    RESOURCE: {
      DOWNLOAD_ZIP: '/resource/courses/{id}/documents'
    },
    CHIENDICH: {
      locChienDichPath: '/campaign-service/api/chien-dich/loc-chien-dich',
      luuChienDichPath: '/campaign-service/api/chien-dich',
      getChienDichPath: '/campaign-service/api/chien-dich/list-chien-dich-by-id/',
      deleteChienDichPath: '/campaign-service/api/chien-dich/',
      chinhSuaChienDichPath: '/campaign-service/api/chien-dich/chinh-sua-chien-dich',
      getChienDichByKHIdPath: '/campaign-service/api/chien-dich/getListChienDichByKHId',
      listAvailableCars: '/campaign-service/api/xe/danh-sach-xe-san-co',
      listCarsInCampaign: '/campaign-service/api/xe/xe-trong-chien-dich',
      addCarsToCampaign: '/campaign-service/api/chien-dich/them-xe-vao-chien-dich',
      removeCarsFromCampaign: '/campaign-service/api/chien-dich/delete-xes-trong-chien-dich',
      getTotalViewAndKm: '/campaign-service/api/chien-dich/countTongViewAndKMChienDich',
      listCampaignCarFilter: '/campaign-service/api/chien-dich/list-campaign-car-filter',
      saveCampaignCarFilter: '/campaign-service/api/chien-dich/save-campaign-car-filter',
      updateCampaignCarFilter: '/campaign-service/api/chien-dich/update-campaign-car-filter',
      deleteCampaignCarFilter: '/campaign-service/api/chien-dich/delete-campaign-car-filter',
      getOverviewInfo: '/campaign-service/api/chien-dich/get-over-view-info'
    },
    KHACHHANG: {
      danhSachTen: '/user-service/api/khach-hang/danh-sach-ten',
      addEditCustomer: '/user-service/api/khach-hang/',
      locKhachHang: '/user-service/api/khach-hang/danh-sach-kh',
      deleteKH: '/user-service/api/khach-hang/'
    },
    HOTRO: {
      listHoTro: '/campaign-service/api/ho-tro/danh-sach-yeu-cau-ho-tro',
      updateHoTro: '/campaign-service/api/ho-tro/update-yeu-cau-ho-tro'
    },
    QUANGCAO: {
      path: '/campaign-service/api/quang-cao/list-view-arguments',
      generalArgumentPath: '/campaign-service/api/quang-cao/get-general-argument',
      updateGeneralArgsPath: '/campaign-service/api/quang-cao/update-general-argument',
      updateViewsArgsPath: '/campaign-service/api/quang-cao/update-views-argument'
    },
    LOCATION: {
      path: './data/location.json'
    },
    QUANLYXE: {
      tinhTrangXe: '/campaign-service/api/xe/getListTinhTrangXe',
      danhSachTenChienDich: '/campaign-service/api/xe/getListTenChienDich',
      locXe: '/campaign-service/api/xe/danhSachXe',
      deleteXe: '/campaign-service/api/xe/',
      addXe: '/campaign-service/api/xe/themXe',
      editXe: '/campaign-service/api/xe',
      getXe: '/campaign-service/api/xe/getXeById',
      getDanhSachXe: '/campaign-service/api/xe/danh-sach-xe-report'
    },
    USER_ROLE: {
      listRolePath: '/user-service/api/role',
      accessControlsPath: '/user-service/api/accesscontrol',
      addAccount: '/user-service/api/user',
      editAccount: '/user-service/api/user/update',
      listAccount: '/user-service/api/user/findAll',
      getAccountById: '/user-service/api/user',
      deleteAccount: '/user-service/api/user',
      listRole: '/user-service/api/user'
    },
    DRIVER: {
      filterDriver: '/user-service/api/taixe/listTaiXe',
      getDriverById: '/user-service/api/taixe/',
      editDriver: '/user-service/api/taixe',
      deleteDriver: '/user-service/api/taixe',
      addDriver: '/user-service/api/taixe'
    },
    UPLOAD: {
      uploadPath: '/campaign-service/api/file/upload'
    },
    AWS_IMAGE: {
      imageBaseUrl: 'https://s3-ap-southeast-1.amazonaws.com/adsgo/'
    },
    BAOCAOHINH: {
      getListBaoCaoHinh: '/campaign-service/api/imageReport/danh-sach-image-report',
      getReportDetail: '/campaign-service/api/imageReport/danh-sach-image-report-detail',
      getReportNote: '/campaign-service/api/imageReport/get-image-report-by-id',
      requestReport: '/campaign-service/api/imageReport/add-image-report',
      addImageReportDetail: '/campaign-service/api/imageReport/add-image-report-detail',
      updateImageReport: '/campaign-service/api/imageReport/update-image-report',
      updateImageReportDetail: '/campaign-service/api/imageReport/update-image-report-detail'
    }
  })

export default moduleName
