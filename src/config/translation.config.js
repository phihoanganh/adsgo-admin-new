import translationsEN from '../statics/i18n/locale-en.json'
import translationsVI from '../statics/i18n/locale-vi.json'

function TranslationConfig($translateProvider) {
  'ngInject'
  $translateProvider.translations('en', translationsEN)
  $translateProvider.translations('vi', translationsVI)
  $translateProvider.preferredLanguage('vi')
  // // remember language
  // $translateProvider.useLocalStorage()
}

export default TranslationConfig
