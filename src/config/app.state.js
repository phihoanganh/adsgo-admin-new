/**
 * Created by DungTQ9 on 11/21/2017.
 */

/* global angular:true */
const moduleName = 'app.state'

const STATE = {
  WELCOME: 'welcome',
  HOME: 'login',
  CAR: 'xe',
  DETAIL: 'details'
}

angular.module(moduleName, [])
  .constant('STATE', STATE)

export default moduleName
