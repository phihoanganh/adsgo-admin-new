/**
 * Created by DungTQ9 on 9/6/2017.
 */

/* global angular:true */
const moduleName = 'app.constant'

const EVENTS = {
  MENU_PANEL: {
    PREVIEW: 'PREVIEW',
    DOWNLOAD: 'DOWNLOAD'
  },
  LOAD_PAGINATION: 'LOAD_PAGINATION',
  EXPAND_MENU: 'EXPAND_MENU',
  SET_ACTIVE_MENU: 'SET_ACTIVE_MENU',
  SUBMIT_UPLOAD: 'SUBMIT_UPLOAD',
  DESELECT_CATEGORY: 'DESELECT_CATEGORY',
  CLEAR_SEARCH_FIELD: 'CLEAR_SEARCH_FIELD',
  CLEAR_UPLOAD_IMAGE: 'CLEAR_UPLOAD_IMAGE'
}

const COURSE_STATUS = {
  OPEN: 'OPEN',
  IN_PROGRESS: 'IN_PROGESS',
  COMPLETED: 'COMPLETED'
}

angular.module(moduleName, [])
  .constant('EVENTS', EVENTS)
  .constant('COURSE_STATUS', COURSE_STATUS)
  .constant('FORMAT', {
    DATETIME: {
      format: 'DD/MMM/YYYY HH:mm'
    },
    FULL_DATETIME: {
      format: 'DD/MMM/YYYY HH:mm:ss'
    },
    DATE: {
      format: 'DD/MMM/YYYY'
    },
    UNLIMITED_TIME: {
      key: 'Unlimited',
      value: '9999-01-01T00:00:00.000Z',
      simpleValue: '01/01/9999 00:00'
    },
    REGEX: {
      // EMAIL: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/,
      USERNAME: /^[a-z0-9_.]{3,50}$/,
      ATTUID: /^[a-z0-9_.]{3,50}$/,
      LETTER: /^[a-zA-Z]*$/
    },
    PUNCTUATION: {
      firstPage: '<<',
      lastPage: '>>'
    }
  })
  .constant('TRAINEE_STATUS', {
    COMPLETED: 'COMPLETED',
    OPEN: 'OPEN',
    IN_PROGRESS: 'IN_PROGRESS',
    NONE: 'NONE'
  })
  .constant('COURSE_TYPE', {
    GENERIC: 'GENERIC',
    HOT: 'HOT',
    BANNER: 'BANNER'
  })
  .constant('PAGE_SIZE', {
    LIST_COURSE: {
      PAGE: 0,
      SIZE: 12
    },
    RECOMMENDATION: {
      PAGE: 0,
      SIZE: 5
    },
    BANNER: {
      PAGE: 0,
      SIZE: 5
    }
  })
  .constant('MENU_PANEL', {
    COURSE_DETAIL_PANEL: [
      {
        value: 'menu.panel.preview'
      },
      {
        value: 'menu.panel.download'
      }
    ]
  })
  .constant('DISPLAY_MODE', {
    SIMPLE: 'SIMPLE',
    ADVANCE: 'ADVANCE'
  })
  .constant('IMAGE_REQUEST_STT', {
    CHO_DUYET: 'Chờ duyệt',
    DANG_TIEN_HANH: 'Đang tiến hành',
    HOAN_THANH: 'Hoàn thành',
    TU_CHOI: 'Từ chối'
  })
  .constant('REPORT_STATUS', {
    DANG_CHO: 'Đang chờ',
    DA_CHUP: 'Đã chụp',
    BI_HUY: 'Bị hủy'
  })
  .constant('DRIVER_REPORT_STT', {
    CHUA_CO_ANH: 'Chưa có ảnh',
    CHUA_DUYET: 'Chưa duyệt',
    DA_DUYET: 'Đã duyệt',
    TU_CHOI: 'Từ chối'
  })
  .constant('NOTI_STATUS', {
    CHUA_XU_LY: 'Chưa xử lý',
    DANG_XU_LY: 'Đang xử lý',
    DA_XU_LY: 'Đã xử lý'
  })
  .constant('CAMPAIGN_STATUS', {
    DANG_CHO: 'Đang chờ',
    DANG_CHAY: 'Đang chạy',
    HOAN_THANH: 'Hoàn thành'
  })
export default moduleName
