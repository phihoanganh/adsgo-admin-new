import angularTranslate from 'angular-translate'
import angularStaticLoader from 'angular-translate-loader-static-files'
import angularTranslateMissingProperty from 'angular-translate-handler-log'

import configApi from './app.config'
import constant from './app.constant'
import dictionary from './app.dictionary'
import errorConfig from './app.errorMsg'
import stateUrl from './app.state'
import translationConfig from './translation.config'

/* global angular:true */
let moduleName = 'app.module.configurations'

/* eslint no-useless-escape: "off" */
angular.module(moduleName, [angularTranslate, angularStaticLoader, angularTranslateMissingProperty, configApi, constant, dictionary, errorConfig, stateUrl])
  .config(translationConfig)

export default moduleName
