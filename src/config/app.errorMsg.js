const moduleName = 'app.errorMsg'

angular.module(moduleName, [])
  .constant('ERROR_MSG', {
    NETWORK_ERR: 'INTERNET CONNECTION ERROR',
    FORBIDDEN: 'User is not authorized to access this resource with an explicit deny',
    LOGIN_ERR: 'Lỗi. Tên đăng nhập hoặc mật khẩu không đúng, vui lòng thử lại.',
    CONCURRENCY_ERROR: 'ConcurrencyLimitViolation',
    EXCEPTION_TEXT: 'exception'
  })

export default moduleName
