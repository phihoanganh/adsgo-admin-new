/** EXTERNAL LIBRARY **/
import 'node_modules/jquery'
import 'node_modules/font-awesome/css/font-awesome.css'
import 'node_modules/moment/moment.js'
import angular from 'node_modules/angular'
import uiRouter from 'node_modules/@uirouter/angularjs'
import sanitize from 'node_modules/angular-sanitize'
import ngMessages from 'node_modules/angular-messages'
import uiSelect from 'node_modules/ui-select'
import 'node_modules/angular-ui-bootstrap'
import 'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css'
import 'node_modules/angular-cookies'
import 'node_modules/angular-animate/angular-animate'
import 'node_modules/angular-aria/angular-aria'
import 'node_modules/angular-material'
import 'node_modules/angular-material/angular-material.scss'
import 'node_modules/font-awesome/css/font-awesome.min.css'
import 'node_modules/slick-carousel/slick/slick.css'
import 'node_modules/slick-carousel/slick/slick-theme.css'
import 'node_modules/slick-carousel/slick/slick'
import 'node_modules/angular-slick-carousel/dist/angular-slick.min'
import 'node_modules/bootstrap/dist/css/bootstrap.min.css'
import 'node_modules/bootstrap/dist/js/bootstrap.min.js'
import 'node_modules/angular-breadcrumb/dist/angular-breadcrumb.min.js'
import 'node_modules/angular-translate'
import 'node_modules/angular-translate-loader-static-files'
import 'node_modules/angular-translate-storage-local'
import 'node_modules/angular-translate-storage-cookie'
import 'node_modules/angular-click-outside/angular-click-outside'
import 'node_modules/angular-paging/'
import 'node_modules/angularjs-toaster'
import 'node_modules/angularjs-toaster/toaster.min.css'
import 'node_modules/angular-image-preloader/dist/angular-image-preloader.min'
import 'node_modules/ng-file-upload/dist/ng-file-upload.js'
import 'node_modules/ng-img-crop/compile/minified/ng-img-crop.js'
import 'node_modules/ng-img-crop/compile/minified/ng-img-crop.css'
import 'node_modules/angular-moment/angular-moment.js'
import 'node_modules/angularjs-slider/dist/rzslider'
import 'node_modules/angularjs-slider/dist/rzslider.css'
import 'node_modules/angular-material-data-table/dist/md-data-table.js'
import 'node_modules/angular-material-data-table/dist/md-data-table.css'
import 'node_modules/angular-filter-count-to/dist/angular-filter-count-to.min.js'

/** APP COMPONENTS **/
import RouterConfig from 'router'
import componentModule from 'components/component.module'
import configModule from 'config/config.module'

/** APP GLOBAL CSS **/
import 'themes/app.mat.styl'
import 'themes/app.global.scss'

/* @ngInject */
angular.module('app', [uiRouter, configModule, 'ngMaterial', 'bw.paging', 'ncy-angular-breadcrumb', 'pascalprecht.translate', 'tw.directives.clickOutside', 'ngCountTo',
  sanitize, ngMessages, uiSelect, componentModule, 'toaster', 'angular-image-preloader', 'slickCarousel', 'ngFileUpload', 'ngImgCrop', 'angularMoment', 'rzModule', 'md.data.table'])
  .config(RouterConfig)
  .run()
