const merge = require('webpack-merge');

const webpackConfig = require('./build/webpack.dev.conf');
process.env.NODE_ENV = 'test';
delete webpackConfig.entry; // Increases build speed
delete webpackConfig.externals; // Increases webpack speed even though there is no such option in webpack's config
webpackConfig.plugins.splice(0, 1)
webpackConfig.plugins.splice(3, 1)
const webpackTestConfig = merge.smart(webpackConfig, {
  devtool: 'inline-source-map'
});
// console.log(webpackConfig.plugins)
module.exports = function (config) {
	config.set({
		basePath: '',

		frameworks: ['jasmine'],

		files: [
			'node_modules/babel-polyfill/dist/polyfill.js',
			'src/tests.conf.js',
      {pattern: 'src/components/**/*.spec.js', included: false}
      // 'src/components/**/*.js'
		],

		preprocessors: {
			'src/tests.conf.js': ['webpack', 'sourcemap']
      // 'src/components/**/*.js': ['webpack', 'sourcemap']
		},

    client: {
      captureConsole: true
    },

    browserConsoleLogOptions: {
      level: 'log',
      format: '%b %T: %m',
      terminal: true
    },
    logLevel: config.LOG_INFO,

		autoWatch: false,

		singleRun: true,

		reporters: ['progress', 'coverage'],

		coverageReporter: {
			dir: './',
			reporters: [
        { type: 'lcov', subdir: 'coverage' },
				{ type: 'json', subdir: 'coverage' },
				{
					type: 'text-summary',
					watermarks: {
						statements: [100, 100],
						branches: [100, 100],
						functions: [100, 100],
						lines: [100, 100]
					}
				}
			]
		},

		browsers: ['PhantomJS'],
		webpack: webpackTestConfig,
		webpackMiddleware: {
			stats: 'errors-only'
		}
	});
};
